"use strict";
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var chai = require("chai");
var lodash_1 = require("lodash");
require("mocha");
var artifacts = require("../src/index");
var expect = chai.expect;
describe('Contract Artifacts', function () {
    var forbiddenProperties = [
        'compilerOutput.evm.bytecode.sourceMap',
        'compilerOutput.evm.bytecode.opcodes',
        'sourceCodes',
        'sources',
        'compiler',
    ];
    it('should not include forbidden attributes', function () {
        var e_1, _a, e_2, _b;
        var forbiddenPropertiesByArtifact = {};
        try {
            for (var _c = __values(Object.entries(artifacts)), _d = _c.next(); !_d.done; _d = _c.next()) {
                var _e = __read(_d.value, 2), artifactName = _e[0], artifact = _e[1];
                try {
                    for (var forbiddenProperties_1 = __values(forbiddenProperties), forbiddenProperties_1_1 = forbiddenProperties_1.next(); !forbiddenProperties_1_1.done; forbiddenProperties_1_1 = forbiddenProperties_1.next()) {
                        var forbiddenProperty = forbiddenProperties_1_1.value;
                        var rejectedValue = lodash_1.get(artifact, forbiddenProperty);
                        if (rejectedValue) {
                            var previousForbidden = forbiddenPropertiesByArtifact[artifactName];
                            forbiddenPropertiesByArtifact[artifactName] = previousForbidden
                                ? __spread(previousForbidden, [forbiddenProperty]) : [forbiddenProperty];
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (forbiddenProperties_1_1 && !forbiddenProperties_1_1.done && (_b = forbiddenProperties_1.return)) _b.call(forbiddenProperties_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
            }
            finally { if (e_1) throw e_1.error; }
        }
        expect(forbiddenPropertiesByArtifact).to.eql({});
    });
});
//# sourceMappingURL=contract_artifacts_test.js.map