"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var dev_utils_1 = require("@0x/dev-utils");
var subproviders_1 = require("@0x/subproviders");
var utils_1 = require("@0x/utils");
var chai = require("chai");
require("mocha");
var src_1 = require("../src");
var chai_setup_1 = require("./utils/chai_setup");
var constants_1 = require("./utils/constants");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
describe('ERC721Wrapper', function () {
    var contractWrappers;
    var userAddresses;
    var tokens;
    var ownerAddress;
    var tokenAddress;
    var anotherOwnerAddress;
    var operatorAddress;
    var approvedAddress;
    var receiverAddress;
    var config;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, contractAddresses;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _b.sent();
                    config = {
                        networkId: constants_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 2:
                    userAddresses = _b.sent();
                    tokens = token_utils_1.tokenUtils.getDummyERC721TokenAddresses();
                    tokenAddress = tokens[0];
                    _a = __read(userAddresses, 5), ownerAddress = _a[0], operatorAddress = _a[1], anotherOwnerAddress = _a[2], approvedAddress = _a[3], receiverAddress = _a[4];
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('#transferFromAsync', function () {
        it('should fail to transfer NFT if fromAddress has no approvals set', function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        return [2 /*return*/, expect(contractWrappers.erc721Token.transferFromAsync(tokenAddress, receiverAddress, approvedAddress, tokenId)).to.be.rejectedWith(src_1.ContractWrappersError.ERC721NoApproval)];
                }
            });
        }); });
        it('should successfully transfer tokens when sender is an approved address', function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId, txHash, owner, newOwner;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalAsync(tokenAddress, approvedAddress, tokenId)];
                    case 2:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, tokenId)];
                    case 4:
                        owner = _a.sent();
                        expect(owner).to.be.equal(ownerAddress);
                        return [4 /*yield*/, contractWrappers.erc721Token.transferFromAsync(tokenAddress, receiverAddress, approvedAddress, tokenId)];
                    case 5:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, tokenId)];
                    case 7:
                        newOwner = _a.sent();
                        expect(newOwner).to.be.equal(receiverAddress);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should successfully transfer tokens when sender is an approved operator', function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId, isApprovedForAll, txHash, owner, newOwner;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                    case 2:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, tokenId)];
                    case 4:
                        owner = _a.sent();
                        expect(owner).to.be.equal(ownerAddress);
                        return [4 /*yield*/, contractWrappers.erc721Token.transferFromAsync(tokenAddress, receiverAddress, operatorAddress, tokenId)];
                    case 5:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, tokenId)];
                    case 7:
                        newOwner = _a.sent();
                        expect(newOwner).to.be.equal(receiverAddress);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#getTokenCountAsync', function () {
        describe('With provider with accounts', function () {
            it('should return the count for an existing ERC721 token', function () { return __awaiter(_this, void 0, void 0, function () {
                var tokenCount;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.erc721Token.getTokenCountAsync(tokenAddress, ownerAddress)];
                        case 1:
                            tokenCount = _a.sent();
                            expect(tokenCount).to.be.bignumber.equal(0);
                            return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.erc721Token.getTokenCountAsync(tokenAddress, ownerAddress)];
                        case 3:
                            tokenCount = _a.sent();
                            expect(tokenCount).to.be.bignumber.equal(1);
                            return [2 /*return*/];
                    }
                });
            }); });
            it('should return a balance of 0 for a non-existent owner address', function () { return __awaiter(_this, void 0, void 0, function () {
                var nonExistentOwner, balance, expectedBalance;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            nonExistentOwner = '0x198c6ad858f213fb31b6fe809e25040e6b964593';
                            return [4 /*yield*/, contractWrappers.erc721Token.getTokenCountAsync(tokenAddress, nonExistentOwner)];
                        case 1:
                            balance = _a.sent();
                            expectedBalance = new utils_1.BigNumber(0);
                            return [2 /*return*/, expect(balance).to.be.bignumber.equal(expectedBalance)];
                    }
                });
            }); });
        });
        describe('With provider without accounts', function () {
            var zeroExContractWithoutAccounts;
            before(function () { return __awaiter(_this, void 0, void 0, function () {
                var emptyWalletProvider;
                return __generator(this, function (_a) {
                    emptyWalletProvider = addEmptyWalletSubprovider(web3_wrapper_1.provider);
                    zeroExContractWithoutAccounts = new src_1.ContractWrappers(emptyWalletProvider, config);
                    return [2 /*return*/];
                });
            }); });
            it('should return balance even when called with provider instance without addresses', function () { return __awaiter(_this, void 0, void 0, function () {
                var balance;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, zeroExContractWithoutAccounts.erc721Token.getTokenCountAsync(tokenAddress, ownerAddress)];
                        case 1:
                            balance = _a.sent();
                            return [2 /*return*/, expect(balance).to.be.bignumber.equal(0)];
                    }
                });
            }); });
        });
    });
    describe('#getOwnerOfAsync', function () {
        it('should return the owner for an existing ERC721 token', function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId, tokenOwner;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, tokenId)];
                    case 2:
                        tokenOwner = _a.sent();
                        expect(tokenOwner).to.be.bignumber.equal(ownerAddress);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should return undefined not 0 for a non-existent ERC721', function () { return __awaiter(_this, void 0, void 0, function () {
            var fakeTokenId;
            return __generator(this, function (_a) {
                fakeTokenId = new utils_1.BigNumber(42);
                return [2 /*return*/, expect(contractWrappers.erc721Token.getOwnerOfAsync(tokenAddress, fakeTokenId)).to.be.rejectedWith(src_1.ContractWrappersError.ERC721OwnerNotFound)];
            });
        }); });
    });
    describe('#setApprovalForAllAsync/isApprovedForAllAsync', function () {
        it('should check if operator address is approved', function () { return __awaiter(_this, void 0, void 0, function () {
            var isApprovedForAll, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.erc721Token.isApprovedForAllAsync(tokenAddress, ownerAddress, operatorAddress)];
                    case 1:
                        isApprovedForAll = _a.sent();
                        expect(isApprovedForAll).to.be.false();
                        // set
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                    case 2:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.isApprovedForAllAsync(tokenAddress, ownerAddress, operatorAddress)];
                    case 4:
                        isApprovedForAll = _a.sent();
                        expect(isApprovedForAll).to.be.true();
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, false)];
                    case 5:
                        // unset
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.isApprovedForAllAsync(tokenAddress, ownerAddress, operatorAddress)];
                    case 7:
                        isApprovedForAll = _a.sent();
                        expect(isApprovedForAll).to.be.false();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#setProxyApprovalForAllAsync/isProxyApprovedForAllAsync', function () {
        it('should check if proxy address is approved', function () { return __awaiter(_this, void 0, void 0, function () {
            var isApprovedForAll, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setProxyApprovalForAllAsync(tokenAddress, ownerAddress, isApprovedForAll)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.isProxyApprovedForAllAsync(tokenAddress, ownerAddress)];
                    case 3:
                        isApprovedForAll = _a.sent();
                        expect(isApprovedForAll).to.be.true();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#setApprovalAsync/getApprovedIfExistsAsync', function () {
        it("should set the spender's approval", function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId, approvalBeforeSet, approvalAfterSet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getApprovedIfExistsAsync(tokenAddress, tokenId)];
                    case 2:
                        approvalBeforeSet = _a.sent();
                        expect(approvalBeforeSet).to.be.undefined();
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalAsync(tokenAddress, approvedAddress, tokenId)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.getApprovedIfExistsAsync(tokenAddress, tokenId)];
                    case 4:
                        approvalAfterSet = _a.sent();
                        expect(approvalAfterSet).to.be.equal(approvedAddress);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#setProxyApprovalAsync/isProxyApprovedAsync', function () {
        it('should set the proxy approval', function () { return __awaiter(_this, void 0, void 0, function () {
            var tokenId, isProxyApprovedBeforeSet, isProxyApprovedAfterSet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                    case 1:
                        tokenId = _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.isProxyApprovedAsync(tokenAddress, tokenId)];
                    case 2:
                        isProxyApprovedBeforeSet = _a.sent();
                        expect(isProxyApprovedBeforeSet).to.be.false();
                        return [4 /*yield*/, contractWrappers.erc721Token.setProxyApprovalAsync(tokenAddress, tokenId)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.isProxyApprovedAsync(tokenAddress, tokenId)];
                    case 4:
                        isProxyApprovedAfterSet = _a.sent();
                        expect(isProxyApprovedAfterSet).to.be.true();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#subscribe', function () {
        var indexFilterValues = {};
        afterEach(function () {
            contractWrappers.erc721Token.unsubscribeAll();
        });
        // Hack: Mocha does not allow a test to be both async and have a `done` callback
        // Since we need to await the receipt of the event in the `subscribe` callback,
        // we do need both. A hack is to make the top-level a sync fn w/ a done callback and then
        // wrap the rest of the test in an async block
        // Source: https://github.com/mochajs/mocha/issues/2407
        it('Should receive the Transfer event when tokens are transfered', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callback, tokenId, isApprovedForAll, _a, _b, _c, _d;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            callback = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                expect(logEvent.isRemoved).to.be.false();
                                expect(logEvent.log.logIndex).to.be.equal(0);
                                expect(logEvent.log.transactionIndex).to.be.equal(0);
                                expect(logEvent.log.blockNumber).to.be.a('number');
                                var args = logEvent.log.args;
                                expect(args._from).to.be.equal(ownerAddress);
                                expect(args._to).to.be.equal(receiverAddress);
                                expect(args._tokenId).to.be.bignumber.equal(tokenId);
                            });
                            return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                        case 1:
                            tokenId = _e.sent();
                            isApprovedForAll = true;
                            _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                            return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                        case 2: return [4 /*yield*/, _b.apply(_a, [_e.sent(),
                                constants_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                        case 3:
                            _e.sent();
                            contractWrappers.erc721Token.subscribe(tokenAddress, src_1.ERC721TokenEvents.Transfer, indexFilterValues, callback);
                            _d = (_c = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                            return [4 /*yield*/, contractWrappers.erc721Token.transferFromAsync(tokenAddress, receiverAddress, operatorAddress, tokenId)];
                        case 4: return [4 /*yield*/, _d.apply(_c, [_e.sent(),
                                constants_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                        case 5:
                            _e.sent();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Should receive the Approval event when allowance is being set', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callback, tokenId, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            callback = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                expect(logEvent).to.not.be.undefined();
                                expect(logEvent.isRemoved).to.be.false();
                                var args = logEvent.log.args;
                                expect(args._owner).to.be.equal(ownerAddress);
                                expect(args._approved).to.be.equal(approvedAddress);
                                expect(args._tokenId).to.be.bignumber.equal(tokenId);
                            });
                            contractWrappers.erc721Token.subscribe(tokenAddress, src_1.ERC721TokenEvents.Approval, indexFilterValues, callback);
                            return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                        case 1:
                            tokenId = _c.sent();
                            _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                            return [4 /*yield*/, contractWrappers.erc721Token.setApprovalAsync(tokenAddress, approvedAddress, tokenId)];
                        case 2: return [4 /*yield*/, _b.apply(_a, [_c.sent(),
                                constants_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                        case 3:
                            _c.sent();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Outstanding subscriptions are cancelled when contractWrappers.unsubscribeAll called', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callbackNeverToBeCalled, callbackToBeCalled, tokenId, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            callbackNeverToBeCalled = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                done(new Error('Expected this subscription to have been cancelled'));
                            });
                            contractWrappers.erc721Token.subscribe(tokenAddress, src_1.ERC721TokenEvents.Transfer, indexFilterValues, callbackNeverToBeCalled);
                            callbackToBeCalled = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)();
                            contractWrappers.unsubscribeAll();
                            contractWrappers.erc721Token.subscribe(tokenAddress, src_1.ERC721TokenEvents.Approval, indexFilterValues, callbackToBeCalled);
                            return [4 /*yield*/, token_utils_1.tokenUtils.mintDummyERC721Async(tokenAddress, ownerAddress)];
                        case 1:
                            tokenId = _c.sent();
                            _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                            return [4 /*yield*/, contractWrappers.erc721Token.setApprovalAsync(tokenAddress, approvedAddress, tokenId)];
                        case 2: return [4 /*yield*/, _b.apply(_a, [_c.sent(),
                                constants_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                        case 3:
                            _c.sent();
                            done();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Should cancel subscription when unsubscribe called', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callbackNeverToBeCalled, subscriptionToken, isApproved, _a, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            callbackNeverToBeCalled = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                done(new Error('Expected this subscription to have been cancelled'));
                            });
                            subscriptionToken = contractWrappers.erc721Token.subscribe(tokenAddress, src_1.ERC721TokenEvents.ApprovalForAll, indexFilterValues, callbackNeverToBeCalled);
                            contractWrappers.erc721Token.unsubscribe(subscriptionToken);
                            isApproved = true;
                            _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                            return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApproved)];
                        case 1: return [4 /*yield*/, _b.apply(_a, [_c.sent(),
                                constants_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                        case 2:
                            _c.sent();
                            done();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
    });
    describe('#getLogsAsync', function () {
        var blockRange = {
            fromBlock: 0,
            toBlock: src_1.BlockParamLiteral.Latest,
        };
        var txHash;
        it('should get logs with decoded args emitted by ApprovalForAll', function () { return __awaiter(_this, void 0, void 0, function () {
            var isApprovedForAll, eventName, indexFilterValues, logs, args;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        eventName = src_1.ERC721TokenEvents.ApprovalForAll;
                        indexFilterValues = {};
                        return [4 /*yield*/, contractWrappers.erc721Token.getLogsAsync(tokenAddress, eventName, blockRange, indexFilterValues)];
                    case 3:
                        logs = _a.sent();
                        expect(logs).to.have.length(1);
                        args = logs[0].args;
                        expect(logs[0].event).to.be.equal(eventName);
                        expect(args._owner).to.be.equal(ownerAddress);
                        expect(args._operator).to.be.equal(operatorAddress);
                        expect(args._approved).to.be.equal(isApprovedForAll);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should only get the logs with the correct event name', function () { return __awaiter(_this, void 0, void 0, function () {
            var isApprovedForAll, differentEventName, indexFilterValues, logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        differentEventName = src_1.ERC721TokenEvents.Transfer;
                        indexFilterValues = {};
                        return [4 /*yield*/, contractWrappers.erc721Token.getLogsAsync(tokenAddress, differentEventName, blockRange, indexFilterValues)];
                    case 3:
                        logs = _a.sent();
                        expect(logs).to.have.length(0);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should only get the logs with the correct indexed fields', function () { return __awaiter(_this, void 0, void 0, function () {
            var isApprovedForAll, eventName, indexFilterValues, logs, args;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isApprovedForAll = true;
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, ownerAddress, operatorAddress, isApprovedForAll)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.erc721Token.setApprovalForAllAsync(tokenAddress, anotherOwnerAddress, operatorAddress, isApprovedForAll)];
                    case 3:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 4:
                        _a.sent();
                        eventName = src_1.ERC721TokenEvents.ApprovalForAll;
                        indexFilterValues = {
                            _owner: anotherOwnerAddress,
                        };
                        return [4 /*yield*/, contractWrappers.erc721Token.getLogsAsync(tokenAddress, eventName, blockRange, indexFilterValues)];
                    case 5:
                        logs = _a.sent();
                        expect(logs).to.have.length(1);
                        args = logs[0].args;
                        expect(args._owner).to.be.equal(anotherOwnerAddress);
                        return [2 /*return*/];
                }
            });
        }); });
    });
});
// tslint:disable:max-file-line-count
function addEmptyWalletSubprovider(p) {
    var e_1, _a;
    var providerEngine = new subproviders_1.Web3ProviderEngine();
    providerEngine.addProvider(new subproviders_1.EmptyWalletSubprovider());
    var currentSubproviders = p._providers;
    try {
        for (var currentSubproviders_1 = __values(currentSubproviders), currentSubproviders_1_1 = currentSubproviders_1.next(); !currentSubproviders_1_1.done; currentSubproviders_1_1 = currentSubproviders_1.next()) {
            var subprovider = currentSubproviders_1_1.value;
            providerEngine.addProvider(subprovider);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (currentSubproviders_1_1 && !currentSubproviders_1_1.done && (_a = currentSubproviders_1.return)) _a.call(currentSubproviders_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    utils_1.providerUtils.startProviderEngine(providerEngine);
    return providerEngine;
}
//# sourceMappingURL=erc721_wrapper_test.js.map