"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var dev_utils_1 = require("@0x/dev-utils");
var fill_scenarios_1 = require("@0x/fill-scenarios");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
require("mocha");
var src_1 = require("../src");
var constants_1 = require("./utils/constants");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
describe('TransactionEncoder', function () {
    var contractWrappers;
    var userAddresses;
    var fillScenarios;
    var exchangeContractAddress;
    var makerTokenAddress;
    var takerTokenAddress;
    var coinbase;
    var makerAddress;
    var senderAddress;
    var takerAddress;
    var makerAssetData;
    var takerAssetData;
    var txHash;
    var fillableAmount = new utils_1.BigNumber(5);
    var takerTokenFillAmount = new utils_1.BigNumber(5);
    var signedOrder;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, contractAddresses, config, zrxTokenAddress;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _d.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 2:
                    _d.sent();
                    config = {
                        networkId: constants_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    exchangeContractAddress = contractWrappers.exchange.address;
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 3:
                    userAddresses = _d.sent();
                    zrxTokenAddress = contractWrappers.exchange.zrxTokenAddress;
                    fillScenarios = new fill_scenarios_1.FillScenarios(web3_wrapper_1.provider, userAddresses, zrxTokenAddress, exchangeContractAddress, contractWrappers.erc20Proxy.address, contractWrappers.erc721Proxy.address);
                    _a = __read(userAddresses, 4), coinbase = _a[0], makerAddress = _a[1], takerAddress = _a[2], senderAddress = _a[3];
                    _b = __read(token_utils_1.tokenUtils.getDummyERC20TokenAddresses(), 2), makerTokenAddress = _b[0], takerTokenAddress = _b[1];
                    _c = __read([
                        order_utils_1.assetDataUtils.encodeERC20AssetData(makerTokenAddress),
                        order_utils_1.assetDataUtils.encodeERC20AssetData(takerTokenAddress),
                    ], 2), makerAssetData = _c[0], takerAssetData = _c[1];
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, takerAddress, fillableAmount)];
                case 4:
                    signedOrder = _d.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    after(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('encode and executeTransaction', function () {
        var executeTransactionOrThrowAsync = function (encoder, data, signerAddress) {
            if (signerAddress === void 0) { signerAddress = takerAddress; }
            return __awaiter(_this, void 0, void 0, function () {
                var salt, transactionHash, signature;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            salt = order_utils_1.generatePseudoRandomSalt();
                            transactionHash = encoder.getTransactionHashHex(data, salt, signerAddress);
                            return [4 /*yield*/, order_utils_1.signatureUtils.ecSignHashAsync(web3_wrapper_1.provider, transactionHash, signerAddress)];
                        case 1:
                            signature = _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.executeTransactionAsync(salt, signerAddress, data, signature, senderAddress)];
                        case 2:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 3:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        describe('#fillOrderTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.fillOrderTx(signedOrder, takerTokenFillAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#fillOrderNoThrowTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.fillOrderNoThrowTx(signedOrder, takerTokenFillAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#fillOrKillOrderTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.fillOrKillOrderTx(signedOrder, takerTokenFillAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketSellOrdersTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.marketSellOrdersTx([signedOrder], takerTokenFillAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketSellOrdersNoThrowTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.marketSellOrdersNoThrowTx([signedOrder], takerTokenFillAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketBuyOrdersTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.marketBuyOrdersTx([signedOrder], fillableAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketBuyOrdersNoThrowTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.marketBuyOrdersNoThrowTx([signedOrder], fillableAmount);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#preSignTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, orderHash, signature, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            orderHash = order_utils_1.orderHashUtils.getOrderHashHex(signedOrder);
                            signature = signedOrder.signature;
                            data = encoder.preSignTx(orderHash, makerAddress, signature);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#setSignatureValidatorApprovalTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, isApproved, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            isApproved = true;
                            data = encoder.setSignatureValidatorApprovalTx(senderAddress, isApproved);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrdersTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.batchFillOrdersTx([signedOrder], [takerTokenFillAmount]);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrKillOrdersTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.batchFillOrKillOrdersTx([signedOrder], [takerTokenFillAmount]);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrdersNoThrowTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.batchFillOrdersNoThrowTx([signedOrder], [takerTokenFillAmount]);
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchCancelOrdersTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data, signerAddress;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.batchCancelOrdersTx([signedOrder]);
                            signerAddress = makerAddress;
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data, signerAddress)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#cancelOrderTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, data, signerAddress;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            data = encoder.cancelOrderTx(signedOrder);
                            signerAddress = makerAddress;
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data, signerAddress)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#cancelOrdersUpToTx', function () {
            it('should successfully execute the transaction', function () { return __awaiter(_this, void 0, void 0, function () {
                var encoder, targetEpoch, data, signerAddress;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                        case 1:
                            encoder = _a.sent();
                            targetEpoch = signedOrder.salt;
                            data = encoder.cancelOrdersUpToTx(targetEpoch);
                            signerAddress = makerAddress;
                            return [4 /*yield*/, executeTransactionOrThrowAsync(encoder, data, signerAddress)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
});
//# sourceMappingURL=transaction_encoder_test.js.map