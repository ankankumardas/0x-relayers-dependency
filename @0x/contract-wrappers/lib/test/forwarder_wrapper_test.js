"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var dev_utils_1 = require("@0x/dev-utils");
var fill_scenarios_1 = require("@0x/fill-scenarios");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var chai = require("chai");
require("mocha");
var src_1 = require("../src");
var chai_setup_1 = require("./utils/chai_setup");
var constants_1 = require("./utils/constants");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
// tslint:disable:custom-no-magic-numbers
describe('ForwarderWrapper', function () {
    var fillableAmount = new utils_1.BigNumber(5);
    var contractWrappers;
    var fillScenarios;
    var exchangeContractAddress;
    var zrxTokenAddress;
    var userAddresses;
    var makerAddress;
    var takerAddress;
    var makerTokenAddress;
    var takerTokenAddress;
    var makerAssetData;
    var takerAssetData;
    var signedOrder;
    var anotherSignedOrder;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, contractAddresses, config;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _d.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 2:
                    _d.sent();
                    config = {
                        networkId: constants_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    exchangeContractAddress = contractWrappers.exchange.address;
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 3:
                    userAddresses = _d.sent();
                    zrxTokenAddress = contractWrappers.exchange.zrxTokenAddress;
                    fillScenarios = new fill_scenarios_1.FillScenarios(web3_wrapper_1.provider, userAddresses, zrxTokenAddress, exchangeContractAddress, contractWrappers.erc20Proxy.address, contractWrappers.erc721Proxy.address);
                    _a = __read(userAddresses, 3), makerAddress = _a[1], takerAddress = _a[2];
                    _b = __read(token_utils_1.tokenUtils.getDummyERC20TokenAddresses(), 1), makerTokenAddress = _b[0];
                    takerTokenAddress = contractWrappers.forwarder.etherTokenAddress;
                    _c = __read([
                        order_utils_1.assetDataUtils.encodeERC20AssetData(makerTokenAddress),
                        order_utils_1.assetDataUtils.encodeERC20AssetData(takerTokenAddress),
                    ], 2), makerAssetData = _c[0], takerAssetData = _c[1];
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, constants_1.constants.NULL_ADDRESS, fillableAmount)];
                case 4:
                    signedOrder = _d.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, constants_1.constants.NULL_ADDRESS, fillableAmount)];
                case 5:
                    anotherSignedOrder = _d.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    after(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('#marketBuyOrdersWithEthAsync', function () {
        it('should market buy orders with eth', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrders, makerAssetFillAmount, txHash, ordersInfo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        signedOrders = [signedOrder, anotherSignedOrder];
                        makerAssetFillAmount = signedOrder.makerAssetAmount.plus(anotherSignedOrder.makerAssetAmount);
                        return [4 /*yield*/, contractWrappers.forwarder.marketBuyOrdersWithEthAsync(signedOrders, makerAssetFillAmount, takerAddress, makerAssetFillAmount)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.getOrdersInfoAsync([signedOrder, anotherSignedOrder])];
                    case 3:
                        ordersInfo = _a.sent();
                        expect(ordersInfo[0].orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                        expect(ordersInfo[1].orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw when invalid transaction and shouldValidate is true', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrders, makerAssetFillAmount;
            return __generator(this, function (_a) {
                signedOrders = [signedOrder];
                makerAssetFillAmount = signedOrder.makerAssetAmount.plus(100);
                return [2 /*return*/, expect(contractWrappers.forwarder.marketBuyOrdersWithEthAsync(signedOrders, makerAssetFillAmount, takerAddress, makerAssetFillAmount, [], 0, constants_1.constants.NULL_ADDRESS, {
                        shouldValidate: true,
                    })).to.be.rejectedWith('COMPLETE_FILL_FAILED')];
            });
        }); });
    });
    describe('#marketSellOrdersWithEthAsync', function () {
        it('should market sell orders with eth', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrders, makerAssetFillAmount, txHash, ordersInfo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        signedOrders = [signedOrder, anotherSignedOrder];
                        makerAssetFillAmount = signedOrder.makerAssetAmount.plus(anotherSignedOrder.makerAssetAmount);
                        return [4 /*yield*/, contractWrappers.forwarder.marketSellOrdersWithEthAsync(signedOrders, takerAddress, makerAssetFillAmount)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.getOrdersInfoAsync([signedOrder, anotherSignedOrder])];
                    case 3:
                        ordersInfo = _a.sent();
                        expect(ordersInfo[0].orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                        expect(ordersInfo[1].orderStatus).to.be.equal(src_1.OrderStatus.Fillable);
                        expect(ordersInfo[1].orderTakerAssetFilledAmount).to.be.bignumber.equal(new utils_1.BigNumber(4)); // only 95% of ETH is sold
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw when invalid transaction and shouldValidate is true', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrderWithFee, signedOrders, makerAssetFillAmount;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, constants_1.constants.ZERO_AMOUNT, new utils_1.BigNumber(100), makerAddress, constants_1.constants.NULL_ADDRESS, fillableAmount, constants_1.constants.NULL_ADDRESS)];
                    case 1:
                        signedOrderWithFee = _a.sent();
                        signedOrders = [signedOrderWithFee];
                        makerAssetFillAmount = signedOrder.makerAssetAmount;
                        return [2 /*return*/, expect(contractWrappers.forwarder.marketSellOrdersWithEthAsync(signedOrders, takerAddress, makerAssetFillAmount, [], 0, constants_1.constants.NULL_ADDRESS, {
                                shouldValidate: true,
                            })).to.be.rejectedWith('COMPLETE_FILL_FAILED')];
                }
            });
        }); });
    });
});
//# sourceMappingURL=forwarder_wrapper_test.js.map