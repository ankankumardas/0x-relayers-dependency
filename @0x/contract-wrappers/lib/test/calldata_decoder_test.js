"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var contracts_test_utils_1 = require("@0x/contracts-test-utils");
var dev_utils_1 = require("@0x/dev-utils");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var chai = require("chai");
require("mocha");
var src_1 = require("../src");
var chai_setup_1 = require("./utils/chai_setup");
var migrate_1 = require("./utils/migrate");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
describe('ABI Decoding Calldata', function () {
    var defaultERC20MakerAssetAddress = utils_1.addressUtils.generatePseudoRandomAddress();
    var matchOrdersSignature = 'matchOrders((address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes,bytes)';
    var signedOrderLeft;
    var signedOrderRight;
    var orderLeft = {};
    var orderRight = {};
    var matchOrdersTxData;
    var contractAddresses;
    var contractWrappers;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var accounts, _a, makerAddressLeft, makerAddressRight, _b, privateKeyLeft, privateKeyRight, exchangeAddress, feeRecipientAddress, orderFactoryLeft, orderFactoryRight, config, transactionEncoder;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0: return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 1:
                    accounts = _c.sent();
                    _a = __read(accounts, 2), makerAddressLeft = _a[0], makerAddressRight = _a[1];
                    _b = __read(contracts_test_utils_1.constants.TESTRPC_PRIVATE_KEYS, 2), privateKeyLeft = _b[0], privateKeyRight = _b[1];
                    exchangeAddress = utils_1.addressUtils.generatePseudoRandomAddress();
                    feeRecipientAddress = utils_1.addressUtils.generatePseudoRandomAddress();
                    // Create orders to match.
                    // Values are arbitrary, with the exception of maker addresses (generated above).
                    orderLeft = {
                        makerAddress: makerAddressLeft,
                        makerAssetData: order_utils_1.assetDataUtils.encodeERC20AssetData(defaultERC20MakerAssetAddress),
                        makerAssetAmount: new utils_1.BigNumber(10),
                        takerAddress: '0x0000000000000000000000000000000000000000',
                        takerAssetData: order_utils_1.assetDataUtils.encodeERC20AssetData(defaultERC20MakerAssetAddress),
                        takerAssetAmount: new utils_1.BigNumber(1),
                        feeRecipientAddress: feeRecipientAddress,
                        makerFee: new utils_1.BigNumber(0),
                        takerFee: new utils_1.BigNumber(0),
                        senderAddress: '0x0000000000000000000000000000000000000000',
                        expirationTimeSeconds: new utils_1.BigNumber(1549498915),
                        salt: new utils_1.BigNumber(217),
                    };
                    orderRight = {
                        makerAddress: makerAddressRight,
                        makerAssetData: order_utils_1.assetDataUtils.encodeERC20AssetData(defaultERC20MakerAssetAddress),
                        makerAssetAmount: new utils_1.BigNumber(1),
                        takerAddress: '0x0000000000000000000000000000000000000000',
                        takerAssetData: order_utils_1.assetDataUtils.encodeERC20AssetData(defaultERC20MakerAssetAddress),
                        takerAssetAmount: new utils_1.BigNumber(8),
                        feeRecipientAddress: feeRecipientAddress,
                        makerFee: new utils_1.BigNumber(0),
                        takerFee: new utils_1.BigNumber(0),
                        senderAddress: '0x0000000000000000000000000000000000000000',
                        expirationTimeSeconds: new utils_1.BigNumber(1549498915),
                        salt: new utils_1.BigNumber(50010),
                    };
                    orderFactoryLeft = new contracts_test_utils_1.OrderFactory(privateKeyLeft, orderLeft);
                    return [4 /*yield*/, orderFactoryLeft.newSignedOrderAsync({ exchangeAddress: exchangeAddress })];
                case 2:
                    signedOrderLeft = _c.sent();
                    orderFactoryRight = new contracts_test_utils_1.OrderFactory(privateKeyRight, orderRight);
                    return [4 /*yield*/, orderFactoryRight.newSignedOrderAsync({ exchangeAddress: exchangeAddress })];
                case 3:
                    signedOrderRight = _c.sent();
                    return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 4:
                    // Encode match orders transaction
                    contractAddresses = _c.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 5:
                    _c.sent();
                    config = {
                        networkId: contracts_test_utils_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    return [4 /*yield*/, contractWrappers.exchange.transactionEncoderAsync()];
                case 6:
                    transactionEncoder = _c.sent();
                    matchOrdersTxData = transactionEncoder.matchOrdersTx(signedOrderLeft, signedOrderRight);
                    return [2 /*return*/];
            }
        });
    }); });
    describe('decode', function () {
        it('should successfully decode DutchAuction.matchOrders calldata', function () { return __awaiter(_this, void 0, void 0, function () {
            var contractName, decodedTxData, expectedFunctionName, expectedFunctionArguments;
            return __generator(this, function (_a) {
                contractName = 'DutchAuction';
                decodedTxData = contractWrappers
                    .getAbiDecoder()
                    .decodeCalldataOrThrow(matchOrdersTxData, contractName);
                expectedFunctionName = 'matchOrders';
                expectedFunctionArguments = {
                    buyOrder: orderLeft,
                    sellOrder: orderRight,
                    buySignature: signedOrderLeft.signature,
                    sellSignature: signedOrderRight.signature,
                };
                expect(decodedTxData.functionName).to.be.equal(expectedFunctionName);
                expect(decodedTxData.functionSignature).to.be.equal(matchOrdersSignature);
                expect(decodedTxData.functionArguments).to.be.deep.equal(expectedFunctionArguments);
                return [2 /*return*/];
            });
        }); });
        it('should successfully decode Exchange.matchOrders calldata (and distinguish from DutchAuction.matchOrders)', function () { return __awaiter(_this, void 0, void 0, function () {
            var contractName, decodedTxData, expectedFunctionName, expectedFunctionArguments;
            return __generator(this, function (_a) {
                contractName = 'Exchange';
                decodedTxData = contractWrappers
                    .getAbiDecoder()
                    .decodeCalldataOrThrow(matchOrdersTxData, contractName);
                expectedFunctionName = 'matchOrders';
                expectedFunctionArguments = {
                    leftOrder: orderLeft,
                    rightOrder: orderRight,
                    leftSignature: signedOrderLeft.signature,
                    rightSignature: signedOrderRight.signature,
                };
                expect(decodedTxData.functionName).to.be.equal(expectedFunctionName);
                expect(decodedTxData.functionSignature).to.be.equal(matchOrdersSignature);
                expect(decodedTxData.functionArguments).to.be.deep.equal(expectedFunctionArguments);
                return [2 /*return*/];
            });
        }); });
        it('should throw if cannot decode calldata', function () { return __awaiter(_this, void 0, void 0, function () {
            var badTxData;
            return __generator(this, function (_a) {
                badTxData = '0x01020304';
                expect(function () {
                    contractWrappers.getAbiDecoder().decodeCalldataOrThrow(badTxData);
                }).to.throw("No functions registered for selector '0x01020304'");
                return [2 /*return*/];
            });
        }); });
    });
});
//# sourceMappingURL=calldata_decoder_test.js.map