"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var contracts_test_utils_1 = require("@0x/contracts-test-utils");
var dev_utils_1 = require("@0x/dev-utils");
var order_utils_1 = require("@0x/order-utils");
var types_1 = require("@0x/types");
var utils_1 = require("@0x/utils");
var chai = require("chai");
require("mocha");
var src_1 = require("../src");
var chai_setup_1 = require("./utils/chai_setup");
var constants_1 = require("./utils/constants");
var dutch_auction_utils_1 = require("./utils/dutch_auction_utils");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
// tslint:disable:custom-no-magic-numbers
describe('DutchAuctionWrapper', function () {
    var makerAssetAmount = new utils_1.BigNumber(5);
    var auctionEndTakerAmount = new utils_1.BigNumber(10);
    var auctionBeginTakerAmount = auctionEndTakerAmount.times(2);
    var tenMinutesInSeconds = 10 * 60;
    var contractWrappers;
    var exchangeContractAddress;
    var userAddresses;
    var makerAddress;
    var takerAddress;
    var makerTokenAddress;
    var takerTokenAddress;
    var buyOrder;
    var sellOrder;
    var makerTokenAssetData;
    var takerTokenAssetData;
    var auctionBeginTimeSeconds;
    var auctionEndTimeSeconds;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, contractAddresses, config, currentBlockTimestamp, coinbase, dutchAuctionUtils;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _d.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 2:
                    _d.sent();
                    config = {
                        networkId: constants_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    exchangeContractAddress = contractWrappers.exchange.address;
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 3:
                    userAddresses = _d.sent();
                    _a = __read(userAddresses, 3), makerAddress = _a[1], takerAddress = _a[2];
                    _b = __read(token_utils_1.tokenUtils.getDummyERC20TokenAddresses(), 2), makerTokenAddress = _b[0], takerTokenAddress = _b[1];
                    // construct asset data for tokens being swapped
                    _c = __read([
                        order_utils_1.assetDataUtils.encodeERC20AssetData(makerTokenAddress),
                        order_utils_1.assetDataUtils.encodeERC20AssetData(takerTokenAddress),
                    ], 2), makerTokenAssetData = _c[0], takerTokenAssetData = _c[1];
                    return [4 /*yield*/, contracts_test_utils_1.getLatestBlockTimestampAsync()];
                case 4:
                    currentBlockTimestamp = _d.sent();
                    auctionBeginTimeSeconds = new utils_1.BigNumber(currentBlockTimestamp - tenMinutesInSeconds);
                    auctionEndTimeSeconds = new utils_1.BigNumber(currentBlockTimestamp + tenMinutesInSeconds);
                    coinbase = userAddresses[0];
                    dutchAuctionUtils = new dutch_auction_utils_1.DutchAuctionUtils(web3_wrapper_1.web3Wrapper, coinbase, exchangeContractAddress, contractWrappers.erc20Proxy.address);
                    return [4 /*yield*/, dutchAuctionUtils.createSignedSellOrderAsync(auctionBeginTimeSeconds, auctionEndTimeSeconds, auctionBeginTakerAmount, auctionEndTakerAmount, makerAssetAmount, makerTokenAssetData, takerTokenAssetData, makerAddress, constants_1.constants.NULL_ADDRESS)];
                case 5:
                    sellOrder = _d.sent();
                    return [4 /*yield*/, dutchAuctionUtils.createSignedBuyOrderAsync(sellOrder, takerAddress)];
                case 6:
                    buyOrder = _d.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    after(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('.decodeDutchAuctionAssetData', function () {
        it('decodes to the encoded values', function () { return __awaiter(_this, void 0, void 0, function () {
            var encodedAssetData, decodedAssetData, erc20AssetData;
            return __generator(this, function (_a) {
                encodedAssetData = src_1.DutchAuctionWrapper.encodeDutchAuctionAssetData(makerTokenAssetData, auctionBeginTimeSeconds, makerAssetAmount);
                decodedAssetData = src_1.DutchAuctionWrapper.decodeDutchAuctionData(encodedAssetData);
                erc20AssetData = decodedAssetData.assetData;
                expect(erc20AssetData.tokenAddress).to.eq(makerTokenAddress);
                expect(decodedAssetData.beginAmount).to.be.bignumber.eq(makerAssetAmount);
                expect(decodedAssetData.beginTimeSeconds).to.be.bignumber.eq(auctionBeginTimeSeconds);
                return [2 /*return*/];
            });
        }); });
    });
    describe('#matchOrdersAsync', function () {
        it('should match two orders', function () { return __awaiter(_this, void 0, void 0, function () {
            var txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.dutchAuction.matchOrdersAsync(buyOrder, sellOrder, takerAddress)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw when invalid transaction and shouldValidate is true', function () { return __awaiter(_this, void 0, void 0, function () {
            var badSellOrder, badBuyOrder;
            return __generator(this, function (_a) {
                badSellOrder = buyOrder;
                badBuyOrder = sellOrder;
                return [2 /*return*/, contracts_test_utils_1.expectTransactionFailedAsync(contractWrappers.dutchAuction.matchOrdersAsync(badBuyOrder, badSellOrder, takerAddress, {
                        shouldValidate: true,
                    }), types_1.RevertReason.InvalidAssetData)];
            });
        }); });
    });
    describe('#getAuctionDetailsAsync', function () {
        it('should get auction details', function () { return __awaiter(_this, void 0, void 0, function () {
            var auctionDetails;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.dutchAuction.getAuctionDetailsAsync(sellOrder)];
                    case 1:
                        auctionDetails = _a.sent();
                        // run some basic sanity checks on the return value
                        expect(auctionDetails.beginTimeSeconds, 'auctionDetails.beginTimeSeconds').to.be.bignumber.equal(auctionBeginTimeSeconds);
                        expect(auctionDetails.beginAmount, 'auctionDetails.beginAmount').to.be.bignumber.equal(auctionBeginTakerAmount);
                        expect(auctionDetails.endTimeSeconds, 'auctionDetails.endTimeSeconds').to.be.bignumber.equal(auctionEndTimeSeconds);
                        return [2 /*return*/];
                }
            });
        }); });
    });
});
//# sourceMappingURL=dutch_auction_wrapper_test.js.map