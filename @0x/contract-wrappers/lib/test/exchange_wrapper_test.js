"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var dev_utils_1 = require("@0x/dev-utils");
var fill_scenarios_1 = require("@0x/fill-scenarios");
var order_utils_1 = require("@0x/order-utils");
var types_1 = require("@0x/types");
var utils_1 = require("@0x/utils");
var chai = require("chai");
var ethereum_types_1 = require("ethereum-types");
require("mocha");
var src_1 = require("../src");
var UntransferrableDummyERC20Token_1 = require("./artifacts/UntransferrableDummyERC20Token");
var chai_setup_1 = require("./utils/chai_setup");
var constants_1 = require("./utils/constants");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
describe('ExchangeWrapper', function () {
    var contractWrappers;
    var userAddresses;
    var zrxTokenAddress;
    var fillScenarios;
    var exchangeContractAddress;
    var makerTokenAddress;
    var takerTokenAddress;
    var makerAddress;
    var anotherMakerAddress;
    var takerAddress;
    var makerAssetData;
    var takerAssetData;
    var txHash;
    var fillableAmount = new utils_1.BigNumber(5);
    var takerTokenFillAmount = new utils_1.BigNumber(5);
    var signedOrder;
    var anotherSignedOrder;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, contractAddresses, config;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _d.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 2:
                    _d.sent();
                    config = {
                        networkId: constants_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    exchangeContractAddress = contractWrappers.exchange.address;
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 3:
                    userAddresses = _d.sent();
                    zrxTokenAddress = contractWrappers.exchange.zrxTokenAddress;
                    fillScenarios = new fill_scenarios_1.FillScenarios(web3_wrapper_1.provider, userAddresses, zrxTokenAddress, exchangeContractAddress, contractWrappers.erc20Proxy.address, contractWrappers.erc721Proxy.address);
                    _a = __read(userAddresses, 5), makerAddress = _a[1], takerAddress = _a[2], anotherMakerAddress = _a[4];
                    _b = __read(token_utils_1.tokenUtils.getDummyERC20TokenAddresses(), 2), makerTokenAddress = _b[0], takerTokenAddress = _b[1];
                    _c = __read([
                        order_utils_1.assetDataUtils.encodeERC20AssetData(makerTokenAddress),
                        order_utils_1.assetDataUtils.encodeERC20AssetData(takerTokenAddress),
                    ], 2), makerAssetData = _c[0], takerAssetData = _c[1];
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, takerAddress, fillableAmount)];
                case 4:
                    signedOrder = _d.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, takerAddress, fillableAmount)];
                case 5:
                    anotherSignedOrder = _d.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    after(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('fill order(s)', function () {
        describe('#fillOrderAsync', function () {
            it('should fill a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#fillOrderNoThrowAsync', function () {
            it('should fill a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                var orderInfo;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrderNoThrowAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(signedOrder)];
                        case 3:
                            orderInfo = _a.sent();
                            expect(orderInfo.orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#fillOrKillOrderAsync', function () {
            it('should fill or kill a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrKillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrdersAsync', function () {
            it('should fill a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount];
                            return [4 /*yield*/, contractWrappers.exchange.batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketBuyOrdersAsync', function () {
            it('should maker buy', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, makerAssetFillAmount;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            makerAssetFillAmount = takerTokenFillAmount;
                            return [4 /*yield*/, contractWrappers.exchange.marketBuyOrdersAsync(signedOrders, makerAssetFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketBuyOrdersNoThrowAsync', function () {
            it('should no throw maker buy', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, makerAssetFillAmount, orderInfo;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            makerAssetFillAmount = takerTokenFillAmount;
                            return [4 /*yield*/, contractWrappers.exchange.marketBuyOrdersNoThrowAsync(signedOrders, makerAssetFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(signedOrder)];
                        case 3:
                            orderInfo = _a.sent();
                            expect(orderInfo.orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketSellOrdersAsync', function () {
            it('should maker sell', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmount;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmount = takerTokenFillAmount;
                            return [4 /*yield*/, contractWrappers.exchange.marketSellOrdersAsync(signedOrders, takerAssetFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#marketSellOrdersNoThrowAsync', function () {
            it('should no throw maker sell', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmount, orderInfo;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmount = takerTokenFillAmount;
                            return [4 /*yield*/, contractWrappers.exchange.marketSellOrdersNoThrowAsync(signedOrders, takerAssetFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(signedOrder)];
                        case 3:
                            orderInfo = _a.sent();
                            expect(orderInfo.orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrdersNoThrowAsync', function () {
            it('should fill a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts, orderInfo;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount];
                            return [4 /*yield*/, contractWrappers.exchange.batchFillOrdersNoThrowAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(signedOrder)];
                        case 3:
                            orderInfo = _a.sent();
                            expect(orderInfo.orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                            return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(anotherSignedOrder)];
                        case 4:
                            orderInfo = _a.sent();
                            expect(orderInfo.orderStatus).to.be.equal(src_1.OrderStatus.FullyFilled);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrKillOrdersAsync', function () {
            it('should fill or kill a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount];
                            return [4 /*yield*/, contractWrappers.exchange.batchFillOrKillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#matchOrdersAsync', function () {
            it('should match two valid ordersr', function () { return __awaiter(_this, void 0, void 0, function () {
                var matchingSignedOrder;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(takerAssetData, makerAssetData, makerAddress, takerAddress, fillableAmount)];
                        case 1:
                            matchingSignedOrder = _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.matchOrdersAsync(signedOrder, matchingSignedOrder, takerAddress)];
                        case 2:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 3:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
    describe('cancel order(s)', function () {
        describe('#cancelOrderAsync', function () {
            it('should cancel a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.exchange.cancelOrderAsync(signedOrder)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchCancelOrdersAsync', function () {
            it('should cancel a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var orders;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            orders = [signedOrder, anotherSignedOrder];
                            return [4 /*yield*/, contractWrappers.exchange.batchCancelOrdersAsync(orders)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#cancelOrdersUpTo/getOrderEpochAsync', function () {
            it('should cancel orders up to target order epoch', function () { return __awaiter(_this, void 0, void 0, function () {
                var targetOrderEpoch, orderEpoch;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            targetOrderEpoch = new utils_1.BigNumber(42);
                            return [4 /*yield*/, contractWrappers.exchange.cancelOrdersUpToAsync(targetOrderEpoch, makerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderEpochAsync(makerAddress, constants_1.constants.NULL_ADDRESS)];
                        case 3:
                            orderEpoch = _a.sent();
                            expect(orderEpoch).to.be.bignumber.equal(targetOrderEpoch.plus(1));
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
    describe('#getZRXAssetData', function () {
        it('should get the asset data', function () {
            var ZRX_ASSET_DATA = contractWrappers.exchange.getZRXAssetData();
            var ASSET_DATA_HEX_LENGTH = 74;
            expect(ZRX_ASSET_DATA).to.have.length(ASSET_DATA_HEX_LENGTH);
        });
    });
    describe('#getOrderInfoAsync', function () {
        it('should get the order info', function () { return __awaiter(_this, void 0, void 0, function () {
            var orderInfo, orderHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.getOrderInfoAsync(signedOrder)];
                    case 1:
                        orderInfo = _a.sent();
                        orderHash = order_utils_1.orderHashUtils.getOrderHashHex(signedOrder);
                        expect(orderInfo.orderHash).to.be.equal(orderHash);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#getOrdersInfoAsync', function () {
        it('should get the orders info', function () { return __awaiter(_this, void 0, void 0, function () {
            var ordersInfo, orderHash, anotherOrderHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.getOrdersInfoAsync([signedOrder, anotherSignedOrder])];
                    case 1:
                        ordersInfo = _a.sent();
                        orderHash = order_utils_1.orderHashUtils.getOrderHashHex(signedOrder);
                        expect(ordersInfo[0].orderHash).to.be.equal(orderHash);
                        anotherOrderHash = order_utils_1.orderHashUtils.getOrderHashHex(anotherSignedOrder);
                        expect(ordersInfo[1].orderHash).to.be.equal(anotherOrderHash);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#validateOrderFillableOrThrowAsync', function () {
        it('should throw if signature is invalid', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrderWithInvalidSignature;
            return __generator(this, function (_a) {
                signedOrderWithInvalidSignature = __assign({}, signedOrder, { signature: '0x1b61a3ed31b43c8780e905a260a35faefcc527be7516aa11c0256729b5b351bc3340349190569279751135161d22529dc25add4f6069af05be04cacbda2ace225403' });
                return [2 /*return*/, expect(contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrderWithInvalidSignature)).to.eventually.to.be.rejectedWith(types_1.RevertReason.InvalidOrderSignature)];
            });
        }); });
        it('should validate the order with the current balances and allowances for the maker', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder, {
                            validateRemainingOrderAmountIsFillable: false,
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should validate the order with remaining fillable amount for the order', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should validate the order with specified amount', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder, {
                            expectedFillTakerTokenAmount: signedOrder.takerAssetAmount,
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw if the amount is greater than the allowance/balance', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, expect(contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder, {
                        // tslint:disable-next-line:custom-no-magic-numbers
                        expectedFillTakerTokenAmount: new utils_1.BigNumber(2).pow(256).minus(1),
                    })).to.eventually.to.be.rejected()];
            });
        }); });
        it('should throw when the maker does not have enough balance for the remaining order amount', function () { return __awaiter(_this, void 0, void 0, function () {
            var makerBalance, remainingBalance, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, contractWrappers.erc20Token.getBalanceAsync(makerTokenAddress, makerAddress)];
                    case 1:
                        makerBalance = _c.sent();
                        remainingBalance = makerBalance.minus(signedOrder.makerAssetAmount.minus(1));
                        _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                        return [4 /*yield*/, contractWrappers.erc20Token.transferAsync(makerTokenAddress, makerAddress, constants_1.constants.NULL_ADDRESS, remainingBalance)];
                    case 2: return [4 /*yield*/, _b.apply(_a, [_c.sent()])];
                    case 3:
                        _c.sent();
                        return [2 /*return*/, expect(contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder)).to.eventually.to.be.rejected()];
                }
            });
        }); });
        it('should validate the order when remaining order amount has some fillable amount', function () { return __awaiter(_this, void 0, void 0, function () {
            var makerBalance, remainingBalance, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, contractWrappers.erc20Token.getBalanceAsync(makerTokenAddress, makerAddress)];
                    case 1:
                        makerBalance = _c.sent();
                        remainingBalance = makerBalance.minus(signedOrder.makerAssetAmount.minus(1));
                        _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                        return [4 /*yield*/, contractWrappers.erc20Token.transferAsync(makerTokenAddress, makerAddress, constants_1.constants.NULL_ADDRESS, remainingBalance)];
                    case 2: return [4 /*yield*/, _b.apply(_a, [_c.sent()])];
                    case 3:
                        _c.sent();
                        // An amount is still transferrable
                        return [4 /*yield*/, contractWrappers.exchange.validateOrderFillableOrThrowAsync(signedOrder, {
                                validateRemainingOrderAmountIsFillable: false,
                            })];
                    case 4:
                        // An amount is still transferrable
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw when the ERC20 token has transfer restrictions', function () { return __awaiter(_this, void 0, void 0, function () {
            var untransferrableToken, untransferrableMakerAssetData, invalidSignedOrder, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, abi_gen_wrappers_1.DummyERC20TokenContract.deployFrom0xArtifactAsync(UntransferrableDummyERC20Token_1.UntransferrableDummyERC20Token, web3_wrapper_1.provider, { from: userAddresses[0] }, 'UntransferrableToken', 'UTT', new utils_1.BigNumber(constants_1.constants.ZRX_DECIMALS), 
                        // tslint:disable-next-line:custom-no-magic-numbers
                        new utils_1.BigNumber(2).pow(20).minus(1))];
                    case 1:
                        untransferrableToken = _c.sent();
                        untransferrableMakerAssetData = order_utils_1.assetDataUtils.encodeERC20AssetData(untransferrableToken.address);
                        return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(untransferrableMakerAssetData, takerAssetData, makerAddress, takerAddress, fillableAmount)];
                    case 2:
                        invalidSignedOrder = _c.sent();
                        _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                        return [4 /*yield*/, contractWrappers.erc20Token.setProxyAllowanceAsync(untransferrableToken.address, makerAddress, signedOrder.makerAssetAmount)];
                    case 3: return [4 /*yield*/, _b.apply(_a, [_c.sent()])];
                    case 4:
                        _c.sent();
                        return [2 /*return*/, expect(contractWrappers.exchange.validateOrderFillableOrThrowAsync(invalidSignedOrder)).to.eventually.to.be.rejectedWith('TRANSFER_FAILED')];
                }
            });
        }); });
    });
    describe('#isValidSignature', function () {
        it('should check if the signature is valid', function () { return __awaiter(_this, void 0, void 0, function () {
            var orderHash, isValid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        orderHash = order_utils_1.orderHashUtils.getOrderHashHex(signedOrder);
                        return [4 /*yield*/, contractWrappers.exchange.isValidSignatureAsync(orderHash, signedOrder.makerAddress, signedOrder.signature)];
                    case 1:
                        isValid = _a.sent();
                        expect(isValid).to.be.true();
                        return [4 /*yield*/, contractWrappers.exchange.isValidSignatureAsync(orderHash, signedOrder.takerAddress, signedOrder.signature)];
                    case 2:
                        isValid = _a.sent();
                        expect(isValid).to.be.false();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#isAllowedValidatorAsync', function () {
        it('should check if the validator is allowed', function () { return __awaiter(_this, void 0, void 0, function () {
            var signerAddress, validatorAddress, isAllowed;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        signerAddress = makerAddress;
                        validatorAddress = constants_1.constants.NULL_ADDRESS;
                        return [4 /*yield*/, contractWrappers.exchange.isAllowedValidatorAsync(signerAddress, validatorAddress)];
                    case 1:
                        isAllowed = _a.sent();
                        expect(isAllowed).to.be.false();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#setSignatureValidatorApproval', function () {
        it('should set signature validator approval', function () { return __awaiter(_this, void 0, void 0, function () {
            var validatorAddress, isApproved, senderAddress;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        validatorAddress = constants_1.constants.NULL_ADDRESS;
                        isApproved = true;
                        senderAddress = makerAddress;
                        return [4 /*yield*/, contractWrappers.exchange.setSignatureValidatorApprovalAsync(validatorAddress, isApproved, senderAddress)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#isTransactionExecutedAsync', function () {
        it('should check if the transaction is executed', function () { return __awaiter(_this, void 0, void 0, function () {
            var transactionHash, isExecuted;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        transactionHash = '0x0000000000000000000000000000000000000000000000000000000000000000';
                        return [4 /*yield*/, contractWrappers.exchange.isTransactionExecutedAsync(transactionHash)];
                    case 1:
                        isExecuted = _a.sent();
                        expect(isExecuted).to.be.false();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#getAssetProxyBySignatureAsync', function () {
        it('should fill or kill a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
            var erc20ProxyId, erc20ProxyAddressById, erc20ProxyAddress, erc721ProxyId, erc721ProxyAddressById, erc721ProxyAddress;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.erc20Proxy.getProxyIdAsync()];
                    case 1:
                        erc20ProxyId = _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.getAssetProxyBySignatureAsync(erc20ProxyId)];
                    case 2:
                        erc20ProxyAddressById = _a.sent();
                        erc20ProxyAddress = contractWrappers.erc20Proxy.address;
                        expect(erc20ProxyAddressById).to.be.equal(erc20ProxyAddress);
                        return [4 /*yield*/, contractWrappers.erc721Proxy.getProxyIdAsync()];
                    case 3:
                        erc721ProxyId = _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.getAssetProxyBySignatureAsync(erc721ProxyId)];
                    case 4:
                        erc721ProxyAddressById = _a.sent();
                        erc721ProxyAddress = contractWrappers.erc721Proxy.address;
                        expect(erc721ProxyAddressById).to.be.equal(erc721ProxyAddress);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#preSignAsync/isPreSignedAsync', function () {
        it('should preSign the hash', function () { return __awaiter(_this, void 0, void 0, function () {
            var senderAddress, hash, signerAddress, isPreSigned, preSignedSignature, isValidSignature, isValidSignatureInTs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        senderAddress = takerAddress;
                        hash = order_utils_1.orderHashUtils.getOrderHashHex(signedOrder);
                        signerAddress = signedOrder.makerAddress;
                        return [4 /*yield*/, contractWrappers.exchange.isPreSignedAsync(hash, signerAddress)];
                    case 1:
                        isPreSigned = _a.sent();
                        expect(isPreSigned).to.be.false();
                        return [4 /*yield*/, contractWrappers.exchange.preSignAsync(hash, signerAddress, signedOrder.signature, senderAddress)];
                    case 2:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.isPreSignedAsync(hash, signerAddress)];
                    case 4:
                        isPreSigned = _a.sent();
                        expect(isPreSigned).to.be.true();
                        preSignedSignature = '0x06';
                        return [4 /*yield*/, contractWrappers.exchange.isValidSignatureAsync(hash, signerAddress, preSignedSignature)];
                    case 5:
                        isValidSignature = _a.sent();
                        expect(isValidSignature).to.be.true();
                        return [4 /*yield*/, order_utils_1.signatureUtils.isValidSignatureAsync(web3_wrapper_1.provider, hash, preSignedSignature, signerAddress)];
                    case 6:
                        isValidSignatureInTs = _a.sent();
                        expect(isValidSignatureInTs).to.be.true();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#getVersionAsync', function () {
        it('should return version the hash', function () { return __awaiter(_this, void 0, void 0, function () {
            var version, VERSION;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.getVersionAsync()];
                    case 1:
                        version = _a.sent();
                        VERSION = '2.0.0';
                        expect(version).to.be.equal(VERSION);
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('#subscribe', function () {
        var indexFilterValues = {};
        var takerTokenFillAmountInBaseUnits = new utils_1.BigNumber(1);
        beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, makerAddress, takerAddress, fillableAmount)];
                    case 1:
                        signedOrder = _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                contractWrappers.exchange.unsubscribeAll();
                return [2 /*return*/];
            });
        }); });
        // Hack: Mocha does not allow a test to be both async and have a `done` callback
        // Since we need to await the receipt of the event in the `subscribe` callback,
        // we do need both. A hack is to make the top-level a sync fn w/ a done callback and then
        // wrap the rest of the test in an async block
        // Source: https://github.com/mochajs/mocha/issues/2407
        it('Should receive the Fill event when an order is filled', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callback;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            callback = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                expect(logEvent.log.event).to.be.equal(src_1.ExchangeEvents.Fill);
                            });
                            contractWrappers.exchange.subscribe(src_1.ExchangeEvents.Fill, indexFilterValues, callback);
                            return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmountInBaseUnits, takerAddress)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Should receive the LogCancel event when an order is cancelled', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callback;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            callback = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                expect(logEvent.log.event).to.be.equal(src_1.ExchangeEvents.Cancel);
                            });
                            contractWrappers.exchange.subscribe(src_1.ExchangeEvents.Cancel, indexFilterValues, callback);
                            return [4 /*yield*/, contractWrappers.exchange.cancelOrderAsync(signedOrder)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Outstanding subscriptions are cancelled when contractWrappers.unsubscribeAll called', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callbackNeverToBeCalled, callback;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            callbackNeverToBeCalled = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                done(new Error('Expected this subscription to have been cancelled'));
                            });
                            contractWrappers.exchange.subscribe(src_1.ExchangeEvents.Fill, indexFilterValues, callbackNeverToBeCalled);
                            contractWrappers.unsubscribeAll();
                            callback = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (logEvent) {
                                expect(logEvent.log.event).to.be.equal(src_1.ExchangeEvents.Fill);
                            });
                            contractWrappers.exchange.subscribe(src_1.ExchangeEvents.Fill, indexFilterValues, callback);
                            return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmountInBaseUnits, takerAddress)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
        it('Should cancel subscription when unsubscribe called', function (done) {
            (function () { return __awaiter(_this, void 0, void 0, function () {
                var callbackNeverToBeCalled, subscriptionToken;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            callbackNeverToBeCalled = dev_utils_1.callbackErrorReporter.reportNodeCallbackErrors(done)(function (_logEvent) {
                                done(new Error('Expected this subscription to have been cancelled'));
                            });
                            subscriptionToken = contractWrappers.exchange.subscribe(src_1.ExchangeEvents.Fill, indexFilterValues, callbackNeverToBeCalled);
                            contractWrappers.exchange.unsubscribe(subscriptionToken);
                            return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmountInBaseUnits, takerAddress)];
                        case 1:
                            _a.sent();
                            done();
                            return [2 /*return*/];
                    }
                });
            }); })().catch(done);
        });
    });
    describe('#getLogsAsync', function () {
        var blockRange = {
            fromBlock: 0,
            toBlock: ethereum_types_1.BlockParamLiteral.Latest,
        };
        it('should get logs with decoded args emitted by Fill', function () { return __awaiter(_this, void 0, void 0, function () {
            var eventName, indexFilterValues, logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                    case 1:
                        txHash = _a.sent();
                        eventName = src_1.ExchangeEvents.Fill;
                        indexFilterValues = {};
                        return [4 /*yield*/, contractWrappers.exchange.getLogsAsync(eventName, blockRange, indexFilterValues)];
                    case 2:
                        logs = _a.sent();
                        expect(logs).to.have.length(1);
                        expect(logs[0].event).to.be.equal(eventName);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should only get the logs with the correct event name', function () { return __awaiter(_this, void 0, void 0, function () {
            var differentEventName, indexFilterValues, logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        differentEventName = src_1.ExchangeEvents.Cancel;
                        indexFilterValues = {};
                        return [4 /*yield*/, contractWrappers.exchange.getLogsAsync(differentEventName, blockRange, indexFilterValues)];
                    case 3:
                        logs = _a.sent();
                        expect(logs).to.have.length(0);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should only get the logs with the correct indexed fields', function () { return __awaiter(_this, void 0, void 0, function () {
            var signedOrderWithAnotherMakerAddress, eventName, indexFilterValues, logs, args;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, fillScenarios.createFillableSignedOrderAsync(makerAssetData, takerAssetData, anotherMakerAddress, takerAddress, fillableAmount)];
                    case 3:
                        signedOrderWithAnotherMakerAddress = _a.sent();
                        return [4 /*yield*/, contractWrappers.exchange.fillOrderAsync(signedOrderWithAnotherMakerAddress, takerTokenFillAmount, takerAddress)];
                    case 4:
                        txHash = _a.sent();
                        return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 5:
                        _a.sent();
                        eventName = src_1.ExchangeEvents.Fill;
                        indexFilterValues = {
                            makerAddress: anotherMakerAddress,
                        };
                        return [4 /*yield*/, contractWrappers.exchange.getLogsAsync(eventName, blockRange, indexFilterValues)];
                    case 6:
                        logs = _a.sent();
                        expect(logs).to.have.length(1);
                        args = logs[0].args;
                        expect(args.makerAddress).to.be.equal(anotherMakerAddress);
                        return [2 /*return*/];
                }
            });
        }); });
    });
}); // tslint:disable:max-file-line-count
//# sourceMappingURL=exchange_wrapper_test.js.map