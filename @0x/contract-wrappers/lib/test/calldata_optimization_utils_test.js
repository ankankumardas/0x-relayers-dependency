"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var order_factory_1 = require("@0x/order-utils/lib/src/order_factory");
var chai = require("chai");
var _ = require("lodash");
require("mocha");
var calldata_optimization_utils_1 = require("../src/utils/calldata_optimization_utils");
var constants_1 = require("../src/utils/constants");
var chai_setup_1 = require("./utils/chai_setup");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
// utility for generating a set of order objects with mostly NULL values
// except for a specified makerAssetData and takerAssetData
var FAKE_ORDERS_COUNT = 5;
var generateFakeOrders = function (makerAssetData, takerAssetData) {
    return _.map(_.range(FAKE_ORDERS_COUNT), function (index) {
        var order = order_factory_1.orderFactory.createOrder(constants_1.constants.NULL_ADDRESS, constants_1.constants.ZERO_AMOUNT, makerAssetData, constants_1.constants.ZERO_AMOUNT, takerAssetData, constants_1.constants.NULL_ADDRESS);
        return __assign({}, order, { signature: 'dummy signature' });
    });
};
describe('calldataOptimizationUtils', function () {
    var fakeMakerAssetData = 'fakeMakerAssetData';
    var fakeTakerAssetData = 'fakeTakerAssetData';
    var orders = generateFakeOrders(fakeMakerAssetData, fakeTakerAssetData);
    describe('#optimizeForwarderOrders', function () {
        it('should make makerAssetData `0x` unless first order', function () {
            var optimizedOrders = calldata_optimization_utils_1.calldataOptimizationUtils.optimizeForwarderOrders(orders);
            expect(optimizedOrders[0].makerAssetData).to.equal(fakeMakerAssetData);
            var ordersWithoutHead = _.slice(optimizedOrders, 1);
            _.forEach(ordersWithoutHead, function (order) { return expect(order.makerAssetData).to.equal(constants_1.constants.NULL_BYTES); });
        });
        it('should make all takerAssetData `0x`', function () {
            var optimizedOrders = calldata_optimization_utils_1.calldataOptimizationUtils.optimizeForwarderOrders(orders);
            _.forEach(optimizedOrders, function (order) { return expect(order.takerAssetData).to.equal(constants_1.constants.NULL_BYTES); });
        });
    });
    describe('#optimizeForwarderFeeOrders', function () {
        it('should make all makerAssetData `0x`', function () {
            var optimizedOrders = calldata_optimization_utils_1.calldataOptimizationUtils.optimizeForwarderFeeOrders(orders);
            _.forEach(optimizedOrders, function (order) { return expect(order.makerAssetData).to.equal(constants_1.constants.NULL_BYTES); });
        });
        it('should make all takerAssetData `0x`', function () {
            var optimizedOrders = calldata_optimization_utils_1.calldataOptimizationUtils.optimizeForwarderFeeOrders(orders);
            _.forEach(optimizedOrders, function (order) { return expect(order.takerAssetData).to.equal(constants_1.constants.NULL_BYTES); });
        });
    });
});
//# sourceMappingURL=calldata_optimization_utils_test.js.map