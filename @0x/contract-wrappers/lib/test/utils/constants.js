"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("@0x/utils");
exports.constants = {
    NULL_ADDRESS: '0x0000000000000000000000000000000000000000',
    ROPSTEN_NETWORK_ID: 3,
    KOVAN_NETWORK_ID: 42,
    TESTRPC_NETWORK_ID: 50,
    AWAIT_TRANSACTION_MINED_MS: 0,
    KOVAN_RPC_URL: 'https://kovan.infura.io/',
    ROPSTEN_RPC_URL: 'https://ropsten.infura.io/',
    ZRX_DECIMALS: 18,
    DUMMY_TOKEN_NAME: '',
    DUMMY_TOKEN_SYMBOL: '',
    DUMMY_TOKEN_DECIMALS: 18,
    DUMMY_TOKEN_TOTAL_SUPPLY: new utils_1.BigNumber(Math.pow(10, 27)),
    NUM_DUMMY_ERC20_TO_DEPLOY: 3,
    NUM_DUMMY_ERC721_TO_DEPLOY: 1,
    ZERO_AMOUNT: new utils_1.BigNumber(0),
};
//# sourceMappingURL=constants.js.map