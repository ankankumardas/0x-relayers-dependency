import { BigNumber } from '@0x/utils';
export declare const constants: {
    NULL_ADDRESS: string;
    ROPSTEN_NETWORK_ID: number;
    KOVAN_NETWORK_ID: number;
    TESTRPC_NETWORK_ID: number;
    AWAIT_TRANSACTION_MINED_MS: number;
    KOVAN_RPC_URL: string;
    ROPSTEN_RPC_URL: string;
    ZRX_DECIMALS: number;
    DUMMY_TOKEN_NAME: string;
    DUMMY_TOKEN_SYMBOL: string;
    DUMMY_TOKEN_DECIMALS: number;
    DUMMY_TOKEN_TOTAL_SUPPLY: BigNumber;
    NUM_DUMMY_ERC20_TO_DEPLOY: number;
    NUM_DUMMY_ERC721_TO_DEPLOY: number;
    ZERO_AMOUNT: BigNumber;
};
//# sourceMappingURL=constants.d.ts.map