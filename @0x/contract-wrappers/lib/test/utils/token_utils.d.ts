import { BigNumber } from '@0x/utils';
export declare const tokenUtils: {
    getDummyERC20TokenAddresses(): string[];
    getDummyERC721TokenAddresses(): string[];
    mintDummyERC721Async(address: string, tokenOwner: string): Promise<BigNumber>;
};
//# sourceMappingURL=token_utils.d.ts.map