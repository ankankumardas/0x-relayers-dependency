import { ContractAddresses } from '@0x/contract-addresses';
/**
 * Configures and runs the migrations exactly once. Any subsequent times this is
 * called, it returns the cached addresses.
 * @returns The addresses of contracts that were deployed during the migrations.
 */
export declare function migrateOnceAsync(): Promise<ContractAddresses>;
//# sourceMappingURL=migrate.d.ts.map