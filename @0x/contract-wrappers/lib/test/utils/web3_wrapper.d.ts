/// <reference types="web3-provider-engine" />
import { Web3ProviderEngine } from '@0x/subproviders';
import { Web3Wrapper } from '@0x/web3-wrapper';
declare const txDefaults: {
    from: string;
    gas: number;
};
declare const provider: Web3ProviderEngine;
declare const web3Wrapper: Web3Wrapper;
export { provider, web3Wrapper, txDefaults };
//# sourceMappingURL=web3_wrapper.d.ts.map