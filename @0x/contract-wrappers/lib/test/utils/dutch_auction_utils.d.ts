import { SignedOrder } from '@0x/types';
import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
export declare class DutchAuctionUtils {
    private readonly _web3Wrapper;
    private readonly _coinbase;
    private readonly _exchangeAddress;
    private readonly _erc20ProxyAddress;
    constructor(web3Wrapper: Web3Wrapper, coinbase: string, exchangeAddress: string, erc20ProxyAddress: string);
    createSignedSellOrderAsync(auctionBeginTimeSections: BigNumber, acutionEndTimeSeconds: BigNumber, auctionBeginTakerAssetAmount: BigNumber, auctionEndTakerAssetAmount: BigNumber, makerAssetAmount: BigNumber, makerAssetData: string, takerAssetData: string, makerAddress: string, takerAddress: string, senderAddress?: string, makerFee?: BigNumber, takerFee?: BigNumber, feeRecipientAddress?: string): Promise<SignedOrder>;
    createSignedBuyOrderAsync(sellOrder: SignedOrder, buyerAddress: string, senderAddress?: string, makerFee?: BigNumber, takerFee?: BigNumber, feeRecipientAddress?: string, expirationTimeSeconds?: BigNumber): Promise<SignedOrder>;
    private _increaseERC20BalanceAndAllowanceAsync;
    private _increaseERC20BalanceAsync;
    private _increaseERC20AllowanceAsync;
}
//# sourceMappingURL=dutch_auction_utils.d.ts.map