"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var artifacts = require("@0x/contract-artifacts");
var order_utils_1 = require("@0x/order-utils");
var order_factory_1 = require("@0x/order-utils/lib/src/order_factory");
var dutch_auction_wrapper_1 = require("../../src/contract_wrappers/dutch_auction_wrapper");
var constants_1 = require("./constants");
var DutchAuctionUtils = /** @class */ (function () {
    function DutchAuctionUtils(web3Wrapper, coinbase, exchangeAddress, erc20ProxyAddress) {
        this._web3Wrapper = web3Wrapper;
        this._coinbase = coinbase;
        this._exchangeAddress = exchangeAddress;
        this._erc20ProxyAddress = erc20ProxyAddress;
    }
    DutchAuctionUtils.prototype.createSignedSellOrderAsync = function (auctionBeginTimeSections, acutionEndTimeSeconds, auctionBeginTakerAssetAmount, auctionEndTakerAssetAmount, makerAssetAmount, makerAssetData, takerAssetData, makerAddress, takerAddress, senderAddress, makerFee, takerFee, feeRecipientAddress) {
        return __awaiter(this, void 0, void 0, function () {
            var makerAssetDataWithAuctionDetails, signedOrder, erc20AssetData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        makerAssetDataWithAuctionDetails = dutch_auction_wrapper_1.DutchAuctionWrapper.encodeDutchAuctionAssetData(makerAssetData, auctionBeginTimeSections, auctionBeginTakerAssetAmount);
                        return [4 /*yield*/, order_factory_1.orderFactory.createSignedOrderAsync(this._web3Wrapper.getProvider(), makerAddress, makerAssetAmount, makerAssetDataWithAuctionDetails, auctionEndTakerAssetAmount, takerAssetData, this._exchangeAddress, {
                                takerAddress: takerAddress,
                                senderAddress: senderAddress,
                                makerFee: makerFee,
                                takerFee: takerFee,
                                feeRecipientAddress: feeRecipientAddress,
                                expirationTimeSeconds: acutionEndTimeSeconds,
                            })];
                    case 1:
                        signedOrder = _a.sent();
                        erc20AssetData = order_utils_1.assetDataUtils.decodeERC20AssetData(makerAssetData);
                        return [4 /*yield*/, this._increaseERC20BalanceAndAllowanceAsync(erc20AssetData.tokenAddress, makerAddress, makerAssetAmount)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, signedOrder];
                }
            });
        });
    };
    DutchAuctionUtils.prototype.createSignedBuyOrderAsync = function (sellOrder, buyerAddress, senderAddress, makerFee, takerFee, feeRecipientAddress, expirationTimeSeconds) {
        return __awaiter(this, void 0, void 0, function () {
            var dutchAuctionData, signedOrder, buyerERC20AssetData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        dutchAuctionData = dutch_auction_wrapper_1.DutchAuctionWrapper.decodeDutchAuctionData(sellOrder.makerAssetData);
                        return [4 /*yield*/, order_factory_1.orderFactory.createSignedOrderAsync(this._web3Wrapper.getProvider(), buyerAddress, dutchAuctionData.beginAmount, sellOrder.takerAssetData, sellOrder.makerAssetAmount, sellOrder.makerAssetData, sellOrder.exchangeAddress, {
                                senderAddress: senderAddress,
                                makerFee: makerFee,
                                takerFee: takerFee,
                                feeRecipientAddress: feeRecipientAddress,
                                expirationTimeSeconds: expirationTimeSeconds,
                            })];
                    case 1:
                        signedOrder = _a.sent();
                        buyerERC20AssetData = order_utils_1.assetDataUtils.decodeERC20AssetData(sellOrder.takerAssetData);
                        return [4 /*yield*/, this._increaseERC20BalanceAndAllowanceAsync(buyerERC20AssetData.tokenAddress, buyerAddress, dutchAuctionData.beginAmount)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, signedOrder];
                }
            });
        });
    };
    DutchAuctionUtils.prototype._increaseERC20BalanceAndAllowanceAsync = function (tokenAddress, address, amount) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (amount.isZero() || address === constants_1.constants.NULL_ADDRESS) {
                            return [2 /*return*/]; // noop
                        }
                        return [4 /*yield*/, Promise.all([
                                this._increaseERC20BalanceAsync(tokenAddress, address, amount),
                                this._increaseERC20AllowanceAsync(tokenAddress, address, amount),
                            ])];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DutchAuctionUtils.prototype._increaseERC20BalanceAsync = function (tokenAddress, address, amount) {
        return __awaiter(this, void 0, void 0, function () {
            var erc20Token, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        erc20Token = new abi_gen_wrappers_1.DummyERC20TokenContract(artifacts.DummyERC20Token.compilerOutput.abi, tokenAddress, this._web3Wrapper.getProvider(), this._web3Wrapper.getContractDefaults());
                        return [4 /*yield*/, erc20Token.transfer.sendTransactionAsync(address, amount, {
                                from: this._coinbase,
                            })];
                    case 1:
                        txHash = _a.sent();
                        return [4 /*yield*/, this._web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DutchAuctionUtils.prototype._increaseERC20AllowanceAsync = function (tokenAddress, address, amount) {
        return __awaiter(this, void 0, void 0, function () {
            var erc20Token, oldMakerAllowance, newMakerAllowance, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        erc20Token = new abi_gen_wrappers_1.DummyERC20TokenContract(artifacts.DummyERC20Token.compilerOutput.abi, tokenAddress, this._web3Wrapper.getProvider(), this._web3Wrapper.getContractDefaults());
                        return [4 /*yield*/, erc20Token.allowance.callAsync(address, this._erc20ProxyAddress)];
                    case 1:
                        oldMakerAllowance = _a.sent();
                        newMakerAllowance = oldMakerAllowance.plus(amount);
                        return [4 /*yield*/, erc20Token.approve.sendTransactionAsync(this._erc20ProxyAddress, newMakerAllowance, {
                                from: address,
                            })];
                    case 2:
                        txHash = _a.sent();
                        return [4 /*yield*/, this._web3Wrapper.awaitTransactionSuccessAsync(txHash, constants_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return DutchAuctionUtils;
}());
exports.DutchAuctionUtils = DutchAuctionUtils;
//# sourceMappingURL=dutch_auction_utils.js.map