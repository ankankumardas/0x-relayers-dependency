"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var contract_artifacts_1 = require("@0x/contract-artifacts");
var order_utils_1 = require("@0x/order-utils");
var web3_wrapper_1 = require("./web3_wrapper");
// Those addresses come from migrations. They're deterministic so it's relatively safe to hard-code them here.
// Before we were fetching them from the TokenRegistry but now we can't as it's deprecated and removed.
// TODO(albrow): Import these from the migrations package instead of hard-coding them.
var DUMMY_ERC_20_ADRESSES = [
    '0x34d402f14d58e001d8efbe6585051bf9706aa064',
    '0x25b8fe1de9daf8ba351890744ff28cf7dfa8f5e3',
    '0xcdb594a32b1cc3479d8746279712c39d18a07fc0',
    '0x1e2f9e10d02a6b8f8f69fcbf515e75039d2ea30d',
    '0xbe0037eaf2d64fe5529bca93c18c9702d3930376',
];
var DUMMY_ERC_721_ADRESSES = ['0x07f96aa816c1f244cbc6ef114bb2b023ba54a2eb'];
exports.tokenUtils = {
    getDummyERC20TokenAddresses: function () {
        return DUMMY_ERC_20_ADRESSES;
    },
    getDummyERC721TokenAddresses: function () {
        return DUMMY_ERC_721_ADRESSES;
    },
    mintDummyERC721Async: function (address, tokenOwner) {
        return __awaiter(this, void 0, void 0, function () {
            var erc721, tokenId, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        erc721 = new abi_gen_wrappers_1.DummyERC721TokenContract(contract_artifacts_1.DummyERC721Token.compilerOutput.abi, address, web3_wrapper_1.provider, web3_wrapper_1.txDefaults);
                        tokenId = order_utils_1.generatePseudoRandomSalt();
                        return [4 /*yield*/, erc721.mint.sendTransactionAsync(tokenOwner, tokenId)];
                    case 1:
                        txHash = _a.sent();
                        web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash);
                        return [2 /*return*/, tokenId];
                }
            });
        });
    },
};
//# sourceMappingURL=token_utils.js.map