"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var contract_artifacts_1 = require("@0x/contract-artifacts");
var contracts_test_utils_1 = require("@0x/contracts-test-utils");
var coordinator_server_1 = require("@0x/coordinator-server");
var dev_utils_1 = require("@0x/dev-utils");
var fill_scenarios_1 = require("@0x/fill-scenarios");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var chai = require("chai");
require("mocha");
var nock = require("nock");
var src_1 = require("../src");
var coordinator_server_types_1 = require("../src/utils/coordinator_server_types");
var chai_setup_1 = require("./utils/chai_setup");
var migrate_1 = require("./utils/migrate");
var token_utils_1 = require("./utils/token_utils");
var web3_wrapper_1 = require("./utils/web3_wrapper");
chai_setup_1.chaiSetup.configure();
var expect = chai.expect;
var blockchainLifecycle = new dev_utils_1.BlockchainLifecycle(web3_wrapper_1.web3Wrapper);
var coordinatorPort = '3000';
var anotherCoordinatorPort = '4000';
var coordinatorEndpoint = 'http://localhost:';
// tslint:disable:custom-no-magic-numbers
describe('CoordinatorWrapper', function () {
    var fillableAmount = new utils_1.BigNumber(5);
    var takerTokenFillAmount = new utils_1.BigNumber(5);
    var coordinatorServerApp;
    var anotherCoordinatorServerApp;
    var contractWrappers;
    var fillScenarios;
    var exchangeContractAddress;
    var zrxTokenAddress;
    var userAddresses;
    var makerAddress;
    var takerAddress;
    var feeRecipientAddressOne;
    var feeRecipientAddressTwo;
    var feeRecipientAddressThree;
    var feeRecipientAddressFour;
    var makerTokenAddress;
    var takerTokenAddress;
    var makerAssetData;
    var takerAssetData;
    var txHash;
    var signedOrder;
    var anotherSignedOrder;
    var signedOrderWithDifferentFeeRecipient;
    var signedOrderWithDifferentCoordinatorOperator;
    var coordinatorRegistryInstance;
    // for testing server error responses
    var serverValidationError;
    var serverCancellationSuccess;
    var serverApprovalSuccess;
    before(function () { return __awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, _d, _e, contractAddresses, config, coordinatorServerConfigs, _f, _g, _h, _j, _k, _l;
        return __generator(this, function (_m) {
            switch (_m.label) {
                case 0: return [4 /*yield*/, migrate_1.migrateOnceAsync()];
                case 1:
                    contractAddresses = _m.sent();
                    return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 2:
                    _m.sent();
                    config = {
                        networkId: contracts_test_utils_1.constants.TESTRPC_NETWORK_ID,
                        contractAddresses: contractAddresses,
                        blockPollingIntervalMs: 10,
                    };
                    contractWrappers = new src_1.ContractWrappers(web3_wrapper_1.provider, config);
                    exchangeContractAddress = contractWrappers.exchange.address;
                    return [4 /*yield*/, web3_wrapper_1.web3Wrapper.getAvailableAddressesAsync()];
                case 3:
                    userAddresses = _m.sent();
                    zrxTokenAddress = contractWrappers.exchange.zrxTokenAddress;
                    fillScenarios = new fill_scenarios_1.FillScenarios(web3_wrapper_1.provider, userAddresses, zrxTokenAddress, exchangeContractAddress, contractWrappers.erc20Proxy.address, contractWrappers.erc721Proxy.address);
                    _a = __read(userAddresses.slice(0, 7), 7), makerAddress = _a[1], takerAddress = _a[2], feeRecipientAddressOne = _a[3], feeRecipientAddressTwo = _a[4], feeRecipientAddressThree = _a[5], feeRecipientAddressFour = _a[6];
                    _b = __read(token_utils_1.tokenUtils.getDummyERC20TokenAddresses(), 2), makerTokenAddress = _b[0], takerTokenAddress = _b[1];
                    _c = __read([
                        order_utils_1.assetDataUtils.encodeERC20AssetData(makerTokenAddress),
                        order_utils_1.assetDataUtils.encodeERC20AssetData(takerTokenAddress),
                    ], 2), makerAssetData = _c[0], takerAssetData = _c[1];
                    coordinatorServerConfigs = {
                        HTTP_PORT: 3000,
                        NETWORK_ID_TO_SETTINGS: {
                            50: {
                                FEE_RECIPIENTS: [
                                    {
                                        ADDRESS: feeRecipientAddressOne,
                                        PRIVATE_KEY: contracts_test_utils_1.constants.TESTRPC_PRIVATE_KEYS[userAddresses.indexOf(feeRecipientAddressOne)].toString('hex'),
                                    },
                                    {
                                        ADDRESS: feeRecipientAddressTwo,
                                        PRIVATE_KEY: contracts_test_utils_1.constants.TESTRPC_PRIVATE_KEYS[userAddresses.indexOf(feeRecipientAddressTwo)].toString('hex'),
                                    },
                                    {
                                        ADDRESS: feeRecipientAddressThree,
                                        PRIVATE_KEY: contracts_test_utils_1.constants.TESTRPC_PRIVATE_KEYS[userAddresses.indexOf(feeRecipientAddressThree)].toString('hex'),
                                    },
                                ],
                                // Ethereum RPC url, only used in the default instantiation in 0x-coordinator-server/server.js
                                // Not used here when instantiating with the imported app
                                RPC_URL: 'http://ignore',
                            },
                        },
                        // Optional selective delay on fill requests
                        SELECTIVE_DELAY_MS: 0,
                        EXPIRATION_DURATION_SECONDS: 60,
                    };
                    return [4 /*yield*/, coordinator_server_1.getAppAsync((_d = {},
                            _d[config.networkId] = web3_wrapper_1.provider,
                            _d), coordinatorServerConfigs, coordinator_server_1.defaultOrmConfig)];
                case 4:
                    coordinatorServerApp = _m.sent();
                    coordinatorServerApp.listen(coordinatorPort, function () {
                        utils_1.logUtils.log("Coordinator SERVER API (HTTP) listening on port " + coordinatorPort + "!");
                    });
                    return [4 /*yield*/, coordinator_server_1.getAppAsync((_e = {},
                            _e[config.networkId] = web3_wrapper_1.provider,
                            _e), coordinatorServerConfigs, {
                            type: 'sqlite',
                            database: 'database.sqlite_2',
                            entities: coordinator_server_1.defaultOrmConfig.entities,
                            cli: coordinator_server_1.defaultOrmConfig.cli,
                            logging: coordinator_server_1.defaultOrmConfig.logging,
                            synchronize: coordinator_server_1.defaultOrmConfig.synchronize,
                        })];
                case 5:
                    anotherCoordinatorServerApp = _m.sent();
                    anotherCoordinatorServerApp.listen(anotherCoordinatorPort, function () {
                        utils_1.logUtils.log("Coordinator SERVER API (HTTP) listening on port " + anotherCoordinatorPort + "!");
                    });
                    // setup coordinator registry
                    coordinatorRegistryInstance = new abi_gen_wrappers_1.CoordinatorRegistryContract(contract_artifacts_1.CoordinatorRegistry.compilerOutput.abi, contractAddresses.coordinatorRegistry, web3_wrapper_1.provider);
                    _g = (_f = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                    return [4 /*yield*/, coordinatorRegistryInstance.setCoordinatorEndpoint.sendTransactionAsync("" + coordinatorEndpoint + coordinatorPort, {
                            from: feeRecipientAddressOne,
                        })];
                case 6: 
                // register coordinator server
                return [4 /*yield*/, _g.apply(_f, [_m.sent(),
                        contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                case 7:
                    // register coordinator server
                    _m.sent();
                    _j = (_h = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                    return [4 /*yield*/, coordinatorRegistryInstance.setCoordinatorEndpoint.sendTransactionAsync("" + coordinatorEndpoint + coordinatorPort, {
                            from: feeRecipientAddressTwo,
                        })];
                case 8: return [4 /*yield*/, _j.apply(_h, [_m.sent(),
                        contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                case 9:
                    _m.sent();
                    _l = (_k = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                    return [4 /*yield*/, coordinatorRegistryInstance.setCoordinatorEndpoint.sendTransactionAsync("" + coordinatorEndpoint + anotherCoordinatorPort, {
                            from: feeRecipientAddressThree,
                        })];
                case 10: 
                // register another coordinator server
                return [4 /*yield*/, _l.apply(_k, [_m.sent(),
                        contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                case 11:
                    // register another coordinator server
                    _m.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    after(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.startAsync()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressOne, undefined, contractWrappers.coordinator.address)];
                case 2:
                    signedOrder = _a.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressOne, undefined, contractWrappers.coordinator.address)];
                case 3:
                    anotherSignedOrder = _a.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressTwo, undefined, contractWrappers.coordinator.address)];
                case 4:
                    signedOrderWithDifferentFeeRecipient = _a.sent();
                    return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressThree, undefined, contractWrappers.coordinator.address)];
                case 5:
                    signedOrderWithDifferentCoordinatorOperator = _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    afterEach(function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, blockchainLifecycle.revertAsync()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    describe('test setup', function () {
        it('should have coordinator registry which returns an endpoint', function () { return __awaiter(_this, void 0, void 0, function () {
            var setCoordinatorEndpoint, anotherSetCoordinatorEndpoint;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, coordinatorRegistryInstance.getCoordinatorEndpoint.callAsync(feeRecipientAddressOne)];
                    case 1:
                        setCoordinatorEndpoint = _a.sent();
                        return [4 /*yield*/, coordinatorRegistryInstance.getCoordinatorEndpoint.callAsync(feeRecipientAddressThree)];
                    case 2:
                        anotherSetCoordinatorEndpoint = _a.sent();
                        expect(setCoordinatorEndpoint).to.be.equal("" + coordinatorEndpoint + coordinatorPort);
                        expect(anotherSetCoordinatorEndpoint).to.be.equal("" + coordinatorEndpoint + anotherCoordinatorPort);
                        return [2 /*return*/];
                }
            });
        }); });
        it('should have coordinator server endpoints which respond to pings', function () { return __awaiter(_this, void 0, void 0, function () {
            var result, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, utils_1.fetchAsync("" + coordinatorEndpoint + coordinatorPort + "/v1/ping")];
                    case 1:
                        result = _c.sent();
                        expect(result.status).to.be.equal(200);
                        _a = expect;
                        return [4 /*yield*/, result.text()];
                    case 2:
                        _a.apply(void 0, [_c.sent()]).to.be.equal('pong');
                        return [4 /*yield*/, utils_1.fetchAsync("" + coordinatorEndpoint + anotherCoordinatorPort + "/v1/ping")];
                    case 3:
                        result = _c.sent();
                        expect(result.status).to.be.equal(200);
                        _b = expect;
                        return [4 /*yield*/, result.text()];
                    case 4:
                        _b.apply(void 0, [_c.sent()]).to.be.equal('pong');
                        return [2 /*return*/];
                }
            });
        }); });
    });
    // fill handling is the same for all fill methods so we can test them all through the fillOrder and batchFillOrders interfaces
    describe('fill order(s)', function () {
        describe('#fillOrderAsync', function () {
            it('should fill a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.coordinator.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchFillOrdersAsync', function () {
            it('should fill a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder];
                            takerAssetFillAmounts = Array(2).fill(takerTokenFillAmount);
                            return [4 /*yield*/, contractWrappers.coordinator.batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
            it('should fill a batch of orders with different feeRecipientAddresses with the same coordinator server', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [signedOrder, anotherSignedOrder, signedOrderWithDifferentFeeRecipient];
                            takerAssetFillAmounts = Array(3).fill(takerTokenFillAmount);
                            return [4 /*yield*/, contractWrappers.coordinator.batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
            // coordinator-server, as currently implemented, shares a singleton database connection across
            // all instantiations. Making the request to two different mocked server endpoints still hits the
            // same database and fails because of the uniqueness constraint on transactions in the database.
            it.skip('should fill a batch of orders with different feeRecipientAddresses with different coordinator servers', function () { return __awaiter(_this, void 0, void 0, function () {
                var signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            signedOrders = [
                                signedOrder,
                                anotherSignedOrder,
                                signedOrderWithDifferentFeeRecipient,
                                signedOrderWithDifferentCoordinatorOperator,
                            ];
                            takerAssetFillAmounts = Array(4).fill(takerTokenFillAmount);
                            return [4 /*yield*/, contractWrappers.coordinator.batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
            it('should fill a batch of mixed coordinator and non-coordinator orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var nonCoordinatorOrder, signedOrders, takerAssetFillAmounts;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressOne, undefined)];
                        case 1:
                            nonCoordinatorOrder = _a.sent();
                            signedOrders = [signedOrder, nonCoordinatorOrder];
                            takerAssetFillAmounts = Array(2).fill(takerTokenFillAmount);
                            return [4 /*yield*/, contractWrappers.coordinator.batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)];
                        case 2:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 3:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
    describe('soft cancel order(s)', function () {
        describe('#softCancelOrderAsync', function () {
            it('should soft cancel a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                var response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.coordinator.softCancelOrderAsync(signedOrder)];
                        case 1:
                            response = _a.sent();
                            expect(response.outstandingFillSignatures).to.have.lengthOf(0);
                            expect(response.cancellationSignatures).to.have.lengthOf(1);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchSoftCancelOrdersAsync', function () {
            it('should soft cancel a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var orders, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            orders = [signedOrder, anotherSignedOrder];
                            return [4 /*yield*/, contractWrappers.coordinator.batchSoftCancelOrdersAsync(orders)];
                        case 1:
                            response = _a.sent();
                            expect(response).to.have.lengthOf(1);
                            expect(response[0].outstandingFillSignatures).to.have.lengthOf(0);
                            expect(response[0].cancellationSignatures).to.have.lengthOf(1);
                            return [2 /*return*/];
                    }
                });
            }); });
            it('should soft cancel a batch of orders with different feeRecipientAddresses', function () { return __awaiter(_this, void 0, void 0, function () {
                var orders, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            orders = [signedOrder, anotherSignedOrder, signedOrderWithDifferentFeeRecipient];
                            return [4 /*yield*/, contractWrappers.coordinator.batchSoftCancelOrdersAsync(orders)];
                        case 1:
                            response = _a.sent();
                            expect(response).to.have.lengthOf(1);
                            expect(response[0].outstandingFillSignatures).to.have.lengthOf(0);
                            expect(response[0].cancellationSignatures).to.have.lengthOf(2);
                            return [2 /*return*/];
                    }
                });
            }); });
            it('should soft cancel a batch of orders with different coordinatorOperator and concatenate responses', function () { return __awaiter(_this, void 0, void 0, function () {
                var orders, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            orders = [
                                signedOrder,
                                anotherSignedOrder,
                                signedOrderWithDifferentFeeRecipient,
                                signedOrderWithDifferentCoordinatorOperator,
                            ];
                            return [4 /*yield*/, contractWrappers.coordinator.batchSoftCancelOrdersAsync(orders)];
                        case 1:
                            response = _a.sent();
                            expect(response).to.have.lengthOf(2);
                            expect(response[0].outstandingFillSignatures).to.have.lengthOf(0);
                            expect(response[0].cancellationSignatures).to.have.lengthOf(3);
                            expect(response[1].cancellationSignatures).to.have.lengthOf(3); // both coordinator servers support the same feeRecipients
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
    describe('hard cancel order(s)', function () {
        describe('#hardCancelOrderAsync', function () {
            it('should hard cancel a valid order', function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contractWrappers.coordinator.hardCancelOrderAsync(signedOrder)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#batchHardCancelOrdersAsync', function () {
            it('should hard cancel a batch of valid orders', function () { return __awaiter(_this, void 0, void 0, function () {
                var orders;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            orders = [signedOrder, anotherSignedOrder];
                            return [4 /*yield*/, contractWrappers.coordinator.batchHardCancelOrdersAsync(orders)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
        describe('#cancelOrdersUpTo/getOrderEpochAsync', function () {
            it('should hard cancel orders up to target order epoch', function () { return __awaiter(_this, void 0, void 0, function () {
                var targetOrderEpoch, orderEpoch;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            targetOrderEpoch = new utils_1.BigNumber(42);
                            return [4 /*yield*/, contractWrappers.coordinator.hardCancelOrdersUpToAsync(targetOrderEpoch, makerAddress)];
                        case 1:
                            txHash = _a.sent();
                            return [4 /*yield*/, web3_wrapper_1.web3Wrapper.awaitTransactionSuccessAsync(txHash, contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, contractWrappers.exchange.getOrderEpochAsync(makerAddress, contractWrappers.coordinator.address)];
                        case 3:
                            orderEpoch = _a.sent();
                            expect(orderEpoch).to.be.bignumber.equal(targetOrderEpoch.plus(1));
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    });
    describe('coordinator edge cases', function () {
        it('should throw error when feeRecipientAddress is not in registry', function () { return __awaiter(_this, void 0, void 0, function () {
            var badOrder;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fillScenarios.createFillableSignedOrderWithFeesAsync(makerAssetData, takerAssetData, new utils_1.BigNumber(1), new utils_1.BigNumber(1), makerAddress, takerAddress, fillableAmount, feeRecipientAddressFour)];
                    case 1:
                        badOrder = _a.sent();
                        expect(contractWrappers.coordinator.fillOrderAsync(badOrder, takerTokenFillAmount, takerAddress)).to.be.rejected();
                        return [2 /*return*/];
                }
            });
        }); });
        it('should throw error when coordinator endpoint is malformed', function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = web3_wrapper_1.web3Wrapper).awaitTransactionSuccessAsync;
                        return [4 /*yield*/, coordinatorRegistryInstance.setCoordinatorEndpoint.sendTransactionAsync('localhost', {
                                from: feeRecipientAddressFour,
                            })];
                    case 1: return [4 /*yield*/, _b.apply(_a, [_c.sent(),
                            contracts_test_utils_1.constants.AWAIT_TRANSACTION_MINED_MS])];
                    case 2:
                        _c.sent();
                        expect(contractWrappers.coordinator.fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)).to.be.rejected();
                        return [2 /*return*/];
                }
            });
        }); });
    });
    describe('coordinator server errors', function () {
        beforeEach('setup', function () {
            serverValidationError = {
                code: 100,
                reason: 'Validation Failed',
                validationErrors: [
                    {
                        field: 'signedTransaction',
                        code: 1011,
                        reason: 'A transaction can only be approved once. To request approval to perform the same actions, generate and sign an identical transaction with a different salt value.',
                    },
                ],
            };
            serverCancellationSuccess = {
                outstandingFillSignatures: [
                    {
                        orderHash: '0xd1dc61f3e7e5f41d72beae7863487beea108971de678ca00d903756f842ef3ce',
                        approvalSignatures: [
                            '0x1c7383ca8ebd6de8b5b20b1c2d49bea166df7dfe4af1932c9c52ec07334e859cf2176901da35f4480ceb3ab63d8d0339d851c31929c40d88752689b9a8a535671303',
                        ],
                        expirationTimeSeconds: 1552390380,
                        takerAssetFillAmount: 100000000000000000000,
                    },
                ],
                cancellationSignatures: [
                    '0x2ea3117a8ebd6de8b5b20b1c2d49bea166df7dfe4af1932c9c52ec07334e859cf2176901da35f4480ceb3ab63d8d0339d851c31929c40d88752689b9a855b5a7b401',
                ],
            };
            serverApprovalSuccess = {
                signatures: [
                    '0x1cc07d7ae39679690a91418d46491520f058e4fb14debdf2e98f2376b3970de8512ace44af0be6d1c65617f7aae8c2364ff63f241515ee1559c3eeecb0f671d9e903',
                ],
                expirationTimeSeconds: 1552390014,
            };
            nock("" + coordinatorEndpoint + coordinatorPort)
                .post('/v1/request_transaction', function () { return true; })
                .query({
                networkId: 50,
            })
                .reply(400, serverValidationError);
        });
        it('should throw error when softCancel fails', function (done) {
            contractWrappers.coordinator
                .softCancelOrderAsync(signedOrder)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw error when batch soft cancel fails with single coordinator operator', function (done) {
            var orders = [signedOrder, signedOrderWithDifferentFeeRecipient];
            contractWrappers.coordinator
                .batchSoftCancelOrdersAsync(orders)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal(orders);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw consolidated error when batch soft cancel partially fails with different coordinator operators', function (done) {
            nock("" + coordinatorEndpoint + anotherCoordinatorPort)
                .post('/v1/request_transaction', function () { return true; })
                .query({
                networkId: 50,
            })
                .reply(200, serverCancellationSuccess);
            var signedOrders = [signedOrder, signedOrderWithDifferentCoordinatorOperator];
            contractWrappers.coordinator
                .batchSoftCancelOrdersAsync(signedOrders)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.deep.equal([serverCancellationSuccess]);
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw consolidated error when batch soft cancel totally fails with different coordinator operators', function (done) {
            nock("" + coordinatorEndpoint + anotherCoordinatorPort)
                .post('/v1/request_transaction', function () { return true; })
                .query({
                networkId: 50,
            })
                .reply(400, serverValidationError);
            var signedOrders = [signedOrder, signedOrderWithDifferentCoordinatorOperator];
            contractWrappers.coordinator
                .batchSoftCancelOrdersAsync(signedOrders)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                var anotherErrorBody = err.errors[1];
                expect(anotherErrorBody.isError).to.be.true();
                expect(anotherErrorBody.status).to.equal(400);
                expect(anotherErrorBody.error).to.deep.equal(serverValidationError);
                expect(anotherErrorBody.orders).to.deep.equal([signedOrderWithDifferentCoordinatorOperator]);
                expect(anotherErrorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + anotherCoordinatorPort);
                done();
            });
        });
        it('should throw error when a fill fails', function (done) {
            contractWrappers.coordinator
                .fillOrderAsync(signedOrder, takerTokenFillAmount, takerAddress)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.FillFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw error when batch fill fails with single coordinator operator', function (done) {
            var signedOrders = [signedOrder, signedOrderWithDifferentFeeRecipient];
            var takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount, takerTokenFillAmount];
            contractWrappers.coordinator
                .batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.FillFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal(signedOrders);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw consolidated error when batch fill partially fails with different coordinator operators', function (done) {
            nock("" + coordinatorEndpoint + anotherCoordinatorPort)
                .post('/v1/request_transaction', function () { return true; })
                .query({
                networkId: 50,
            })
                .reply(200, serverApprovalSuccess);
            var signedOrders = [signedOrder, signedOrderWithDifferentCoordinatorOperator];
            var takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount, takerTokenFillAmount];
            contractWrappers.coordinator
                .batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.FillFailed);
                expect(err.approvedOrders).to.deep.equal([signedOrderWithDifferentCoordinatorOperator]);
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                done();
            });
        });
        it('should throw consolidated error when batch fill totally fails with different coordinator operators', function (done) {
            nock("" + coordinatorEndpoint + anotherCoordinatorPort)
                .post('/v1/request_transaction', function () { return true; })
                .query({
                networkId: 50,
            })
                .reply(400, serverValidationError);
            var signedOrders = [signedOrder, signedOrderWithDifferentCoordinatorOperator];
            var takerAssetFillAmounts = [takerTokenFillAmount, takerTokenFillAmount, takerTokenFillAmount];
            contractWrappers.coordinator
                .batchFillOrdersAsync(signedOrders, takerAssetFillAmounts, takerAddress)
                .then(function (res) {
                expect(res).to.be.undefined();
            })
                .catch(function (err) {
                expect(err.message).equal(coordinator_server_types_1.CoordinatorServerErrorMsg.FillFailed);
                expect(err.approvedOrders).to.be.empty('array');
                expect(err.cancellations).to.be.empty('array');
                var errorBody = err.errors[0];
                expect(errorBody.isError).to.be.true();
                expect(errorBody.status).to.equal(400);
                expect(errorBody.error).to.deep.equal(serverValidationError);
                expect(errorBody.orders).to.deep.equal([signedOrder]);
                expect(errorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + coordinatorPort);
                var anotherErrorBody = err.errors[1];
                expect(anotherErrorBody.isError).to.be.true();
                expect(anotherErrorBody.status).to.equal(400);
                expect(anotherErrorBody.error).to.deep.equal(serverValidationError);
                expect(anotherErrorBody.orders).to.deep.equal([signedOrderWithDifferentCoordinatorOperator]);
                expect(anotherErrorBody.coordinatorOperator).to.equal("" + coordinatorEndpoint + anotherCoordinatorPort);
                done();
            });
        });
    });
});
// tslint:disable:max-file-line-count
//# sourceMappingURL=coordinator_wrapper_test.js.map