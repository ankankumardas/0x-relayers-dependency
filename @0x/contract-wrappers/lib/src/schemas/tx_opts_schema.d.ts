export declare const txOptsSchema: {
    id: string;
    properties: {
        gasPrice: {
            $ref: string;
        };
        gasLimit: {
            type: string;
        };
        nonce: {
            type: string;
        };
    };
    type: string;
};
//# sourceMappingURL=tx_opts_schema.d.ts.map