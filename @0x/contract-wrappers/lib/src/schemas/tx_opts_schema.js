"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.txOptsSchema = {
    id: '/TxOpts',
    properties: {
        gasPrice: { $ref: '/numberSchema' },
        gasLimit: { type: 'number' },
        nonce: { type: 'number' },
    },
    type: 'object',
};
//# sourceMappingURL=tx_opts_schema.js.map