export declare const methodOptsSchema: {
    id: string;
    properties: {
        defaultBlock: {
            $ref: string;
        };
    };
    type: string;
};
//# sourceMappingURL=method_opts_schema.d.ts.map