"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.methodOptsSchema = {
    id: '/MethodOpts',
    properties: {
        defaultBlock: { $ref: '/blockParamSchema' },
    },
    type: 'object',
};
//# sourceMappingURL=method_opts_schema.js.map