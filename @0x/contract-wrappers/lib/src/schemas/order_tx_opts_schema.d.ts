export declare const orderTxOptsSchema: {
    id: string;
    allOf: {
        $ref: string;
    }[];
    properties: {
        shouldValidate: {
            type: string;
        };
    };
    type: string;
};
//# sourceMappingURL=order_tx_opts_schema.d.ts.map