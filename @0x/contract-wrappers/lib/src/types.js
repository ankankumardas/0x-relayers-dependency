"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ExchangeWrapperError;
(function (ExchangeWrapperError) {
    ExchangeWrapperError["AssetDataMismatch"] = "ASSET_DATA_MISMATCH";
})(ExchangeWrapperError = exports.ExchangeWrapperError || (exports.ExchangeWrapperError = {}));
var ForwarderWrapperError;
(function (ForwarderWrapperError) {
    ForwarderWrapperError["CompleteFillFailed"] = "COMPLETE_FILL_FAILED";
})(ForwarderWrapperError = exports.ForwarderWrapperError || (exports.ForwarderWrapperError = {}));
var ContractWrappersError;
(function (ContractWrappersError) {
    ContractWrappersError["ContractNotDeployedOnNetwork"] = "CONTRACT_NOT_DEPLOYED_ON_NETWORK";
    ContractWrappersError["InsufficientAllowanceForTransfer"] = "INSUFFICIENT_ALLOWANCE_FOR_TRANSFER";
    ContractWrappersError["InsufficientBalanceForTransfer"] = "INSUFFICIENT_BALANCE_FOR_TRANSFER";
    ContractWrappersError["InsufficientEthBalanceForDeposit"] = "INSUFFICIENT_ETH_BALANCE_FOR_DEPOSIT";
    ContractWrappersError["InsufficientWEthBalanceForWithdrawal"] = "INSUFFICIENT_WETH_BALANCE_FOR_WITHDRAWAL";
    ContractWrappersError["InvalidJump"] = "INVALID_JUMP";
    ContractWrappersError["OutOfGas"] = "OUT_OF_GAS";
    ContractWrappersError["SubscriptionNotFound"] = "SUBSCRIPTION_NOT_FOUND";
    ContractWrappersError["SubscriptionAlreadyPresent"] = "SUBSCRIPTION_ALREADY_PRESENT";
    ContractWrappersError["ERC721OwnerNotFound"] = "ERC_721_OWNER_NOT_FOUND";
    ContractWrappersError["ERC721NoApproval"] = "ERC_721_NO_APPROVAL";
    ContractWrappersError["SignatureRequestDenied"] = "SIGNATURE_REQUEST_DENIED";
})(ContractWrappersError = exports.ContractWrappersError || (exports.ContractWrappersError = {}));
var InternalContractWrappersError;
(function (InternalContractWrappersError) {
    InternalContractWrappersError["NoAbiDecoder"] = "NO_ABI_DECODER";
})(InternalContractWrappersError = exports.InternalContractWrappersError || (exports.InternalContractWrappersError = {}));
var TradeSide;
(function (TradeSide) {
    TradeSide["Maker"] = "maker";
    TradeSide["Taker"] = "taker";
})(TradeSide = exports.TradeSide || (exports.TradeSide = {}));
var TransferType;
(function (TransferType) {
    TransferType["Trade"] = "trade";
    TransferType["Fee"] = "fee";
})(TransferType = exports.TransferType || (exports.TransferType = {}));
var OrderStatus;
(function (OrderStatus) {
    OrderStatus[OrderStatus["Invalid"] = 0] = "Invalid";
    OrderStatus[OrderStatus["InvalidMakerAssetAmount"] = 1] = "InvalidMakerAssetAmount";
    OrderStatus[OrderStatus["InvalidTakerAssetAmount"] = 2] = "InvalidTakerAssetAmount";
    OrderStatus[OrderStatus["Fillable"] = 3] = "Fillable";
    OrderStatus[OrderStatus["Expired"] = 4] = "Expired";
    OrderStatus[OrderStatus["FullyFilled"] = 5] = "FullyFilled";
    OrderStatus[OrderStatus["Cancelled"] = 6] = "Cancelled";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
var DutchAuctionWrapperError;
(function (DutchAuctionWrapperError) {
    DutchAuctionWrapperError["AssetDataMismatch"] = "ASSET_DATA_MISMATCH";
})(DutchAuctionWrapperError = exports.DutchAuctionWrapperError || (exports.DutchAuctionWrapperError = {}));
var coordinator_server_types_1 = require("./utils/coordinator_server_types");
exports.CoordinatorServerError = coordinator_server_types_1.CoordinatorServerError;
//# sourceMappingURL=types.js.map