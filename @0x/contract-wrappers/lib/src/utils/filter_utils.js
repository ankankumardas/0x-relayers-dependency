"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ethUtil = require("ethereumjs-util");
var jsSHA3 = require("js-sha3");
var _ = require("lodash");
var uuid = require("uuid/v4");
var TOPIC_LENGTH = 32;
exports.filterUtils = {
    generateUUID: function () {
        return uuid();
    },
    getFilter: function (address, eventName, indexFilterValues, abi, blockRange) {
        var eventAbi = _.find(abi, { name: eventName });
        var eventSignature = exports.filterUtils.getEventSignatureFromAbiByName(eventAbi);
        var topicForEventSignature = ethUtil.addHexPrefix(jsSHA3.keccak256(eventSignature));
        var topicsForIndexedArgs = exports.filterUtils.getTopicsForIndexedArgs(eventAbi, indexFilterValues);
        var topics = __spread([topicForEventSignature], topicsForIndexedArgs);
        var filter = {
            address: address,
            topics: topics,
        };
        if (blockRange !== undefined) {
            filter = __assign({}, blockRange, filter);
        }
        return filter;
    },
    getEventSignatureFromAbiByName: function (eventAbi) {
        var types = _.map(eventAbi.inputs, 'type');
        var signature = eventAbi.name + "(" + types.join(',') + ")";
        return signature;
    },
    getTopicsForIndexedArgs: function (abi, indexFilterValues) {
        var e_1, _a;
        var topics = [];
        try {
            for (var _b = __values(abi.inputs), _c = _b.next(); !_c.done; _c = _b.next()) {
                var eventInput = _c.value;
                if (!eventInput.indexed) {
                    continue;
                }
                if (indexFilterValues[eventInput.name] === undefined) {
                    // Null is a wildcard topic in a JSON-RPC call
                    topics.push(null);
                }
                else {
                    var value = indexFilterValues[eventInput.name];
                    var buffer = ethUtil.toBuffer(value);
                    var paddedBuffer = ethUtil.setLengthLeft(buffer, TOPIC_LENGTH);
                    var topic = ethUtil.bufferToHex(paddedBuffer);
                    topics.push(topic);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return topics;
    },
    matchesFilter: function (log, filter) {
        if (filter.address !== undefined && log.address !== filter.address) {
            return false;
        }
        if (filter.topics !== undefined) {
            return exports.filterUtils.doesMatchTopics(log.topics, filter.topics);
        }
        return true;
    },
    doesMatchTopics: function (logTopics, filterTopics) {
        var matchesTopic = _.zipWith(logTopics, filterTopics, exports.filterUtils.matchesTopic.bind(exports.filterUtils));
        var doesMatchTopics = _.every(matchesTopic);
        return doesMatchTopics;
    },
    matchesTopic: function (logTopic, filterTopic) {
        if (_.isArray(filterTopic)) {
            return _.includes(filterTopic, logTopic);
        }
        if (_.isString(filterTopic)) {
            return filterTopic === logTopic;
        }
        // null topic is a wildcard
        return true;
    },
};
//# sourceMappingURL=filter_utils.js.map