import { ContractAddresses } from '@0x/contract-addresses';
/**
 * Returns the default contract addresses for the given networkId or throws with
 * a context-specific error message if the networkId is not recognized.
 */
export declare function _getDefaultContractAddresses(networkId: number): ContractAddresses;
//# sourceMappingURL=contract_addresses.d.ts.map