import { BigNumber } from '@0x/utils';
export declare const constants: {
    NULL_ADDRESS: string;
    NULL_BYTES: string;
    TESTRPC_NETWORK_ID: number;
    INVALID_JUMP_PATTERN: string;
    REVERT: string;
    OUT_OF_GAS_PATTERN: string;
    INVALID_TAKER_FORMAT: string;
    UNLIMITED_ALLOWANCE_IN_BASE_UNITS: BigNumber;
    DEFAULT_BLOCK_POLLING_INTERVAL: number;
    ZERO_AMOUNT: BigNumber;
    ONE_AMOUNT: BigNumber;
    ETHER_TOKEN_DECIMALS: number;
    METAMASK_USER_DENIED_SIGNATURE_PATTERN: string;
    TRUST_WALLET_USER_DENIED_SIGNATURE_PATTERN: string;
};
//# sourceMappingURL=constants.d.ts.map