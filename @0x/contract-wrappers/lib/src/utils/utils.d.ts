import { BigNumber } from '@0x/utils';
export declare const utils: {
    getCurrentUnixTimestampSec(): BigNumber;
    getCurrentUnixTimestampMs(): BigNumber;
    numberPercentageToEtherTokenAmountPercentage(percentage: number): BigNumber;
    removeUndefinedProperties<T extends object>(obj: T): Partial<T>;
};
//# sourceMappingURL=utils.d.ts.map