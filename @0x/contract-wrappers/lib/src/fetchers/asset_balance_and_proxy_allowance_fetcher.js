"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var AssetBalanceAndProxyAllowanceFetcher = /** @class */ (function () {
    function AssetBalanceAndProxyAllowanceFetcher(erc20Token, erc721Token, stateLayer) {
        this._erc20Token = erc20Token;
        this._erc721Token = erc721Token;
        this._stateLayer = stateLayer;
    }
    AssetBalanceAndProxyAllowanceFetcher.prototype.getBalanceAsync = function (assetData, userAddress) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1, _a, decodedAssetData, balance, tokenOwner, _b, _c, _d, index, nestedAssetDataElement, nestedAmountElement, nestedAssetBalance, e_1_1;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        decodedAssetData = order_utils_1.assetDataUtils.decodeAssetDataOrThrow(assetData);
                        if (!order_utils_1.assetDataUtils.isERC20AssetData(decodedAssetData)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._erc20Token.getBalanceAsync(decodedAssetData.tokenAddress, userAddress, {
                                defaultBlock: this._stateLayer,
                            })];
                    case 1:
                        balance = _e.sent();
                        return [3 /*break*/, 12];
                    case 2:
                        if (!order_utils_1.assetDataUtils.isERC721AssetData(decodedAssetData)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._erc721Token.getOwnerOfAsync(decodedAssetData.tokenAddress, decodedAssetData.tokenId, {
                                defaultBlock: this._stateLayer,
                            })];
                    case 3:
                        tokenOwner = _e.sent();
                        balance = tokenOwner === userAddress ? new utils_1.BigNumber(1) : new utils_1.BigNumber(0);
                        return [3 /*break*/, 12];
                    case 4:
                        if (!order_utils_1.assetDataUtils.isMultiAssetData(decodedAssetData)) return [3 /*break*/, 12];
                        _e.label = 5;
                    case 5:
                        _e.trys.push([5, 10, 11, 12]);
                        _b = __values(decodedAssetData.nestedAssetData.entries()), _c = _b.next();
                        _e.label = 6;
                    case 6:
                        if (!!_c.done) return [3 /*break*/, 9];
                        _d = __read(_c.value, 2), index = _d[0], nestedAssetDataElement = _d[1];
                        nestedAmountElement = decodedAssetData.amounts[index];
                        return [4 /*yield*/, this.getBalanceAsync(nestedAssetDataElement, userAddress)];
                    case 7:
                        nestedAssetBalance = (_e.sent()).dividedToIntegerBy(nestedAmountElement);
                        if (balance === undefined || nestedAssetBalance.isLessThan(balance)) {
                            balance = nestedAssetBalance;
                        }
                        _e.label = 8;
                    case 8:
                        _c = _b.next();
                        return [3 /*break*/, 6];
                    case 9: return [3 /*break*/, 12];
                    case 10:
                        e_1_1 = _e.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 12];
                    case 11:
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 12: return [2 /*return*/, balance];
                }
            });
        });
    };
    AssetBalanceAndProxyAllowanceFetcher.prototype.getProxyAllowanceAsync = function (assetData, userAddress) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2, _a, decodedAssetData, proxyAllowance, isApprovedForAll, isApproved, _b, _c, _d, index, nestedAssetDataElement, nestedAmountElement, nestedAssetAllowance, e_2_1;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        decodedAssetData = order_utils_1.assetDataUtils.decodeAssetDataOrThrow(assetData);
                        if (!order_utils_1.assetDataUtils.isERC20AssetData(decodedAssetData)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._erc20Token.getProxyAllowanceAsync(decodedAssetData.tokenAddress, userAddress, {
                                defaultBlock: this._stateLayer,
                            })];
                    case 1:
                        proxyAllowance = _e.sent();
                        return [3 /*break*/, 15];
                    case 2:
                        if (!order_utils_1.assetDataUtils.isERC721AssetData(decodedAssetData)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this._erc721Token.isProxyApprovedForAllAsync(decodedAssetData.tokenAddress, userAddress, {
                                defaultBlock: this._stateLayer,
                            })];
                    case 3:
                        isApprovedForAll = _e.sent();
                        if (!isApprovedForAll) return [3 /*break*/, 4];
                        return [2 /*return*/, new utils_1.BigNumber(this._erc20Token.UNLIMITED_ALLOWANCE_IN_BASE_UNITS)];
                    case 4: return [4 /*yield*/, this._erc721Token.isProxyApprovedAsync(decodedAssetData.tokenAddress, decodedAssetData.tokenId, {
                            defaultBlock: this._stateLayer,
                        })];
                    case 5:
                        isApproved = _e.sent();
                        proxyAllowance = isApproved ? new utils_1.BigNumber(1) : new utils_1.BigNumber(0);
                        _e.label = 6;
                    case 6: return [3 /*break*/, 15];
                    case 7:
                        if (!order_utils_1.assetDataUtils.isMultiAssetData(decodedAssetData)) return [3 /*break*/, 15];
                        _e.label = 8;
                    case 8:
                        _e.trys.push([8, 13, 14, 15]);
                        _b = __values(decodedAssetData.nestedAssetData.entries()), _c = _b.next();
                        _e.label = 9;
                    case 9:
                        if (!!_c.done) return [3 /*break*/, 12];
                        _d = __read(_c.value, 2), index = _d[0], nestedAssetDataElement = _d[1];
                        nestedAmountElement = decodedAssetData.amounts[index];
                        return [4 /*yield*/, this.getProxyAllowanceAsync(nestedAssetDataElement, userAddress)];
                    case 10:
                        nestedAssetAllowance = (_e.sent()).dividedToIntegerBy(nestedAmountElement);
                        if (proxyAllowance === undefined || nestedAssetAllowance.isLessThan(proxyAllowance)) {
                            proxyAllowance = nestedAssetAllowance;
                        }
                        _e.label = 11;
                    case 11:
                        _c = _b.next();
                        return [3 /*break*/, 9];
                    case 12: return [3 /*break*/, 15];
                    case 13:
                        e_2_1 = _e.sent();
                        e_2 = { error: e_2_1 };
                        return [3 /*break*/, 15];
                    case 14:
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_2) throw e_2.error; }
                        return [7 /*endfinally*/];
                    case 15: return [2 /*return*/, proxyAllowance];
                }
            });
        });
    };
    return AssetBalanceAndProxyAllowanceFetcher;
}());
exports.AssetBalanceAndProxyAllowanceFetcher = AssetBalanceAndProxyAllowanceFetcher;
//# sourceMappingURL=asset_balance_and_proxy_allowance_fetcher.js.map