import { DutchAuctionDetails, SignedOrder } from '@0x/types';
import { BigNumber } from '@0x/utils';
import { Web3Wrapper } from '@0x/web3-wrapper';
import { ContractAbi } from 'ethereum-types';
import { DutchAuctionData, OrderTransactionOpts } from '../types';
import { ContractWrapper } from './contract_wrapper';
export declare class DutchAuctionWrapper extends ContractWrapper {
    abi: ContractAbi;
    address: string;
    private _dutchAuctionContractIfExists?;
    /**
     * Dutch auction details are encoded with the asset data for a 0x order. This function produces a hex
     * encoded assetData string, containing information both about the asset being traded and the
     * dutch auction; which is usable in the makerAssetData or takerAssetData fields in a 0x order.
     * @param assetData Hex encoded assetData string for the asset being auctioned.
     * @param beginTimeSeconds Begin time of the dutch auction.
     * @param beginAmount Starting amount being sold in the dutch auction.
     * @return The hex encoded assetData string.
     */
    static encodeDutchAuctionAssetData(assetData: string, beginTimeSeconds: BigNumber, beginAmount: BigNumber): string;
    /**
     * Dutch auction details are encoded with the asset data for a 0x order. This function decodes a hex
     * encoded assetData string, containing information both about the asset being traded and the
     * dutch auction.
     * @param dutchAuctionData Hex encoded assetData string for the asset being auctioned.
     * @return An object containing the auction asset, auction begin time and auction begin amount.
     */
    static decodeDutchAuctionData(dutchAuctionData: string): DutchAuctionData;
    /**
     * Instantiate DutchAuctionWrapper
     * @param web3Wrapper Web3Wrapper instance to use.
     * @param networkId Desired networkId.
     * @param address The address of the Dutch Auction contract. If undefined, will
     * default to the known address corresponding to the networkId.
     */
    constructor(web3Wrapper: Web3Wrapper, networkId: number, address?: string);
    /**
     * Matches the buy and sell orders at an amount given the following: the current block time, the auction
     * start time and the auction begin amount. The sell order is a an order at the lowest amount
     * at the end of the auction. Excess from the match is transferred to the seller.
     * Over time the price moves from beginAmount to endAmount given the current block.timestamp.
     * @param buyOrder      The Buyer's order. This order is for the current expected price of the auction.
     * @param sellOrder     The Seller's order. This order is for the lowest amount (at the end of the auction).
     * @param takerAddress  The user Ethereum address who would like to fill this order. Must be available via the supplied
     *                      Provider provided at instantiation.
     * @return              Transaction hash.
     */
    matchOrdersAsync(buyOrder: SignedOrder, sellOrder: SignedOrder, takerAddress: string, orderTransactionOpts?: OrderTransactionOpts): Promise<string>;
    /**
     * Fetches the Auction Details for the given order
     * @param sellOrder The Seller's order. This order is for the lowest amount (at the end of the auction).
     * @return The dutch auction details.
     */
    getAuctionDetailsAsync(sellOrder: SignedOrder): Promise<DutchAuctionDetails>;
    private _getDutchAuctionContractAsync;
}
//# sourceMappingURL=dutch_auction_wrapper.d.ts.map