"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var contract_addresses_1 = require("@0x/contract-addresses");
var contract_artifacts_1 = require("@0x/contract-artifacts");
var json_schemas_1 = require("@0x/json-schemas");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var HttpStatus = require("http-status-codes");
var lodash_1 = require("lodash");
var order_tx_opts_schema_1 = require("../schemas/order_tx_opts_schema");
var tx_opts_schema_1 = require("../schemas/tx_opts_schema");
var assert_1 = require("../utils/assert");
var coordinator_server_types_1 = require("../utils/coordinator_server_types");
var decorators_1 = require("../utils/decorators");
var transaction_encoder_1 = require("../utils/transaction_encoder");
var contract_wrapper_1 = require("./contract_wrapper");
/**
 * This class includes all the functionality related to filling or cancelling orders through
 * the 0x V2 Coordinator extension contract.
 */
var CoordinatorWrapper = /** @class */ (function (_super) {
    __extends(CoordinatorWrapper, _super);
    /**
     * Instantiate CoordinatorWrapper
     * @param web3Wrapper Web3Wrapper instance to use.
     * @param networkId Desired networkId.
     * @param address The address of the Coordinator contract. If undefined, will
     * default to the known address corresponding to the networkId.
     * @param exchangeAddress The address of the Exchange contract. If undefined, will
     * default to the known address corresponding to the networkId.
     * @param registryAddress The address of the CoordinatorRegistry contract. If undefined, will
     * default to the known address corresponding to the networkId.
     */
    function CoordinatorWrapper(web3Wrapper, networkId, address, exchangeAddress, registryAddress) {
        var _this = _super.call(this, web3Wrapper, networkId) || this;
        _this.abi = contract_artifacts_1.Coordinator.compilerOutput.abi;
        _this._feeRecipientToEndpoint = {};
        _this.networkId = networkId;
        var contractAddresses = contract_addresses_1.getContractAddressesForNetworkOrThrow(networkId);
        _this.address = address === undefined ? contractAddresses.coordinator : address;
        _this.exchangeAddress = exchangeAddress === undefined ? contractAddresses.coordinator : exchangeAddress;
        _this.registryAddress = registryAddress === undefined ? contractAddresses.coordinatorRegistry : registryAddress;
        _this._contractInstance = new abi_gen_wrappers_1.CoordinatorContract(_this.abi, _this.address, _this._web3Wrapper.getProvider(), _this._web3Wrapper.getContractDefaults());
        _this._registryInstance = new abi_gen_wrappers_1.CoordinatorRegistryContract(contract_artifacts_1.CoordinatorRegistry.compilerOutput.abi, _this.registryAddress, _this._web3Wrapper.getProvider(), _this._web3Wrapper.getContractDefaults());
        _this._exchangeInstance = new abi_gen_wrappers_1.ExchangeContract(contract_artifacts_1.Exchange.compilerOutput.abi, _this.exchangeAddress, _this._web3Wrapper.getProvider(), _this._web3Wrapper.getContractDefaults());
        _this._transactionEncoder = new transaction_encoder_1.TransactionEncoder(_this._exchangeInstance);
        return _this;
    }
    /**
     * Fills a signed order with an amount denominated in baseUnits of the taker asset. Under-the-hood, this
     * method uses the `feeRecipientAddress` of the order to look up the coordinator server endpoint registered in the
     * coordinator registry contract. It requests a signature from that coordinator server before
     * submitting the order and signature as a 0x transaction to the coordinator extension contract. The coordinator extension
     * contract validates signatures and then fills the order via the Exchange contract.
     * @param   signedOrder           An object that conforms to the SignedOrder interface.
     * @param   takerAssetFillAmount  The amount of the order (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress          The user Ethereum address who would like to fill this order. Must be available via the supplied
     *                                Provider provided at instantiation.
     * @param   orderTransactionOpts  Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.fillOrderAsync = function (signedOrder, takerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrder', signedOrder, json_schemas_1.schemas.signedOrderSchema);
                        assert_1.assert.isValidBaseUnitAmount('takerAssetFillAmount', takerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.fillOrderTx(signedOrder, takerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, [signedOrder], orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * No-throw version of fillOrderAsync. This version will not throw if the fill fails. This allows the caller to save gas at the expense of not knowing the reason the fill failed.
     * @param   signedOrder          An object that conforms to the SignedOrder interface.
     * @param   takerAssetFillAmount The amount of the order (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress         The user Ethereum address who would like to fill this order.
     *                               Must be available via the supplied Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.fillOrderNoThrowAsync = function (signedOrder, takerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrder', signedOrder, json_schemas_1.schemas.signedOrderSchema);
                        assert_1.assert.isValidBaseUnitAmount('takerAssetFillAmount', takerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.fillOrderNoThrowTx(signedOrder, takerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, [signedOrder], orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Attempts to fill a specific amount of an order. If the entire amount specified cannot be filled,
     * the fill order is abandoned.
     * @param   signedOrder          An object that conforms to the SignedOrder interface.
     * @param   takerAssetFillAmount The amount of the order (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress         The user Ethereum address who would like to fill this order. Must be available via the supplied
     *                               Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.fillOrKillOrderAsync = function (signedOrder, takerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrder', signedOrder, json_schemas_1.schemas.signedOrderSchema);
                        assert_1.assert.isValidBaseUnitAmount('takerAssetFillAmount', takerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.fillOrKillOrderTx(signedOrder, takerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, [signedOrder], orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Batch version of fillOrderAsync. Executes multiple fills atomically in a single transaction.
     * Under-the-hood, this method uses the `feeRecipientAddress`s of the orders to looks up the coordinator server endpoints
     * registered in the coordinator registry contract. It requests a signature from each coordinator server before
     * submitting the orders and signatures as a 0x transaction to the coordinator extension contract, which validates the
     * signatures and then fills the order through the Exchange contract.
     * If any `feeRecipientAddress` in the batch is not registered to a coordinator server, the whole batch fails.
     * @param   signedOrders          An array of signed orders to fill.
     * @param   takerAssetFillAmounts The amounts of the orders (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress          The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                                Provider provided at instantiation.
     * @param   orderTransactionOpts  Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.batchFillOrdersAsync = function (signedOrders, takerAssetFillAmounts, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var e_1, _a, takerAssetFillAmounts_1, takerAssetFillAmounts_1_1, takerAssetFillAmount, data, txHash;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        try {
                            for (takerAssetFillAmounts_1 = __values(takerAssetFillAmounts), takerAssetFillAmounts_1_1 = takerAssetFillAmounts_1.next(); !takerAssetFillAmounts_1_1.done; takerAssetFillAmounts_1_1 = takerAssetFillAmounts_1.next()) {
                                takerAssetFillAmount = takerAssetFillAmounts_1_1.value;
                                assert_1.assert.isBigNumber('takerAssetFillAmount', takerAssetFillAmount);
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (takerAssetFillAmounts_1_1 && !takerAssetFillAmounts_1_1.done && (_a = takerAssetFillAmounts_1.return)) _a.call(takerAssetFillAmounts_1);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _b.sent();
                        data = this._transactionEncoder.batchFillOrdersTx(signedOrders, takerAssetFillAmounts);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _b.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * No throw version of batchFillOrdersAsync
     * @param   signedOrders          An array of signed orders to fill.
     * @param   takerAssetFillAmounts The amounts of the orders (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress          The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                                Provider provided at instantiation.
     * @param   orderTransactionOpts  Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.batchFillOrdersNoThrowAsync = function (signedOrders, takerAssetFillAmounts, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var e_2, _a, takerAssetFillAmounts_2, takerAssetFillAmounts_2_1, takerAssetFillAmount, data, txHash;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        try {
                            for (takerAssetFillAmounts_2 = __values(takerAssetFillAmounts), takerAssetFillAmounts_2_1 = takerAssetFillAmounts_2.next(); !takerAssetFillAmounts_2_1.done; takerAssetFillAmounts_2_1 = takerAssetFillAmounts_2.next()) {
                                takerAssetFillAmount = takerAssetFillAmounts_2_1.value;
                                assert_1.assert.isBigNumber('takerAssetFillAmount', takerAssetFillAmount);
                            }
                        }
                        catch (e_2_1) { e_2 = { error: e_2_1 }; }
                        finally {
                            try {
                                if (takerAssetFillAmounts_2_1 && !takerAssetFillAmounts_2_1.done && (_a = takerAssetFillAmounts_2.return)) _a.call(takerAssetFillAmounts_2);
                            }
                            finally { if (e_2) throw e_2.error; }
                        }
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _b.sent();
                        data = this._transactionEncoder.batchFillOrdersNoThrowTx(signedOrders, takerAssetFillAmounts);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _b.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Batch version of fillOrKillOrderAsync. Executes multiple fills atomically in a single transaction.
     * @param   signedOrders          An array of signed orders to fill.
     * @param   takerAssetFillAmounts The amounts of the orders (in taker asset baseUnits) that you wish to fill.
     * @param   takerAddress          The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                                Provider provided at instantiation.
     * @param   orderTransactionOpts  Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.batchFillOrKillOrdersAsync = function (signedOrders, takerAssetFillAmounts, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var e_3, _a, takerAssetFillAmounts_3, takerAssetFillAmounts_3_1, takerAssetFillAmount, data, txHash;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        try {
                            for (takerAssetFillAmounts_3 = __values(takerAssetFillAmounts), takerAssetFillAmounts_3_1 = takerAssetFillAmounts_3.next(); !takerAssetFillAmounts_3_1.done; takerAssetFillAmounts_3_1 = takerAssetFillAmounts_3.next()) {
                                takerAssetFillAmount = takerAssetFillAmounts_3_1.value;
                                assert_1.assert.isBigNumber('takerAssetFillAmount', takerAssetFillAmount);
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (takerAssetFillAmounts_3_1 && !takerAssetFillAmounts_3_1.done && (_a = takerAssetFillAmounts_3.return)) _a.call(takerAssetFillAmounts_3);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _b.sent();
                        data = this._transactionEncoder.batchFillOrKillOrdersTx(signedOrders, takerAssetFillAmounts);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _b.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Synchronously executes multiple calls to fillOrder until total amount of makerAsset is bought by taker.
     * Under-the-hood, this method uses the `feeRecipientAddress`s of the orders to looks up the coordinator server endpoints
     * registered in the coordinator registry contract. It requests a signature from each coordinator server before
     * submitting the orders and signatures as a 0x transaction to the coordinator extension contract, which validates the
     * signatures and then fills the order through the Exchange contract.
     * If any `feeRecipientAddress` in the batch is not registered to a coordinator server, the whole batch fails.
     * @param   signedOrders         An array of signed orders to fill.
     * @param   makerAssetFillAmount Maker asset fill amount.
     * @param   takerAddress         The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                               Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.marketBuyOrdersAsync = function (signedOrders, makerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        assert_1.assert.isBigNumber('makerAssetFillAmount', makerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.marketBuyOrdersTx(signedOrders, makerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Synchronously executes multiple calls to fillOrder until total amount of makerAsset is bought by taker.
     * Under-the-hood, this method uses the `feeRecipientAddress`s of the orders to looks up the coordinator server endpoints
     * registered in the coordinator registry contract. It requests a signature from each coordinator server before
     * submitting the orders and signatures as a 0x transaction to the coordinator extension contract, which validates the
     * signatures and then fills the order through the Exchange contract.
     * If any `feeRecipientAddress` in the batch is not registered to a coordinator server, the whole batch fails.
     * @param   signedOrders         An array of signed orders to fill.
     * @param   takerAssetFillAmount Taker asset fill amount.
     * @param   takerAddress         The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                               Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.marketSellOrdersAsync = function (signedOrders, takerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        assert_1.assert.isBigNumber('takerAssetFillAmount', takerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.marketSellOrdersTx(signedOrders, takerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * No throw version of marketBuyOrdersAsync
     * @param   signedOrders         An array of signed orders to fill.
     * @param   makerAssetFillAmount Maker asset fill amount.
     * @param   takerAddress         The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                               Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.marketBuyOrdersNoThrowAsync = function (signedOrders, makerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        assert_1.assert.isBigNumber('makerAssetFillAmount', makerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.marketBuyOrdersNoThrowTx(signedOrders, makerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * No throw version of marketSellOrdersAsync
     * @param   signedOrders         An array of signed orders to fill.
     * @param   takerAssetFillAmount Taker asset fill amount.
     * @param   takerAddress         The user Ethereum address who would like to fill these orders. Must be available via the supplied
     *                               Provider provided at instantiation.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.marketSellOrdersNoThrowAsync = function (signedOrders, takerAssetFillAmount, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('signedOrders', signedOrders, json_schemas_1.schemas.signedOrdersSchema);
                        assert_1.assert.isBigNumber('takerAssetFillAmount', takerAssetFillAmount);
                        assert_1.assert.isETHAddressHex('takerAddress', takerAddress);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.marketSellOrdersNoThrowTx(signedOrders, takerAssetFillAmount);
                        return [4 /*yield*/, this._handleFillsAsync(data, takerAddress, signedOrders, orderTransactionOpts)];
                    case 2:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Soft cancel a given order.
     * Soft cancels are recorded only on coordinator operator servers and do not involve an Ethereum transaction.
     * See [soft cancels](https://github.com/0xProject/0x-protocol-specification/blob/master/v2/coordinator-specification.md#soft-cancels).
     * @param   order           An object that conforms to the Order or SignedOrder interface. The order you would like to cancel.
     * @return  CoordinatorServerCancellationResponse. See [Cancellation Response](https://github.com/0xProject/0x-protocol-specification/blob/master/v2/coordinator-specification.md#response).
     */
    CoordinatorWrapper.prototype.softCancelOrderAsync = function (order) {
        return __awaiter(this, void 0, void 0, function () {
            var data, transaction, endpoint, response, approvedOrders, cancellations, errors;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('order', order, json_schemas_1.schemas.orderSchema);
                        assert_1.assert.isETHAddressHex('feeRecipientAddress', order.feeRecipientAddress);
                        assert_1.assert.isSenderAddressAsync('makerAddress', order.makerAddress, this._web3Wrapper);
                        data = this._transactionEncoder.cancelOrderTx(order);
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, order.makerAddress)];
                    case 1:
                        transaction = _a.sent();
                        return [4 /*yield*/, this._getServerEndpointOrThrowAsync(order.feeRecipientAddress)];
                    case 2:
                        endpoint = _a.sent();
                        return [4 /*yield*/, this._executeServerRequestAsync(transaction, order.makerAddress, endpoint)];
                    case 3:
                        response = _a.sent();
                        if (response.isError) {
                            approvedOrders = new Array();
                            cancellations = new Array();
                            errors = [
                                __assign({}, response, { orders: [order] }),
                            ];
                            throw new coordinator_server_types_1.CoordinatorServerError(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed, approvedOrders, cancellations, errors);
                        }
                        else {
                            return [2 /*return*/, response.body];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Batch version of softCancelOrderAsync. Requests multiple soft cancels
     * @param   orders                An array of orders to cancel.
     * @return  CoordinatorServerCancellationResponse. See [Cancellation Response](https://github.com/0xProject/0x-protocol-specification/blob/master/v2/coordinator-specification.md#response).
     */
    CoordinatorWrapper.prototype.batchSoftCancelOrdersAsync = function (orders) {
        return __awaiter(this, void 0, void 0, function () {
            var e_4, _a, makerAddress, data, serverEndpointsToOrders, errorResponses, successResponses, transaction, _b, _c, endpoint, response, e_4_1, errorsWithOrders, approvedOrders, cancellations;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('orders', orders, json_schemas_1.schemas.ordersSchema);
                        makerAddress = getMakerAddressOrThrow(orders);
                        assert_1.assert.isSenderAddressAsync('makerAddress', makerAddress, this._web3Wrapper);
                        data = this._transactionEncoder.batchCancelOrdersTx(orders);
                        return [4 /*yield*/, this._mapServerEndpointsToOrdersAsync(orders)];
                    case 1:
                        serverEndpointsToOrders = _d.sent();
                        errorResponses = [];
                        successResponses = [];
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, makerAddress)];
                    case 2:
                        transaction = _d.sent();
                        _d.label = 3;
                    case 3:
                        _d.trys.push([3, 8, 9, 10]);
                        _b = __values(Object.keys(serverEndpointsToOrders)), _c = _b.next();
                        _d.label = 4;
                    case 4:
                        if (!!_c.done) return [3 /*break*/, 7];
                        endpoint = _c.value;
                        return [4 /*yield*/, this._executeServerRequestAsync(transaction, makerAddress, endpoint)];
                    case 5:
                        response = _d.sent();
                        if (response.isError) {
                            errorResponses.push(response);
                        }
                        else {
                            successResponses.push(response.body);
                        }
                        _d.label = 6;
                    case 6:
                        _c = _b.next();
                        return [3 /*break*/, 4];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_4_1 = _d.sent();
                        e_4 = { error: e_4_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_4) throw e_4.error; }
                        return [7 /*endfinally*/];
                    case 10:
                        // if no errors
                        if (errorResponses.length === 0) {
                            return [2 /*return*/, successResponses];
                        }
                        else {
                            errorsWithOrders = errorResponses.map(function (resp) {
                                var endpoint = resp.coordinatorOperator;
                                var _orders = serverEndpointsToOrders[endpoint];
                                return __assign({}, resp, { orders: _orders });
                            });
                            approvedOrders = new Array();
                            cancellations = successResponses;
                            // return errors and approvals
                            throw new coordinator_server_types_1.CoordinatorServerError(coordinator_server_types_1.CoordinatorServerErrorMsg.CancellationFailed, approvedOrders, cancellations, errorsWithOrders);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Cancels an order on-chain by submitting an Ethereum transaction.
     * @param   order           An object that conforms to the Order or SignedOrder interface. The order you would like to cancel.
     * @param   orderTransactionOpts Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.hardCancelOrderAsync = function (order, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, transaction, approvalSignatures, approvalExpirationTimeSeconds, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('order', order, json_schemas_1.schemas.orderSchema);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('makerAddress', order.makerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.cancelOrderTx(order);
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, order.makerAddress)];
                    case 2:
                        transaction = _a.sent();
                        approvalSignatures = new Array();
                        approvalExpirationTimeSeconds = new Array();
                        return [4 /*yield*/, this._submitCoordinatorTransactionAsync(transaction, order.makerAddress, transaction.signature, approvalExpirationTimeSeconds, approvalSignatures, orderTransactionOpts)];
                    case 3:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Batch version of hardCancelOrderAsync. Cancels orders on-chain by submitting an Ethereum transaction.
     * Executes multiple cancels atomically in a single transaction.
     * @param   orders                An array of orders to cancel.
     * @param   orderTransactionOpts  Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.batchHardCancelOrdersAsync = function (orders, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var makerAddress, data, transaction, approvalSignatures, approvalExpirationTimeSeconds, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('orders', orders, json_schemas_1.schemas.ordersSchema);
                        makerAddress = getMakerAddressOrThrow(orders);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('makerAddress', makerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.batchCancelOrdersTx(orders);
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, makerAddress)];
                    case 2:
                        transaction = _a.sent();
                        approvalSignatures = new Array();
                        approvalExpirationTimeSeconds = new Array();
                        return [4 /*yield*/, this._submitCoordinatorTransactionAsync(transaction, makerAddress, transaction.signature, approvalExpirationTimeSeconds, approvalSignatures, orderTransactionOpts)];
                    case 3:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Cancels orders on-chain by submitting an Ethereum transaction.
     * Cancels all orders created by makerAddress with a salt less than or equal to the targetOrderEpoch
     * and senderAddress equal to coordinator extension contract address.
     * @param   targetOrderEpoch             Target order epoch.
     * @param   senderAddress                Address that should send the transaction.
     * @param   orderTransactionOpts         Optional arguments this method accepts.
     * @return  Transaction hash.
     */
    CoordinatorWrapper.prototype.hardCancelOrdersUpToAsync = function (targetOrderEpoch, senderAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var data, transaction, approvalSignatures, approvalExpirationTimeSeconds, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.isBigNumber('targetOrderEpoch', targetOrderEpoch);
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('senderAddress', senderAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        data = this._transactionEncoder.cancelOrdersUpToTx(targetOrderEpoch);
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, senderAddress)];
                    case 2:
                        transaction = _a.sent();
                        approvalSignatures = new Array();
                        approvalExpirationTimeSeconds = new Array();
                        return [4 /*yield*/, this._submitCoordinatorTransactionAsync(transaction, senderAddress, transaction.signature, approvalExpirationTimeSeconds, approvalSignatures, orderTransactionOpts)];
                    case 3:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Validates that the 0x transaction has been approved by all of the feeRecipients that correspond to each order in the transaction's Exchange calldata.
     * Throws an error if the transaction approvals are not valid. Will not detect failures that would occur when the transaction is executed on the Exchange contract.
     * @param transaction 0x transaction containing salt, signerAddress, and data.
     * @param txOrigin Required signer of Ethereum transaction calling this function.
     * @param transactionSignature Proof that the transaction has been signed by the signer.
     * @param approvalExpirationTimeSeconds Array of expiration times in seconds for which each corresponding approval signature expires.
     * @param approvalSignatures Array of signatures that correspond to the feeRecipients of each order in the transaction's Exchange calldata.
     */
    CoordinatorWrapper.prototype.assertValidCoordinatorApprovalsOrThrowAsync = function (transaction, txOrigin, transactionSignature, approvalExpirationTimeSeconds, approvalSignatures) {
        return __awaiter(this, void 0, void 0, function () {
            var e_5, _a, e_6, _b, approvalExpirationTimeSeconds_1, approvalExpirationTimeSeconds_1_1, expirationTime, approvalSignatures_1, approvalSignatures_1_1, approvalSignature;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        assert_1.assert.doesConformToSchema('transaction', transaction, json_schemas_1.schemas.zeroExTransactionSchema);
                        assert_1.assert.isETHAddressHex('txOrigin', txOrigin);
                        assert_1.assert.isHexString('transactionSignature', transactionSignature);
                        try {
                            for (approvalExpirationTimeSeconds_1 = __values(approvalExpirationTimeSeconds), approvalExpirationTimeSeconds_1_1 = approvalExpirationTimeSeconds_1.next(); !approvalExpirationTimeSeconds_1_1.done; approvalExpirationTimeSeconds_1_1 = approvalExpirationTimeSeconds_1.next()) {
                                expirationTime = approvalExpirationTimeSeconds_1_1.value;
                                assert_1.assert.isBigNumber('expirationTime', expirationTime);
                            }
                        }
                        catch (e_5_1) { e_5 = { error: e_5_1 }; }
                        finally {
                            try {
                                if (approvalExpirationTimeSeconds_1_1 && !approvalExpirationTimeSeconds_1_1.done && (_a = approvalExpirationTimeSeconds_1.return)) _a.call(approvalExpirationTimeSeconds_1);
                            }
                            finally { if (e_5) throw e_5.error; }
                        }
                        try {
                            for (approvalSignatures_1 = __values(approvalSignatures), approvalSignatures_1_1 = approvalSignatures_1.next(); !approvalSignatures_1_1.done; approvalSignatures_1_1 = approvalSignatures_1.next()) {
                                approvalSignature = approvalSignatures_1_1.value;
                                assert_1.assert.isHexString('approvalSignature', approvalSignature);
                            }
                        }
                        catch (e_6_1) { e_6 = { error: e_6_1 }; }
                        finally {
                            try {
                                if (approvalSignatures_1_1 && !approvalSignatures_1_1.done && (_b = approvalSignatures_1.return)) _b.call(approvalSignatures_1);
                            }
                            finally { if (e_6) throw e_6.error; }
                        }
                        return [4 /*yield*/, this._contractInstance.assertValidCoordinatorApprovals.callAsync(transaction, txOrigin, transactionSignature, approvalExpirationTimeSeconds, approvalSignatures)];
                    case 1:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Recovers the address of a signer given a hash and signature.
     * @param hash Any 32 byte hash.
     * @param signature Proof that the hash has been signed by signer.
     * @returns Signer address.
     */
    CoordinatorWrapper.prototype.getSignerAddressAsync = function (hash, signature) {
        return __awaiter(this, void 0, void 0, function () {
            var signerAddress;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assert_1.assert.isHexString('hash', hash);
                        assert_1.assert.isHexString('signature', signature);
                        return [4 /*yield*/, this._contractInstance.getSignerAddress.callAsync(hash, signature)];
                    case 1:
                        signerAddress = _a.sent();
                        return [2 /*return*/, signerAddress];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._handleFillsAsync = function (data, takerAddress, signedOrders, orderTransactionOpts) {
        return __awaiter(this, void 0, void 0, function () {
            function formatRawResponse(rawResponse) {
                return {
                    signatures: [].concat(rawResponse.signatures),
                    expirationTimeSeconds: [].concat(Array(rawResponse.signatures.length).fill(rawResponse.expirationTimeSeconds)),
                };
            }
            var e_7, _a, coordinatorOrders, serverEndpointsToOrders, errorResponses, approvalResponses, transaction, _b, _c, endpoint, response, e_7_1, allApprovals, allSignatures, allExpirationTimes, txHash, notCoordinatorOrders, approvedOrdersNested, approvedOrders, errorsWithOrders, cancellations;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        coordinatorOrders = signedOrders.filter(function (o) { return o.senderAddress === _this.address; });
                        return [4 /*yield*/, this._mapServerEndpointsToOrdersAsync(coordinatorOrders)];
                    case 1:
                        serverEndpointsToOrders = _d.sent();
                        errorResponses = [];
                        approvalResponses = [];
                        return [4 /*yield*/, this._generateSignedZeroExTransactionAsync(data, takerAddress)];
                    case 2:
                        transaction = _d.sent();
                        _d.label = 3;
                    case 3:
                        _d.trys.push([3, 8, 9, 10]);
                        _b = __values(Object.keys(serverEndpointsToOrders)), _c = _b.next();
                        _d.label = 4;
                    case 4:
                        if (!!_c.done) return [3 /*break*/, 7];
                        endpoint = _c.value;
                        return [4 /*yield*/, this._executeServerRequestAsync(transaction, takerAddress, endpoint)];
                    case 5:
                        response = _d.sent();
                        if (response.isError) {
                            errorResponses.push(response);
                        }
                        else {
                            approvalResponses.push(response);
                        }
                        _d.label = 6;
                    case 6:
                        _c = _b.next();
                        return [3 /*break*/, 4];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_7_1 = _d.sent();
                        e_7 = { error: e_7_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_7) throw e_7.error; }
                        return [7 /*endfinally*/];
                    case 10:
                        if (!(errorResponses.length === 0)) return [3 /*break*/, 12];
                        allApprovals = approvalResponses.map(function (resp) {
                            return formatRawResponse(resp.body);
                        });
                        allSignatures = lodash_1.flatten(allApprovals.map(function (a) { return a.signatures; }));
                        allExpirationTimes = lodash_1.flatten(allApprovals.map(function (a) { return a.expirationTimeSeconds; }));
                        return [4 /*yield*/, this._submitCoordinatorTransactionAsync(transaction, takerAddress, transaction.signature, allExpirationTimes, allSignatures, orderTransactionOpts)];
                    case 11:
                        txHash = _d.sent();
                        return [2 /*return*/, txHash];
                    case 12:
                        notCoordinatorOrders = signedOrders.filter(function (o) { return o.senderAddress !== _this.address; });
                        approvedOrdersNested = approvalResponses.map(function (resp) {
                            var endpoint = resp.coordinatorOperator;
                            var orders = serverEndpointsToOrders[endpoint];
                            return orders;
                        });
                        approvedOrders = lodash_1.flatten(approvedOrdersNested.concat(notCoordinatorOrders));
                        errorsWithOrders = errorResponses.map(function (resp) {
                            var endpoint = resp.coordinatorOperator;
                            var orders = serverEndpointsToOrders[endpoint];
                            return __assign({}, resp, { orders: orders });
                        });
                        cancellations = new Array();
                        throw new coordinator_server_types_1.CoordinatorServerError(coordinator_server_types_1.CoordinatorServerErrorMsg.FillFailed, approvedOrders, cancellations, errorsWithOrders);
                    case 13: return [2 /*return*/];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._getServerEndpointOrThrowAsync = function (feeRecipientAddress) {
        return __awaiter(this, void 0, void 0, function () {
            function _fetchServerEndpointOrThrowAsync(feeRecipient, registryInstance) {
                return __awaiter(this, void 0, void 0, function () {
                    var coordinatorOperatorEndpoint;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, registryInstance.getCoordinatorEndpoint.callAsync(feeRecipient)];
                            case 1:
                                coordinatorOperatorEndpoint = _a.sent();
                                if (coordinatorOperatorEndpoint === '' || coordinatorOperatorEndpoint === undefined) {
                                    throw new Error("No Coordinator server endpoint found in Coordinator Registry for feeRecipientAddress: " + feeRecipient + ". Registry contract address: " + registryInstance.address);
                                }
                                return [2 /*return*/, coordinatorOperatorEndpoint];
                        }
                    });
                });
            }
            var cached, endpoint, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        cached = this._feeRecipientToEndpoint[feeRecipientAddress];
                        if (!(cached !== undefined)) return [3 /*break*/, 1];
                        _a = cached;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, _fetchServerEndpointOrThrowAsync(feeRecipientAddress, this._registryInstance)];
                    case 2:
                        _a = _b.sent();
                        _b.label = 3;
                    case 3:
                        endpoint = _a;
                        return [2 /*return*/, endpoint];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._generateSignedZeroExTransactionAsync = function (data, signerAddress) {
        return __awaiter(this, void 0, void 0, function () {
            var transaction, signedTransaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        transaction = {
                            salt: order_utils_1.generatePseudoRandomSalt(),
                            signerAddress: signerAddress,
                            data: data,
                            verifyingContractAddress: this.exchangeAddress,
                        };
                        return [4 /*yield*/, order_utils_1.signatureUtils.ecSignTransactionAsync(this._web3Wrapper.getProvider(), transaction, transaction.signerAddress)];
                    case 1:
                        signedTransaction = _a.sent();
                        return [2 /*return*/, signedTransaction];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._executeServerRequestAsync = function (signedTransaction, txOrigin, endpoint) {
        return __awaiter(this, void 0, void 0, function () {
            var requestPayload, response, isError, isValidationError, json, _a, result;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        requestPayload = {
                            signedTransaction: signedTransaction,
                            txOrigin: txOrigin,
                        };
                        return [4 /*yield*/, utils_1.fetchAsync(endpoint + "/v1/request_transaction?networkId=" + this.networkId, {
                                body: JSON.stringify(requestPayload),
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json; charset=utf-8',
                                },
                            })];
                    case 1:
                        response = _b.sent();
                        isError = response.status !== HttpStatus.OK;
                        isValidationError = response.status === HttpStatus.BAD_REQUEST;
                        if (!(isError && !isValidationError)) return [3 /*break*/, 2];
                        _a = undefined;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, response.json()];
                    case 3:
                        _a = _b.sent();
                        _b.label = 4;
                    case 4:
                        json = _a;
                        result = {
                            isError: isError,
                            status: response.status,
                            body: isError ? undefined : json,
                            error: isError ? json : undefined,
                            request: requestPayload,
                            coordinatorOperator: endpoint,
                        };
                        return [2 /*return*/, result];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._submitCoordinatorTransactionAsync = function (transaction, txOrigin, transactionSignature, approvalExpirationTimeSeconds, approvalSignatures, orderTransactionOpts) {
        return __awaiter(this, void 0, void 0, function () {
            var txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!orderTransactionOpts.shouldValidate) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._contractInstance.executeTransaction.callAsync(transaction, txOrigin, transactionSignature, approvalExpirationTimeSeconds, approvalSignatures, {
                                from: txOrigin,
                                gas: orderTransactionOpts.gasLimit,
                                gasPrice: orderTransactionOpts.gasPrice,
                                nonce: orderTransactionOpts.nonce,
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this._contractInstance.executeTransaction.sendTransactionAsync(transaction, txOrigin, transactionSignature, approvalExpirationTimeSeconds, approvalSignatures, {
                            from: txOrigin,
                            gas: orderTransactionOpts.gasLimit,
                            gasPrice: orderTransactionOpts.gasPrice,
                            nonce: orderTransactionOpts.nonce,
                        })];
                    case 3:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    CoordinatorWrapper.prototype._mapServerEndpointsToOrdersAsync = function (coordinatorOrders) {
        return __awaiter(this, void 0, void 0, function () {
            var e_8, _a, e_9, _b, feeRecipientsToOrders, coordinatorOrders_1, coordinatorOrders_1_1, order, feeRecipient, serverEndpointsToOrders, _c, _d, feeRecipient, endpoint, orders, e_9_1;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        feeRecipientsToOrders = {};
                        try {
                            for (coordinatorOrders_1 = __values(coordinatorOrders), coordinatorOrders_1_1 = coordinatorOrders_1.next(); !coordinatorOrders_1_1.done; coordinatorOrders_1_1 = coordinatorOrders_1.next()) {
                                order = coordinatorOrders_1_1.value;
                                feeRecipient = order.feeRecipientAddress;
                                if (feeRecipientsToOrders[feeRecipient] === undefined) {
                                    feeRecipientsToOrders[feeRecipient] = [];
                                }
                                feeRecipientsToOrders[feeRecipient].push(order);
                            }
                        }
                        catch (e_8_1) { e_8 = { error: e_8_1 }; }
                        finally {
                            try {
                                if (coordinatorOrders_1_1 && !coordinatorOrders_1_1.done && (_a = coordinatorOrders_1.return)) _a.call(coordinatorOrders_1);
                            }
                            finally { if (e_8) throw e_8.error; }
                        }
                        serverEndpointsToOrders = {};
                        _e.label = 1;
                    case 1:
                        _e.trys.push([1, 6, 7, 8]);
                        _c = __values(Object.keys(feeRecipientsToOrders)), _d = _c.next();
                        _e.label = 2;
                    case 2:
                        if (!!_d.done) return [3 /*break*/, 5];
                        feeRecipient = _d.value;
                        return [4 /*yield*/, this._getServerEndpointOrThrowAsync(feeRecipient)];
                    case 3:
                        endpoint = _e.sent();
                        orders = feeRecipientsToOrders[feeRecipient];
                        if (serverEndpointsToOrders[endpoint] === undefined) {
                            serverEndpointsToOrders[endpoint] = [];
                        }
                        serverEndpointsToOrders[endpoint] = serverEndpointsToOrders[endpoint].concat(orders);
                        _e.label = 4;
                    case 4:
                        _d = _c.next();
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        e_9_1 = _e.sent();
                        e_9 = { error: e_9_1 };
                        return [3 /*break*/, 8];
                    case 7:
                        try {
                            if (_d && !_d.done && (_b = _c.return)) _b.call(_c);
                        }
                        finally { if (e_9) throw e_9.error; }
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/, serverEndpointsToOrders];
                }
            });
        });
    };
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "fillOrderAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "fillOrderNoThrowAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "fillOrKillOrderAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "batchFillOrdersAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "batchFillOrdersNoThrowAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "batchFillOrKillOrdersAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "marketBuyOrdersAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "marketSellOrdersAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "marketBuyOrdersNoThrowAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "marketSellOrdersNoThrowAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "hardCancelOrderAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "batchHardCancelOrdersAsync", null);
    __decorate([
        decorators_1.decorators.asyncZeroExErrorHandler
    ], CoordinatorWrapper.prototype, "hardCancelOrdersUpToAsync", null);
    return CoordinatorWrapper;
}(contract_wrapper_1.ContractWrapper));
exports.CoordinatorWrapper = CoordinatorWrapper;
function getMakerAddressOrThrow(orders) {
    var uniqueMakerAddresses = new Set(orders.map(function (o) { return o.makerAddress; }));
    if (uniqueMakerAddresses.size > 1) {
        throw new Error("All orders in a batch must have the same makerAddress");
    }
    return orders[0].makerAddress;
}
// tslint:disable:max-file-line-count
//# sourceMappingURL=coordinator_wrapper.js.map