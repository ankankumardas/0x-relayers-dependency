"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var abi_gen_wrappers_1 = require("@0x/abi-gen-wrappers");
var contract_artifacts_1 = require("@0x/contract-artifacts");
var json_schemas_1 = require("@0x/json-schemas");
var order_utils_1 = require("@0x/order-utils");
var utils_1 = require("@0x/utils");
var ethAbi = require("ethereumjs-abi");
var ethUtil = require("ethereumjs-util");
var order_tx_opts_schema_1 = require("../schemas/order_tx_opts_schema");
var tx_opts_schema_1 = require("../schemas/tx_opts_schema");
var types_1 = require("../types");
var assert_1 = require("../utils/assert");
var contract_addresses_1 = require("../utils/contract_addresses");
var contract_wrapper_1 = require("./contract_wrapper");
var DutchAuctionWrapper = /** @class */ (function (_super) {
    __extends(DutchAuctionWrapper, _super);
    /**
     * Instantiate DutchAuctionWrapper
     * @param web3Wrapper Web3Wrapper instance to use.
     * @param networkId Desired networkId.
     * @param address The address of the Dutch Auction contract. If undefined, will
     * default to the known address corresponding to the networkId.
     */
    function DutchAuctionWrapper(web3Wrapper, networkId, address) {
        var _this = _super.call(this, web3Wrapper, networkId) || this;
        _this.abi = contract_artifacts_1.DutchAuction.compilerOutput.abi;
        _this.address = address === undefined ? contract_addresses_1._getDefaultContractAddresses(networkId).dutchAuction : address;
        return _this;
    }
    /**
     * Dutch auction details are encoded with the asset data for a 0x order. This function produces a hex
     * encoded assetData string, containing information both about the asset being traded and the
     * dutch auction; which is usable in the makerAssetData or takerAssetData fields in a 0x order.
     * @param assetData Hex encoded assetData string for the asset being auctioned.
     * @param beginTimeSeconds Begin time of the dutch auction.
     * @param beginAmount Starting amount being sold in the dutch auction.
     * @return The hex encoded assetData string.
     */
    DutchAuctionWrapper.encodeDutchAuctionAssetData = function (assetData, beginTimeSeconds, beginAmount) {
        var assetDataBuffer = ethUtil.toBuffer(assetData);
        var abiEncodedAuctionData = ethAbi.rawEncode(['uint256', 'uint256'], [beginTimeSeconds.toString(), beginAmount.toString()]);
        var abiEncodedAuctionDataBuffer = ethUtil.toBuffer(abiEncodedAuctionData);
        var dutchAuctionDataBuffer = Buffer.concat([assetDataBuffer, abiEncodedAuctionDataBuffer]);
        var dutchAuctionData = ethUtil.bufferToHex(dutchAuctionDataBuffer);
        return dutchAuctionData;
    };
    /**
     * Dutch auction details are encoded with the asset data for a 0x order. This function decodes a hex
     * encoded assetData string, containing information both about the asset being traded and the
     * dutch auction.
     * @param dutchAuctionData Hex encoded assetData string for the asset being auctioned.
     * @return An object containing the auction asset, auction begin time and auction begin amount.
     */
    DutchAuctionWrapper.decodeDutchAuctionData = function (dutchAuctionData) {
        var dutchAuctionDataBuffer = ethUtil.toBuffer(dutchAuctionData);
        // Decode asset data
        var dutchAuctionDataLengthInBytes = 64;
        var assetDataBuffer = dutchAuctionDataBuffer.slice(0, dutchAuctionDataBuffer.byteLength - dutchAuctionDataLengthInBytes);
        var assetDataHex = ethUtil.bufferToHex(assetDataBuffer);
        var assetData = order_utils_1.assetDataUtils.decodeAssetDataOrThrow(assetDataHex);
        // Decode auction details
        var dutchAuctionDetailsBuffer = dutchAuctionDataBuffer.slice(dutchAuctionDataBuffer.byteLength - dutchAuctionDataLengthInBytes);
        var _a = __read(ethAbi.rawDecode(['uint256', 'uint256'], dutchAuctionDetailsBuffer), 2), beginTimeSecondsAsBN = _a[0], beginAmountAsBN = _a[1];
        var beginTimeSeconds = new utils_1.BigNumber(beginTimeSecondsAsBN.toString());
        var beginAmount = new utils_1.BigNumber(beginAmountAsBN.toString());
        return {
            assetData: assetData,
            beginTimeSeconds: beginTimeSeconds,
            beginAmount: beginAmount,
        };
    };
    /**
     * Matches the buy and sell orders at an amount given the following: the current block time, the auction
     * start time and the auction begin amount. The sell order is a an order at the lowest amount
     * at the end of the auction. Excess from the match is transferred to the seller.
     * Over time the price moves from beginAmount to endAmount given the current block.timestamp.
     * @param buyOrder      The Buyer's order. This order is for the current expected price of the auction.
     * @param sellOrder     The Seller's order. This order is for the lowest amount (at the end of the auction).
     * @param takerAddress  The user Ethereum address who would like to fill this order. Must be available via the supplied
     *                      Provider provided at instantiation.
     * @return              Transaction hash.
     */
    DutchAuctionWrapper.prototype.matchOrdersAsync = function (buyOrder, sellOrder, takerAddress, orderTransactionOpts) {
        if (orderTransactionOpts === void 0) { orderTransactionOpts = { shouldValidate: true }; }
        return __awaiter(this, void 0, void 0, function () {
            var normalizedTakerAddress, dutchAuctionInstance, txHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // type assertions
                        assert_1.assert.doesConformToSchema('buyOrder', buyOrder, json_schemas_1.schemas.signedOrderSchema);
                        assert_1.assert.doesConformToSchema('sellOrder', sellOrder, json_schemas_1.schemas.signedOrderSchema);
                        return [4 /*yield*/, assert_1.assert.isSenderAddressAsync('takerAddress', takerAddress, this._web3Wrapper)];
                    case 1:
                        _a.sent();
                        assert_1.assert.doesConformToSchema('orderTransactionOpts', orderTransactionOpts, order_tx_opts_schema_1.orderTxOptsSchema, [tx_opts_schema_1.txOptsSchema]);
                        normalizedTakerAddress = takerAddress.toLowerCase();
                        // other assertions
                        if (sellOrder.makerAssetData !== buyOrder.takerAssetData ||
                            sellOrder.takerAssetData !== buyOrder.makerAssetData) {
                            throw new Error(types_1.DutchAuctionWrapperError.AssetDataMismatch);
                        }
                        return [4 /*yield*/, this._getDutchAuctionContractAsync()];
                    case 2:
                        dutchAuctionInstance = _a.sent();
                        if (!orderTransactionOpts.shouldValidate) return [3 /*break*/, 4];
                        return [4 /*yield*/, dutchAuctionInstance.matchOrders.callAsync(buyOrder, sellOrder, buyOrder.signature, sellOrder.signature, {
                                from: normalizedTakerAddress,
                                gas: orderTransactionOpts.gasLimit,
                                gasPrice: orderTransactionOpts.gasPrice,
                                nonce: orderTransactionOpts.nonce,
                            })];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [4 /*yield*/, dutchAuctionInstance.matchOrders.sendTransactionAsync(buyOrder, sellOrder, buyOrder.signature, sellOrder.signature, {
                            from: normalizedTakerAddress,
                            gas: orderTransactionOpts.gasLimit,
                            gasPrice: orderTransactionOpts.gasPrice,
                            nonce: orderTransactionOpts.nonce,
                        })];
                    case 5:
                        txHash = _a.sent();
                        return [2 /*return*/, txHash];
                }
            });
        });
    };
    /**
     * Fetches the Auction Details for the given order
     * @param sellOrder The Seller's order. This order is for the lowest amount (at the end of the auction).
     * @return The dutch auction details.
     */
    DutchAuctionWrapper.prototype.getAuctionDetailsAsync = function (sellOrder) {
        return __awaiter(this, void 0, void 0, function () {
            var dutchAuctionInstance, auctionDetails;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // type assertions
                        assert_1.assert.doesConformToSchema('sellOrder', sellOrder, json_schemas_1.schemas.signedOrderSchema);
                        return [4 /*yield*/, this._getDutchAuctionContractAsync()];
                    case 1:
                        dutchAuctionInstance = _a.sent();
                        return [4 /*yield*/, dutchAuctionInstance.getAuctionDetails.callAsync(sellOrder)];
                    case 2:
                        auctionDetails = _a.sent();
                        return [2 /*return*/, auctionDetails];
                }
            });
        });
    };
    DutchAuctionWrapper.prototype._getDutchAuctionContractAsync = function () {
        return __awaiter(this, void 0, void 0, function () {
            var contractInstance;
            return __generator(this, function (_a) {
                if (this._dutchAuctionContractIfExists !== undefined) {
                    return [2 /*return*/, this._dutchAuctionContractIfExists];
                }
                contractInstance = new abi_gen_wrappers_1.DutchAuctionContract(this.abi, this.address, this._web3Wrapper.getProvider(), this._web3Wrapper.getContractDefaults());
                this._dutchAuctionContractIfExists = contractInstance;
                return [2 /*return*/, this._dutchAuctionContractIfExists];
            });
        });
    };
    return DutchAuctionWrapper;
}(contract_wrapper_1.ContractWrapper));
exports.DutchAuctionWrapper = DutchAuctionWrapper;
//# sourceMappingURL=dutch_auction_wrapper.js.map