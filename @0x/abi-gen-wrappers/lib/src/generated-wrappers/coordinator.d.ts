import { BaseContract, PromiseWithTransactionHash } from '@0x/base-contract';
import { BlockParamLiteral, CallData, ContractAbi, ContractArtifact, TransactionReceiptWithDecodedLogs, TxData, SupportedProvider } from 'ethereum-types';
import { BigNumber } from '@0x/utils';
import { SimpleContractArtifact } from '@0x/types';
export declare class CoordinatorContract extends BaseContract {
    getSignerAddress: {
        callAsync(hash: string, signature: string, callData?: Partial<CallData>, defaultBlock?: number | BlockParamLiteral | undefined): Promise<string>;
    };
    executeTransaction: {
        sendTransactionAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], txData?: Partial<TxData>): Promise<string>;
        awaitTransactionSuccessAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], txData?: number | Partial<TxData> | undefined, pollingIntervalMs?: number | undefined, timeoutMs?: number | undefined): PromiseWithTransactionHash<TransactionReceiptWithDecodedLogs>;
        estimateGasAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], txData?: Partial<TxData>): Promise<number>;
        getABIEncodedTransactionData(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[]): string;
        callAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], callData?: Partial<CallData>, defaultBlock?: number | BlockParamLiteral | undefined): Promise<void>;
    };
    assertValidCoordinatorApprovals: {
        callAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], callData?: Partial<CallData>, defaultBlock?: number | BlockParamLiteral | undefined): Promise<void>;
    };
    EIP712_DOMAIN_HASH: {
        callAsync(callData?: Partial<CallData>, defaultBlock?: number | BlockParamLiteral | undefined): Promise<string>;
    };
    assertValidTransactionOrdersApproval: {
        callAsync(transaction: {
            salt: BigNumber;
            signerAddress: string;
            data: string;
        }, orders: {
            makerAddress: string;
            takerAddress: string;
            feeRecipientAddress: string;
            senderAddress: string;
            makerAssetAmount: BigNumber;
            takerAssetAmount: BigNumber;
            makerFee: BigNumber;
            takerFee: BigNumber;
            expirationTimeSeconds: BigNumber;
            salt: BigNumber;
            makerAssetData: string;
            takerAssetData: string;
        }[], txOrigin: string, transactionSignature: string, approvalExpirationTimeSeconds: BigNumber[], approvalSignatures: string[], callData?: Partial<CallData>, defaultBlock?: number | BlockParamLiteral | undefined): Promise<void>;
    };
    static deployFrom0xArtifactAsync(artifact: ContractArtifact | SimpleContractArtifact, supportedProvider: SupportedProvider, txDefaults: Partial<TxData>, _exchange: string): Promise<CoordinatorContract>;
    static deployAsync(bytecode: string, abi: ContractAbi, supportedProvider: SupportedProvider, txDefaults: Partial<TxData>, _exchange: string): Promise<CoordinatorContract>;
    constructor(abi: ContractAbi, address: string, supportedProvider: SupportedProvider, txDefaults?: Partial<TxData>);
}
//# sourceMappingURL=coordinator.d.ts.map