"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("@0x/utils");
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var dirtyChai = require("dirty-chai");
var fetchMock = require("fetch-mock");
require("mocha");
var index_1 = require("../src/index");
var asset_pairs_1 = require("./fixtures/standard_relayer_api/asset_pairs");
var assetDataPairsResponseJSON = require("./fixtures/standard_relayer_api/asset_pairs.json");
var fee_recipients_1 = require("./fixtures/standard_relayer_api/fee_recipients");
var feeRecipientsResponseJSON = require("./fixtures/standard_relayer_api/fee_recipients.json");
var _0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f_1 = require("./fixtures/standard_relayer_api/order/0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f");
var orderResponseJSON = require("./fixtures/standard_relayer_api/order/0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f.json");
var order_config_1 = require("./fixtures/standard_relayer_api/order_config");
var orderConfigResponseJSON = require("./fixtures/standard_relayer_api/order_config.json");
var orderbook_1 = require("./fixtures/standard_relayer_api/orderbook");
var orderbookJSON = require("./fixtures/standard_relayer_api/orderbook.json");
var orders_1 = require("./fixtures/standard_relayer_api/orders");
var ordersResponseJSON = require("./fixtures/standard_relayer_api/orders.json");
chai.config.includeStack = true;
chai.use(dirtyChai);
chai.use(chaiAsPromised);
var expect = chai.expect;
describe('HttpClient', function () {
    var relayUrl = 'https://example.com';
    var relayerClient = new index_1.HttpClient(relayUrl);
    beforeEach(function () {
        fetchMock.restore();
    });
    describe('#constructor', function () {
        it('should remove trailing slashes from api url', function () { return __awaiter(_this, void 0, void 0, function () {
            var urlWithTrailingSlash, urlWithoutTrailingSlash, client, sanitizedUrl;
            return __generator(this, function (_a) {
                urlWithTrailingSlash = 'https://slash.com/';
                urlWithoutTrailingSlash = 'https://slash.com';
                client = new index_1.HttpClient(urlWithTrailingSlash);
                sanitizedUrl = client._apiEndpointUrl;
                expect(sanitizedUrl).to.be.deep.equal(urlWithoutTrailingSlash);
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getAssetPairsAsync', function () {
        var url = relayUrl + "/asset_pairs";
        it('gets assetData pairs with default options when none are provided', function () { return __awaiter(_this, void 0, void 0, function () {
            var assetDataPairs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.get(url, assetDataPairsResponseJSON);
                        return [4 /*yield*/, relayerClient.getAssetPairsAsync()];
                    case 1:
                        assetDataPairs = _a.sent();
                        expect(assetDataPairs).to.be.deep.equal(asset_pairs_1.assetDataPairsResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('gets assetData pairs with specified request options', function () { return __awaiter(_this, void 0, void 0, function () {
            var assetData, assetPairsRequestOpts, urlWithQuery, assetDataPairs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assetData = '0xf47261b04c32345ced77393b3530b1eed0f346429d';
                        assetPairsRequestOpts = {
                            assetDataA: assetData,
                            page: 3,
                            perPage: 50,
                            networkdId: 42,
                        };
                        urlWithQuery = url + "?assetDataA=" + assetData + "&networkdId=42&page=3&perPage=50";
                        fetchMock.get(urlWithQuery, assetDataPairsResponseJSON);
                        return [4 /*yield*/, relayerClient.getAssetPairsAsync(assetPairsRequestOpts)];
                    case 1:
                        assetDataPairs = _a.sent();
                        expect(assetDataPairs).to.be.deep.equal(asset_pairs_1.assetDataPairsResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.get(url, { test: 'dummy' });
                expect(relayerClient.getAssetPairsAsync()).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getOrdersAsync', function () {
        var url = relayUrl + "/orders";
        it('gets orders with default options when none are provided', function () { return __awaiter(_this, void 0, void 0, function () {
            var orders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.get(url, ordersResponseJSON);
                        return [4 /*yield*/, relayerClient.getOrdersAsync()];
                    case 1:
                        orders = _a.sent();
                        expect(orders).to.be.deep.equal(orders_1.ordersResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('gets orders with specified request options', function () { return __awaiter(_this, void 0, void 0, function () {
            var assetDataAddress, ordersRequest, urlWithQuery, orders;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        assetDataAddress = '0x323b5d4c32345ced77393b3530b1eed0f346429d';
                        ordersRequest = {
                            assetDataAddress: assetDataAddress,
                            page: 3,
                            perPage: 50,
                            networkdId: 42,
                        };
                        urlWithQuery = url + "?assetDataAddress=" + assetDataAddress + "&networkdId=42&page=3&perPage=50";
                        fetchMock.get(urlWithQuery, ordersResponseJSON);
                        return [4 /*yield*/, relayerClient.getOrdersAsync(ordersRequest)];
                    case 1:
                        orders = _a.sent();
                        expect(orders).to.be.deep.equal(orders_1.ordersResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.get(url, { test: 'dummy' });
                expect(relayerClient.getOrdersAsync()).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getOrderAsync', function () {
        var orderHash = '0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f';
        var url = relayUrl + "/order/" + orderHash;
        it('gets order', function () { return __awaiter(_this, void 0, void 0, function () {
            var order;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.get(url, orderResponseJSON);
                        return [4 /*yield*/, relayerClient.getOrderAsync(orderHash)];
                    case 1:
                        order = _a.sent();
                        expect(order).to.be.deep.equal(_0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f_1.orderResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.get(url, { test: 'dummy' });
                expect(relayerClient.getOrderAsync(orderHash)).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getOrderBookAsync', function () {
        var request = {
            baseAssetData: '0x323b5d4c32345ced77393b3530b1eed0f346429d',
            quoteAssetData: '0xa2b31dacf30a9c50ca473337c01d8a201ae33e32',
        };
        var url = relayUrl + "/orderbook";
        it('gets orderbook with default page options when none are provided', function () { return __awaiter(_this, void 0, void 0, function () {
            var urlWithQuery, orderbook;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        urlWithQuery = url + "?baseAssetData=" + request.baseAssetData + "&quoteAssetData=" + request.quoteAssetData;
                        fetchMock.get(urlWithQuery, orderbookJSON);
                        return [4 /*yield*/, relayerClient.getOrderbookAsync(request)];
                    case 1:
                        orderbook = _a.sent();
                        expect(orderbook).to.be.deep.equal(orderbook_1.orderbookResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('gets orderbook with specified page options', function () { return __awaiter(_this, void 0, void 0, function () {
            var urlWithQuery, pagedRequestOptions, orderbook;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        urlWithQuery = url + "?baseAssetData=" + request.baseAssetData + "&networkId=42&page=3&perPage=50&quoteAssetData=" + request.quoteAssetData;
                        fetchMock.get(urlWithQuery, orderbookJSON);
                        pagedRequestOptions = {
                            page: 3,
                            perPage: 50,
                            networkId: 42,
                        };
                        return [4 /*yield*/, relayerClient.getOrderbookAsync(request, pagedRequestOptions)];
                    case 1:
                        orderbook = _a.sent();
                        expect(orderbook).to.be.deep.equal(orderbook_1.orderbookResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.get(url, { test: 'dummy' });
                expect(relayerClient.getOrderbookAsync(request)).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getOrderConfigAsync', function () {
        var request = {
            makerAddress: '0x9e56625509c2f60af937f23b7b532600390e8c8b',
            takerAddress: '0xa2b31dacf30a9c50ca473337c01d8a201ae33e32',
            makerAssetAmount: new utils_1.BigNumber('10000000000000000'),
            takerAssetAmount: new utils_1.BigNumber('20000000000000000'),
            expirationTimeSeconds: new utils_1.BigNumber('1532560590'),
            makerAssetData: '0xf47261b04c32345ced77393b3530b1eed0f346429d',
            takerAssetData: '0x0257179264389b814a946f3e92105513705ca6b990',
            exchangeAddress: '0x12459c951127e0c374ff9105dda097662a027093',
        };
        var url = relayUrl + "/order_config";
        it('gets order config', function () { return __awaiter(_this, void 0, void 0, function () {
            var fees;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.post(url, orderConfigResponseJSON);
                        return [4 /*yield*/, relayerClient.getOrderConfigAsync(request)];
                    case 1:
                        fees = _a.sent();
                        expect(fees).to.be.deep.equal(order_config_1.orderConfigResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('does not mutate input', function () { return __awaiter(_this, void 0, void 0, function () {
            var makerAssetAmountBefore, takerAssetAmountBefore, expirationTimeSecondsBefore;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.post(url, orderConfigResponseJSON);
                        makerAssetAmountBefore = request.makerAssetAmount;
                        takerAssetAmountBefore = request.takerAssetAmount;
                        expirationTimeSecondsBefore = request.expirationTimeSeconds;
                        return [4 /*yield*/, relayerClient.getOrderConfigAsync(request)];
                    case 1:
                        _a.sent();
                        expect(makerAssetAmountBefore).to.be.deep.equal(request.makerAssetAmount);
                        expect(takerAssetAmountBefore).to.be.deep.equal(request.takerAssetAmount);
                        expect(expirationTimeSecondsBefore).to.be.deep.equal(request.expirationTimeSeconds);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.post(url, { test: 'dummy' });
                expect(relayerClient.getOrderConfigAsync(request)).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
    describe('#getFeeRecipientsAsync', function () {
        var url = relayUrl + "/fee_recipients";
        it('gets fee recipients with default page options when none are provided', function () { return __awaiter(_this, void 0, void 0, function () {
            var feeRecipients;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetchMock.get(url, feeRecipientsResponseJSON);
                        return [4 /*yield*/, relayerClient.getFeeRecipientsAsync()];
                    case 1:
                        feeRecipients = _a.sent();
                        expect(feeRecipients).to.be.deep.equal(fee_recipients_1.feeRecipientsResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('gets fee recipient with specified page options', function () { return __awaiter(_this, void 0, void 0, function () {
            var urlWithQuery, pagedRequestOptions, feeRecipients;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        urlWithQuery = url + "?networkId=42&page=3&perPage=50";
                        fetchMock.get(urlWithQuery, feeRecipientsResponseJSON);
                        pagedRequestOptions = {
                            page: 3,
                            perPage: 50,
                            networkId: 42,
                        };
                        return [4 /*yield*/, relayerClient.getFeeRecipientsAsync(pagedRequestOptions)];
                    case 1:
                        feeRecipients = _a.sent();
                        expect(feeRecipients).to.be.deep.equal(fee_recipients_1.feeRecipientsResponse);
                        return [2 /*return*/];
                }
            });
        }); });
        it('throws an error for invalid JSON response', function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                fetchMock.get(url, { test: 'dummy' });
                expect(relayerClient.getFeeRecipientsAsync()).to.be.rejected();
                return [2 /*return*/];
            });
        }); });
    });
});
//# sourceMappingURL=http_client_test.js.map