import { BigNumber } from '@0x/utils';
export declare const orderResponse: {
    order: {
        makerAddress: string;
        takerAddress: string;
        feeRecipientAddress: string;
        senderAddress: string;
        makerAssetAmount: BigNumber;
        takerAssetAmount: BigNumber;
        makerFee: BigNumber;
        takerFee: BigNumber;
        expirationTimeSeconds: BigNumber;
        salt: BigNumber;
        makerAssetData: string;
        takerAssetData: string;
        exchangeAddress: string;
        signature: string;
    };
    metaData: {};
};
//# sourceMappingURL=0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f.d.ts.map