"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var apiOrderJSON = require("./order/0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f.json");
var apiOrderJSONString = JSON.stringify(apiOrderJSON);
exports.updateOrdersChannelMessage = "{\n    \"type\": \"update\",\n    \"channel\": \"orders\",\n    \"requestId\": \"5a1ce3a2-22b9-41e6-a615-68077512e9e2\",\n    \"payload\": [" + apiOrderJSONString + "]\n}";
exports.malformedUpdateOrdersChannelMessage = "{\n    \"type\": \"update\",\n    \"channel\": \"orders\",\n    \"requestId\": \"4d8efcee-adde-4475-9601-f0b30962ca2b\",\n    \"payload\": {}\n}";
//# sourceMappingURL=update_orders_channel_message.js.map