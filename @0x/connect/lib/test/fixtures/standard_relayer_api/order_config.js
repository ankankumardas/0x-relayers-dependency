"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("@0x/utils");
exports.orderConfigResponse = {
    senderAddress: '0xa2b31dacf30a9c50ca473337c01d8a201ae33e32',
    feeRecipientAddress: '0xb046140686d052fff581f63f8136cce132e857da',
    makerFee: new utils_1.BigNumber('100000000000000'),
    takerFee: new utils_1.BigNumber('200000000000000'),
};
//# sourceMappingURL=order_config.js.map