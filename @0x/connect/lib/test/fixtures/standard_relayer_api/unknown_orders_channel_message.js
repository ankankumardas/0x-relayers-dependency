"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var orderResponseJSON = require("./order/0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f.json");
var orderJSONString = JSON.stringify(orderResponseJSON);
exports.unknownOrdersChannelMessage = "{\n    \"type\": \"superGoodUpdate\",\n    \"channel\": \"orderbook\",\n    \"requestId\": \"6ce8c5a6-5c46-4027-a44a-51831c77b8a1\",\n    \"payload\": [" + orderJSONString + "]\n}";
//# sourceMappingURL=unknown_orders_channel_message.js.map