"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("@0x/utils");
exports.assetDataPairsResponse = {
    total: 43,
    page: 1,
    perPage: 100,
    records: [
        {
            assetDataA: {
                minAmount: new utils_1.BigNumber('0'),
                maxAmount: new utils_1.BigNumber('10000000000000000000'),
                precision: 5,
                assetData: '0xf47261b04c32345ced77393b3530b1eed0f346429d',
            },
            assetDataB: {
                minAmount: new utils_1.BigNumber('0'),
                maxAmount: new utils_1.BigNumber('50000000000000000000'),
                precision: 5,
                assetData: '0x0257179264389b814a946f3e92105513705ca6b990',
            },
        },
    ],
};
//# sourceMappingURL=asset_pairs.js.map