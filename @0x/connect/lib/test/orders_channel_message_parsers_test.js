"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai = require("chai");
var dirtyChai = require("dirty-chai");
require("mocha");
var orders_channel_message_parser_1 = require("../src/utils/orders_channel_message_parser");
var _0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f_1 = require("./fixtures/standard_relayer_api/order/0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f");
var unknown_orders_channel_message_1 = require("./fixtures/standard_relayer_api/unknown_orders_channel_message");
var update_orders_channel_message_1 = require("./fixtures/standard_relayer_api/update_orders_channel_message");
chai.config.includeStack = true;
chai.use(dirtyChai);
var expect = chai.expect;
describe('ordersChannelMessageParser', function () {
    describe('#parser', function () {
        it('parses update messages', function () {
            var updateMessage = orders_channel_message_parser_1.ordersChannelMessageParser.parse(update_orders_channel_message_1.updateOrdersChannelMessage);
            expect(updateMessage.type).to.be.equal('update');
            expect(updateMessage.payload).to.be.deep.equal([_0xabc67323774bdbd24d94f977fa9ac94a50f016026fd13f42990861238897721f_1.orderResponse]);
        });
        it('returns unknown message for messages with unsupported types', function () {
            var unknownMessage = orders_channel_message_parser_1.ordersChannelMessageParser.parse(unknown_orders_channel_message_1.unknownOrdersChannelMessage);
            expect(unknownMessage.type).to.be.equal('unknown');
            expect(unknownMessage.payload).to.be.undefined();
        });
        it('throws when message does not include a type', function () {
            var typelessMessage = "{\n                \"channel\": \"orders\",\n                \"requestId\": \"4d8efcee-adde-4475-9601-f0b30962ca2b\",\n                \"payload\": []\n            }";
            var badCall = function () { return orders_channel_message_parser_1.ordersChannelMessageParser.parse(typelessMessage); };
            expect(badCall).throws("Message is missing a type parameter: " + typelessMessage);
        });
        it('throws when type is not a string', function () {
            var messageWithBadType = "{\n                \"type\": 1,\n                \"channel\": \"orders\",\n                \"requestId\": \"4d8efcee-adde-4475-9601-f0b30962ca2b\",\n                \"payload\": []\n            }";
            var badCall = function () { return orders_channel_message_parser_1.ordersChannelMessageParser.parse(messageWithBadType); };
            expect(badCall).throws('Expected type to be of type string, encountered: 1');
        });
        it('throws when update message has malformed payload', function () {
            var badCall = function () { return orders_channel_message_parser_1.ordersChannelMessageParser.parse(update_orders_channel_message_1.malformedUpdateOrdersChannelMessage); };
            expect(badCall).throws(/^Expected message to conform to schema/);
        });
        it('throws when input message is not valid JSON', function () {
            var nonJsonString = 'h93b{sdfs9fsd f';
            var badCall = function () { return orders_channel_message_parser_1.ordersChannelMessageParser.parse(nonJsonString); };
            expect(badCall).throws('Unexpected token h in JSON at position 0');
        });
    });
});
//# sourceMappingURL=orders_channel_message_parsers_test.js.map