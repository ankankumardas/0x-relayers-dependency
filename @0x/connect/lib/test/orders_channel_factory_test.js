"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai = require("chai");
var dirtyChai = require("dirty-chai");
var _ = require("lodash");
require("mocha");
var orders_channel_factory_1 = require("../src/orders_channel_factory");
chai.config.includeStack = true;
chai.use(dirtyChai);
var expect = chai.expect;
var emptyOrdersChannelHandler = {
    onUpdate: _.noop.bind(_),
    onError: _.noop.bind(_),
    onClose: _.noop.bind(_),
};
describe('ordersChannelFactory', function () {
    var websocketUrl = 'ws://localhost:8080';
    describe('#createWebSocketOrdersChannelAsync', function () {
        it('throws when input is not a url', function () {
            var badUrlInput = 54;
            expect(orders_channel_factory_1.ordersChannelFactory.createWebSocketOrdersChannelAsync(badUrlInput, emptyOrdersChannelHandler)).to.be.rejected();
        });
        it('throws when handler has the incorrect members', function () {
            var badHandlerInput = {};
            expect(orders_channel_factory_1.ordersChannelFactory.createWebSocketOrdersChannelAsync(websocketUrl, badHandlerInput)).to.be.rejected();
        });
    });
});
//# sourceMappingURL=orders_channel_factory_test.js.map