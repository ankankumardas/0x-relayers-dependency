"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai = require("chai");
var dirtyChai = require("dirty-chai");
var _ = require("lodash");
require("mocha");
var Sinon = require("sinon");
var WebSocket = require("websocket");
var ws_orders_channel_1 = require("../src/ws_orders_channel");
chai.config.includeStack = true;
chai.use(dirtyChai);
var expect = chai.expect;
var emptyOrdersChannelHandler = {
    onUpdate: _.noop.bind(_),
    onError: _.noop.bind(_),
    onClose: _.noop.bind(_),
};
describe('WebSocketOrdersChannel', function () {
    var websocketUrl = 'ws://localhost:8080';
    var openClient = new WebSocket.w3cwebsocket(websocketUrl);
    Sinon.stub(openClient, 'readyState').get(function () { return WebSocket.w3cwebsocket.OPEN; });
    Sinon.stub(openClient, 'send').callsFake(_.noop.bind(_));
    var openOrdersChannel = new ws_orders_channel_1.WebSocketOrdersChannel(openClient, emptyOrdersChannelHandler);
    var subscriptionOpts = {
        baseAssetData: '0x323b5d4c32345ced77393b3530b1eed0f346429d',
        quoteAssetData: '0xef7fff64389b814a946f3e92105513705ca6b990',
        limit: 100,
    };
    describe('#subscribe', function () {
        it('throws when subscriptionOpts does not conform to schema', function () {
            var badSubscribeCall = openOrdersChannel.subscribe.bind(openOrdersChannel, {
                makerAssetData: 5,
            });
            expect(badSubscribeCall).throws();
        });
        it('does not throw when inputs are of correct types', function () {
            var goodSubscribeCall = openOrdersChannel.subscribe.bind(openOrdersChannel, subscriptionOpts);
            expect(goodSubscribeCall).to.not.throw();
        });
        it('throws when client is closed', function () {
            var closedClient = new WebSocket.w3cwebsocket(websocketUrl);
            Sinon.stub(closedClient, 'readyState').get(function () { return WebSocket.w3cwebsocket.CLOSED; });
            var closedOrdersChannel = new ws_orders_channel_1.WebSocketOrdersChannel(closedClient, emptyOrdersChannelHandler);
            var badSubscribeCall = closedOrdersChannel.subscribe.bind(closedOrdersChannel, subscriptionOpts);
            expect(badSubscribeCall).throws('WebSocket connection is closed');
        });
    });
});
//# sourceMappingURL=ws_orders_channel_test.js.map