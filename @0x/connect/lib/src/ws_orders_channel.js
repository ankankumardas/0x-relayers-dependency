"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var types_1 = require("@0x/types");
var uuid_1 = require("uuid");
var WebSocket = require("websocket");
var assert_1 = require("./utils/assert");
var orders_channel_message_parser_1 = require("./utils/orders_channel_message_parser");
/**
 * This class includes all the functionality related to interacting with a websocket endpoint
 * that implements the standard relayer API v0
 */
var WebSocketOrdersChannel = /** @class */ (function () {
    /**
     * Instantiates a new WebSocketOrdersChannel instance
     * @param   client               A WebSocket client
     * @param   handler              An OrdersChannelHandler instance that responds to various
     *                               channel updates
     * @return  An instance of WebSocketOrdersChannel
     */
    function WebSocketOrdersChannel(client, handler) {
        var _this = this;
        this._subscriptionOptsMap = {};
        assert_1.assert.isOrdersChannelHandler('handler', handler);
        // set private members
        this._client = client;
        this._handler = handler;
        // attach client callbacks
        this._client.onerror = function (err) {
            _this._handler.onError(_this, err);
        };
        this._client.onclose = function () {
            _this._handler.onClose(_this);
        };
        this._client.onmessage = function (message) {
            _this._handleWebSocketMessage(message);
        };
    }
    /**
     * Subscribe to orderbook snapshots and updates from the websocket
     * @param   subscriptionOpts     An OrdersChannelSubscriptionOpts instance describing which
     *                               assetData pair to subscribe to
     */
    WebSocketOrdersChannel.prototype.subscribe = function (subscriptionOpts) {
        assert_1.assert.isOrdersChannelSubscriptionOpts('subscriptionOpts', subscriptionOpts);
        assert_1.assert.assert(this._client.readyState === WebSocket.w3cwebsocket.OPEN, 'WebSocket connection is closed');
        var requestId = uuid_1.v4();
        this._subscriptionOptsMap[requestId] = subscriptionOpts;
        var subscribeMessage = {
            type: 'subscribe',
            channel: 'orders',
            requestId: requestId,
            payload: subscriptionOpts,
        };
        this._client.send(JSON.stringify(subscribeMessage));
    };
    /**
     * Close the websocket and stop receiving updates
     */
    WebSocketOrdersChannel.prototype.close = function () {
        this._client.close();
    };
    WebSocketOrdersChannel.prototype._handleWebSocketMessage = function (message) {
        if (message.data === undefined) {
            this._handler.onError(this, new Error("Message does not contain data. Url: " + this._client.url));
            return;
        }
        try {
            var data = message.data;
            var parserResult = orders_channel_message_parser_1.ordersChannelMessageParser.parse(data);
            var subscriptionOpts = this._subscriptionOptsMap[parserResult.requestId];
            if (subscriptionOpts === undefined) {
                this._handler.onError(this, new Error("Message has unknown requestId. Url: " + this._client.url + " Message: " + data));
                return;
            }
            switch (parserResult.type) {
                case types_1.OrdersChannelMessageTypes.Update: {
                    this._handler.onUpdate(this, subscriptionOpts, parserResult.payload);
                    break;
                }
                default: {
                    this._handler.onError(this, new Error("Message has unknown type parameter. Url: " + this._client.url + " Message: " + data), subscriptionOpts);
                }
            }
        }
        catch (error) {
            this._handler.onError(this, error);
        }
    };
    return WebSocketOrdersChannel;
}());
exports.WebSocketOrdersChannel = WebSocketOrdersChannel;
//# sourceMappingURL=ws_orders_channel.js.map