import { OrdersChannel, OrdersChannelHandler } from './types';
export declare const ordersChannelFactory: {
    /**
     * Instantiates a new WebSocketOrdersChannel instance
     * @param   url                  The relayer API base WS url you would like to interact with
     * @param   handler              An OrdersChannelHandler instance that responds to various
     *                               channel updates
     * @return  An OrdersChannel Promise
     */
    createWebSocketOrdersChannelAsync(url: string, handler: OrdersChannelHandler): Promise<OrdersChannel>;
};
//# sourceMappingURL=orders_channel_factory.d.ts.map