import { APIOrder, AssetPairsRequestOpts, AssetPairsResponse, FeeRecipientsResponse, OrderbookRequest, OrderbookResponse, OrderConfigRequest, OrderConfigResponse, OrdersRequestOpts, OrdersResponse, PagedRequestOpts, RequestOpts, SignedOrder } from '@0x/types';
import { Client } from './types';
/**
 * This class includes all the functionality related to interacting with a set of HTTP endpoints
 * that implement the standard relayer API v2
 */
export declare class HttpClient implements Client {
    private readonly _apiEndpointUrl;
    /**
     * Format parameters to be appended to http requests into query string form
     */
    private static _buildQueryStringFromHttpParams;
    /**
     * Instantiates a new HttpClient instance
     * @param   url    The relayer API base HTTP url you would like to interact with
     * @return  An instance of HttpClient
     */
    constructor(url: string);
    /**
     * Retrieve assetData pair info from the API
     * @param   requestOpts     Options specifying assetData information to retrieve, page information, and network id.
     * @return  The resulting AssetPairsResponse that match the request
     */
    getAssetPairsAsync(requestOpts?: RequestOpts & AssetPairsRequestOpts & PagedRequestOpts): Promise<AssetPairsResponse>;
    /**
     * Retrieve orders from the API
     * @param   requestOpts     Options specifying orders to retrieve and page information, page information, and network id.
     * @return  The resulting OrdersResponse that match the request
     */
    getOrdersAsync(requestOpts?: RequestOpts & OrdersRequestOpts & PagedRequestOpts): Promise<OrdersResponse>;
    /**
     * Retrieve a specific order from the API
     * @param   orderHash     An orderHash generated from the desired order
     * @return  The APIOrder that matches the supplied orderHash
     */
    getOrderAsync(orderHash: string, requestOpts?: RequestOpts): Promise<APIOrder>;
    /**
     * Retrieve an orderbook from the API
     * @param   request         An OrderbookRequest instance describing the specific orderbook to retrieve
     * @param   requestOpts     Options specifying page information, and network id.
     * @return  The resulting OrderbookResponse that matches the request
     */
    getOrderbookAsync(request: OrderbookRequest, requestOpts?: RequestOpts & PagedRequestOpts): Promise<OrderbookResponse>;
    /**
     * Retrieve fee information from the API
     * @param   request         A OrderConfigRequest instance describing the specific fees to retrieve
     * @param   requestOpts     Options specifying network id.
     * @return  The resulting OrderConfigResponse that matches the request
     */
    getOrderConfigAsync(request: OrderConfigRequest, requestOpts?: RequestOpts): Promise<OrderConfigResponse>;
    /**
     * Retrieve the list of fee recipient addresses used by the relayer.
     * @param   requestOpts     Options specifying page information, and network id.
     * @return  The resulting FeeRecipientsResponse
     */
    getFeeRecipientsAsync(requestOpts?: RequestOpts & PagedRequestOpts): Promise<FeeRecipientsResponse>;
    /**
     * Submit a signed order to the API
     * @param   signedOrder     A SignedOrder instance to submit
     * @param   requestOpts     Options specifying network id.
     */
    submitOrderAsync(signedOrder: SignedOrder, requestOpts?: RequestOpts): Promise<void>;
    private _requestAsync;
}
//# sourceMappingURL=http_client.d.ts.map