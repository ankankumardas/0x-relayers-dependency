import { APIOrder, AssetPairsItem, OrderbookResponse, OrderConfigResponse } from '@0x/types';
export declare const relayerResponseJsonParsers: {
    parseAssetDataPairsJson(json: any): import("@0x/types").PaginatedCollection<AssetPairsItem>;
    parseAssetPairsItemsJson(json: any): AssetPairsItem[];
    parseOrdersJson(json: any): import("@0x/types").PaginatedCollection<APIOrder>;
    parseAPIOrdersJson(json: any): APIOrder[];
    parseAPIOrderJson(json: any): APIOrder;
    parseOrderbookResponseJson(json: any): OrderbookResponse;
    parseOrderConfigResponseJson(json: any): OrderConfigResponse;
};
//# sourceMappingURL=relayer_response_json_parsers.d.ts.map