import { APIOrder } from '@0x/types';
export declare const typeConverters: {
    convertOrderbookStringFieldsToBigNumber(orderbook: any): any;
    convertAPIOrderStringFieldsToBigNumber(apiOrder: any): APIOrder;
};
//# sourceMappingURL=type_converters.d.ts.map