"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var order_utils_1 = require("@0x/order-utils");
var _ = require("lodash");
exports.typeConverters = {
    convertOrderbookStringFieldsToBigNumber: function (orderbook) {
        var bids = _.get(orderbook, 'bids', []);
        var asks = _.get(orderbook, 'asks', []);
        var convertedBids = __assign({}, bids, { records: bids.records.map(function (order) { return exports.typeConverters.convertAPIOrderStringFieldsToBigNumber(order); }) });
        var convertedAsks = __assign({}, asks, { records: asks.records.map(function (order) { return exports.typeConverters.convertAPIOrderStringFieldsToBigNumber(order); }) });
        return {
            bids: convertedBids,
            asks: convertedAsks,
        };
    },
    convertAPIOrderStringFieldsToBigNumber: function (apiOrder) {
        return __assign({}, apiOrder, { order: order_utils_1.orderParsingUtils.convertOrderStringFieldsToBigNumber(apiOrder.order) });
    },
};
//# sourceMappingURL=type_converters.js.map