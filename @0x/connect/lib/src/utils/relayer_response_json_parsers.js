"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert_1 = require("@0x/assert");
var json_schemas_1 = require("@0x/json-schemas");
var order_utils_1 = require("@0x/order-utils");
var type_converters_1 = require("./type_converters");
exports.relayerResponseJsonParsers = {
    parseAssetDataPairsJson: function (json) {
        assert_1.assert.doesConformToSchema('assetDataPairsResponse', json, json_schemas_1.schemas.relayerApiAssetDataPairsResponseSchema);
        return __assign({}, json, { records: exports.relayerResponseJsonParsers.parseAssetPairsItemsJson(json.records) });
    },
    parseAssetPairsItemsJson: function (json) {
        return json.map(function (assetDataPair) {
            return order_utils_1.orderParsingUtils.convertStringsFieldsToBigNumbers(assetDataPair, [
                'assetDataA.minAmount',
                'assetDataA.maxAmount',
                'assetDataB.minAmount',
                'assetDataB.maxAmount',
            ]);
        });
    },
    parseOrdersJson: function (json) {
        assert_1.assert.doesConformToSchema('relayerApiOrdersResponse', json, json_schemas_1.schemas.relayerApiOrdersResponseSchema);
        return __assign({}, json, { records: exports.relayerResponseJsonParsers.parseAPIOrdersJson(json.records) });
    },
    parseAPIOrdersJson: function (json) {
        return json.map(exports.relayerResponseJsonParsers.parseAPIOrderJson.bind(exports.relayerResponseJsonParsers));
    },
    parseAPIOrderJson: function (json) {
        assert_1.assert.doesConformToSchema('relayerApiOrder', json, json_schemas_1.schemas.relayerApiOrderSchema);
        return type_converters_1.typeConverters.convertAPIOrderStringFieldsToBigNumber(json);
    },
    parseOrderbookResponseJson: function (json) {
        assert_1.assert.doesConformToSchema('orderBookResponse', json, json_schemas_1.schemas.relayerApiOrderbookResponseSchema);
        return type_converters_1.typeConverters.convertOrderbookStringFieldsToBigNumber(json);
    },
    parseOrderConfigResponseJson: function (json) {
        assert_1.assert.doesConformToSchema('orderConfigResponse', json, json_schemas_1.schemas.relayerApiOrderConfigResponseSchema);
        return order_utils_1.orderParsingUtils.convertStringsFieldsToBigNumbers(json, ['makerFee', 'takerFee']);
    },
};
//# sourceMappingURL=relayer_response_json_parsers.js.map