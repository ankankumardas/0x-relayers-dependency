import { OrdersChannelMessage } from '@0x/types';
export declare const ordersChannelMessageParser: {
    parse(utf8Data: string): OrdersChannelMessage;
};
//# sourceMappingURL=orders_channel_message_parser.d.ts.map