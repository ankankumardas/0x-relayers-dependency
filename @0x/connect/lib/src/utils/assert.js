"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert_1 = require("@0x/assert");
// HACK: We need those two unused imports because they're actually used by sharedAssert which gets injected here
// tslint:disable-next-line:no-unused-variable
var json_schemas_1 = require("@0x/json-schemas");
var _ = require("lodash");
exports.assert = __assign({}, assert_1.assert, { isOrdersChannelSubscriptionOpts: function (variableName, subscriptionOpts) {
        assert_1.assert.doesConformToSchema(variableName, subscriptionOpts, json_schemas_1.schemas.relayerApiOrdersChannelSubscribePayloadSchema);
    },
    isOrdersChannelHandler: function (variableName, handler) {
        assert_1.assert.isFunction(variableName + ".onUpdate", _.get(handler, 'onUpdate'));
        assert_1.assert.isFunction(variableName + ".onError", _.get(handler, 'onError'));
        assert_1.assert.isFunction(variableName + ".onClose", _.get(handler, 'onClose'));
    } });
//# sourceMappingURL=assert.js.map