"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var assert_1 = require("@0x/assert");
var json_schemas_1 = require("@0x/json-schemas");
var _ = require("lodash");
var types_1 = require("@0x/types");
var relayer_response_json_parsers_1 = require("./relayer_response_json_parsers");
exports.ordersChannelMessageParser = {
    parse: function (utf8Data) {
        // parse the message
        var messageObj = JSON.parse(utf8Data);
        // ensure we have a type parameter to switch on
        var type = _.get(messageObj, 'type');
        assert_1.assert.assert(type !== undefined, "Message is missing a type parameter: " + utf8Data);
        assert_1.assert.isString('type', type);
        // ensure we have a request id for the resulting message
        var requestId = _.get(messageObj, 'requestId');
        assert_1.assert.assert(requestId !== undefined, "Message is missing a requestId parameter: " + utf8Data);
        assert_1.assert.isString('requestId', requestId);
        switch (type) {
            case types_1.OrdersChannelMessageTypes.Update: {
                assert_1.assert.doesConformToSchema('message', messageObj, json_schemas_1.schemas.relayerApiOrdersChannelUpdateSchema);
                var ordersJson = messageObj.payload;
                var orders = relayer_response_json_parsers_1.relayerResponseJsonParsers.parseAPIOrdersJson(ordersJson);
                return _.assign(messageObj, { payload: orders });
            }
            default: {
                return {
                    type: types_1.OrdersChannelMessageTypes.Unknown,
                    requestId: requestId,
                    payload: undefined,
                };
            }
        }
    },
};
//# sourceMappingURL=orders_channel_message_parser.js.map