import { OrdersChannelSubscriptionOpts } from '@0x/types';
import * as WebSocket from 'websocket';
import { OrdersChannel, OrdersChannelHandler } from './types';
export interface OrdersChannelSubscriptionOptsMap {
    [key: string]: OrdersChannelSubscriptionOpts;
}
/**
 * This class includes all the functionality related to interacting with a websocket endpoint
 * that implements the standard relayer API v0
 */
export declare class WebSocketOrdersChannel implements OrdersChannel {
    private readonly _client;
    private readonly _handler;
    private readonly _subscriptionOptsMap;
    /**
     * Instantiates a new WebSocketOrdersChannel instance
     * @param   client               A WebSocket client
     * @param   handler              An OrdersChannelHandler instance that responds to various
     *                               channel updates
     * @return  An instance of WebSocketOrdersChannel
     */
    constructor(client: WebSocket.w3cwebsocket, handler: OrdersChannelHandler);
    /**
     * Subscribe to orderbook snapshots and updates from the websocket
     * @param   subscriptionOpts     An OrdersChannelSubscriptionOpts instance describing which
     *                               assetData pair to subscribe to
     */
    subscribe(subscriptionOpts: OrdersChannelSubscriptionOpts): void;
    /**
     * Close the websocket and stop receiving updates
     */
    close(): void;
    private _handleWebSocketMessage;
}
//# sourceMappingURL=ws_orders_channel.d.ts.map