"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HttpRequestType;
(function (HttpRequestType) {
    HttpRequestType["Get"] = "GET";
    HttpRequestType["Post"] = "POST";
})(HttpRequestType = exports.HttpRequestType || (exports.HttpRequestType = {}));
//# sourceMappingURL=types.js.map