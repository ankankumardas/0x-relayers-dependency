import { Block } from "./models/block";
import { List as ImmutableList } from "immutable";
declare type GetBlockByHash<TBlock> = (hash: string) => Promise<TBlock | null>;
export declare const reconcileBlockHistory: <TBlock extends Block>(getBlockByHash: GetBlockByHash<TBlock>, blockHistory: ImmutableList<TBlock> | Promise<ImmutableList<TBlock>>, newBlock: TBlock, onBlockAdded: (block: TBlock) => Promise<void>, onBlockRemoved: (block: TBlock) => Promise<void>, blockRetention?: number) => Promise<ImmutableList<TBlock>>;
export {};
