"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var block_reconciler_1 = require("../source/block-reconciler");
var log_reconciler_1 = require("../source/log-reconciler");
var index_1 = require("../source/index");
var helpers_1 = require("./helpers");
var chai_1 = require("chai");
var chaiAsPromised = require("chai-as-promised");
var chaiImmutable = require("chai-immutable");
var immutable_1 = require("immutable");
require("mocha");
chai_1.use(chaiImmutable);
chai_1.use(chaiAsPromised);
describe("reconcileBlockHistory", function () {
    var newBlockAnnouncements;
    var blockRemovalAnnouncments;
    var onBlockAdded = function (block) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, helpers_1.delay(0)];
            case 1:
                _a.sent();
                newBlockAnnouncements.push(block);
                return [2 /*return*/];
        }
    }); }); };
    var onBlockRemoved = function (block) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, helpers_1.delay(0)];
            case 1:
                _a.sent();
                blockRemovalAnnouncments.push(block);
                return [2 /*return*/];
        }
    }); }); };
    beforeEach(function () {
        newBlockAnnouncements = [];
        blockRemovalAnnouncments = [];
    });
    it("announces new head when first block is added to history", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List());
                    newBlock = new helpers_1.MockBlock(0x7777);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([newBlock]);
                    chai_1.expect(newBlockAnnouncements).to.deep.include(newBlock);
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("does not announce new block on repeat of current head", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([new helpers_1.MockBlock(0x7777)]));
                    newBlock = new helpers_1.MockBlock(0x7777);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _c.sent();
                    _b = (_a = chai_1.expect(newHistory).to).equal;
                    return [4 /*yield*/, oldHistory];
                case 2:
                    _b.apply(_a, [_c.sent()]);
                    chai_1.expect(newBlockAnnouncements).to.be.empty;
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("announces a new head when nth block is added to history", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x7779);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779)
                    ]);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([newBlock]);
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("ignores blocks already in history", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x7778);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _c.sent();
                    _b = (_a = chai_1.expect(newHistory).to).equal;
                    return [4 /*yield*/, oldHistory];
                case 2:
                    _b.apply(_a, [_c.sent()]);
                    chai_1.expect(newBlockAnnouncements).to.be.empty;
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("does a multi-block rollback to attach new block to head", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x777A)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x7779, "BBBB", "AAAA");
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7777, "AAAA"),
                        new helpers_1.MockBlock(0x7778, "AAAA"),
                        new helpers_1.MockBlock(0x7779, "BBBB", "AAAA")
                    ]);
                    chai_1.expect(newHistory.count()).to.equal(3);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([newBlock]);
                    chai_1.expect(blockRemovalAnnouncments).to.deep.equal([
                        new helpers_1.MockBlock(0x777A),
                        new helpers_1.MockBlock(0x7779)
                    ]);
                    return [2 /*return*/];
            }
        });
    }); });
    it("backfills missing blocks", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x777B);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x777A),
                        new helpers_1.MockBlock(0x777B)
                    ]);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x777A),
                        new helpers_1.MockBlock(0x777B)
                    ]);
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("rolls back and backfills if necessary", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, getBlockByHash, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x777A)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x777B, "BBBB", "BBBB");
                    getBlockByHash = helpers_1.getBlockByHashFactory([
                        new helpers_1.MockBlock(0x777A, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x7779, "BBBB", "AAAA")
                    ]);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(getBlockByHash, oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7777, "AAAA", "AAAA"),
                        new helpers_1.MockBlock(0x7778, "AAAA", "AAAA"),
                        new helpers_1.MockBlock(0x7779, "BBBB", "AAAA"),
                        new helpers_1.MockBlock(0x777A, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x777B, "BBBB", "BBBB"),
                    ]);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([
                        new helpers_1.MockBlock(0x7779, "BBBB", "AAAA"),
                        new helpers_1.MockBlock(0x777A, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x777B, "BBBB", "BBBB"),
                    ]);
                    chai_1.expect(blockRemovalAnnouncments).to.deep.equal([
                        new helpers_1.MockBlock(0x777A, "AAAA", "AAAA"),
                        new helpers_1.MockBlock(0x7779, "AAAA", "AAAA"),
                    ]);
                    return [2 /*return*/];
            }
        });
    }); });
    it("resets history if reconciliation not possible", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x7778, "BBBB", "BBBB");
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved, 5)];
                case 1:
                    newHistory = _c.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7776, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x7777, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x7778, "BBBB", "BBBB"),
                    ]);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([
                        new helpers_1.MockBlock(0x7776, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x7777, "BBBB", "BBBB"),
                        new helpers_1.MockBlock(0x7778, "BBBB", "BBBB"),
                    ]);
                    _b = (_a = chai_1.expect(blockRemovalAnnouncments).to.deep).equal;
                    return [4 /*yield*/, oldHistory];
                case 2:
                    _b.apply(_a, [(_c.sent()).reverse().toJS()]);
                    return [2 /*return*/];
            }
        });
    }); });
    it("throws if block fetching of parent during backfill fails", function () { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        var getBlockByHash, oldHistory, newBlock, newHistoryPromise;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    getBlockByHash = function (hash) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, helpers_1.delay(0)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/, null];
                        }
                    }); }); };
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778)
                    ]));
                    newBlock = new helpers_1.MockBlock(0x777B);
                    newHistoryPromise = block_reconciler_1.reconcileBlockHistory(getBlockByHash, oldHistory, newBlock, onBlockAdded, onBlockRemoved);
                    return [4 /*yield*/, chai_1.expect(newHistoryPromise).to.eventually.be.rejectedWith(Error, "Failed to fetch parent block.")];
                case 1:
                    _a.sent();
                    chai_1.expect(newBlockAnnouncements).to.be.empty;
                    chai_1.expect(blockRemovalAnnouncments).to.be.empty;
                    return [2 /*return*/];
            }
        });
    }); });
    it("wipes out history if new block is older than oldest block in history", function () { return __awaiter(_this, void 0, void 0, function () {
        var oldHistory, newBlock, newHistory;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    oldHistory = Promise.resolve(immutable_1.List([
                        new helpers_1.MockBlock(0x7777),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x777A),
                    ]));
                    newBlock = new helpers_1.MockBlock(0x7776);
                    return [4 /*yield*/, block_reconciler_1.reconcileBlockHistory(helpers_1.getBlockByHashFactory(), oldHistory, newBlock, onBlockAdded, onBlockRemoved)];
                case 1:
                    newHistory = _a.sent();
                    chai_1.expect(newHistory.toJS()).to.deep.equal([
                        new helpers_1.MockBlock(0x7776),
                    ]);
                    chai_1.expect(newBlockAnnouncements).to.deep.equal([
                        new helpers_1.MockBlock(0x7776),
                    ]);
                    chai_1.expect(blockRemovalAnnouncments).to.deep.equal([
                        new helpers_1.MockBlock(0x777A),
                        new helpers_1.MockBlock(0x7779),
                        new helpers_1.MockBlock(0x7778),
                        new helpers_1.MockBlock(0x7777),
                    ]);
                    return [2 /*return*/];
            }
        });
    }); });
});
describe("reconcileLogHistoryWithAddedBlock", function () { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var newLogAnnouncements, onLogAdded;
    return __generator(this, function (_a) {
        onLogAdded = function (log) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, helpers_1.delay(0)];
                case 1:
                    _a.sent();
                    newLogAnnouncements.push(log);
                    return [2 /*return*/];
            }
        }); }); };
        beforeEach(function () {
            newLogAnnouncements = [];
        });
        it("does not fetch logs if no filters are applied", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var called, getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        called = 0;
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            ++called;
                            return [2 /*return*/, Promise.resolve([])];
                        }); }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded)];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory).to.deep.equal(immutable_1.List());
                        chai_1.expect(newLogAnnouncements).to.be.empty;
                        chai_1.expect(called).to.equal(0);
                        return [2 /*return*/];
                }
            });
        }); });
        it("adds block with no logs", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, Promise.resolve([])];
                        }); }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}])];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory).to.deep.equal(immutable_1.List());
                        chai_1.expect(newLogAnnouncements).to.be.empty;
                        return [2 /*return*/];
                }
            });
        }); });
        it("adds block with logs", function () { return __awaiter(_this, void 0, void 0, function () {
            var getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = helpers_1.getLogsFactory(1);
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}])];
                    case 1:
                        newLogHistory = _a.sent();
                        // unfortunately, because we have an immutable list of a complex object with a nested list of a complex object in it, we can't do a normal equality comparison
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([new helpers_1.MockLog(0x7777)]);
                        chai_1.expect(newLogAnnouncements).to.deep.equal([new helpers_1.MockLog(0x7777)]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("adds block with multiple logs", function () { return __awaiter(_this, void 0, void 0, function () {
            var getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = helpers_1.getLogsFactory(2);
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}])];
                    case 1:
                        newLogHistory = _a.sent();
                        // unfortunately, because we have an immutable list of a complex object with a nested list of a complex object in it, we can't do a normal equality comparison
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                        ]);
                        chai_1.expect(newLogAnnouncements).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("orders logs by index", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2 /*return*/, Promise.resolve([
                                        new helpers_1.MockLog(0x7777, 0x1),
                                        new helpers_1.MockLog(0x7777, 0x2),
                                        new helpers_1.MockLog(0x7777, 0x0),
                                        new helpers_1.MockLog(0x7777, 0x3),
                                    ])];
                            });
                        }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}])];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                            new helpers_1.MockLog(0x7777, 0x2),
                            new helpers_1.MockLog(0x7777, 0x3),
                        ]);
                        chai_1.expect(newLogAnnouncements).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                            new helpers_1.MockLog(0x7777, 0x2),
                            new helpers_1.MockLog(0x7777, 0x3),
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("fails if getLogs fails", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogs, newBlock, oldLogHistory, newLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, helpers_1.delay(0)];
                                case 1:
                                    _a.sent();
                                    throw new Error("apple");
                            }
                        }); }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        newLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}]);
                        return [4 /*yield*/, chai_1.expect(newLogHistoryPromise).to.eventually.be.rejectedWith(Error, "apple")];
                    case 1:
                        _a.sent();
                        chai_1.expect(newLogAnnouncements).to.be.empty;
                        return [2 /*return*/];
                }
            });
        }); });
        it("fails if onNewLog fails", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogs, failingOnLogAdded, newBlock, oldLogHistory, newLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = helpers_1.getLogsFactory(1);
                        failingOnLogAdded = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, helpers_1.delay(0)];
                                case 1:
                                    _a.sent();
                                    throw new Error("banana");
                            }
                        }); }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        newLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, failingOnLogAdded, [{}]);
                        return [4 /*yield*/, chai_1.expect(newLogHistoryPromise).to.eventually.rejectedWith(Error, "banana")];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it("fails if old block with logs is added before new block with logs is removed", function () { return __awaiter(_this, void 0, void 0, function () {
            var getLogs, firstBlock, secondBlock, oldLogHistory, firstLogHistory, secondLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = helpers_1.getLogsFactory(1);
                        firstBlock = new helpers_1.MockBlock(0x7777);
                        secondBlock = new helpers_1.MockBlock(0x7776);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, firstBlock, onLogAdded, [{}])];
                    case 1:
                        firstLogHistory = _a.sent();
                        secondLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, firstLogHistory, secondBlock, onLogAdded, [{}]);
                        return [4 /*yield*/, chai_1.expect(secondLogHistoryPromise).to.eventually.rejectedWith(Error, /received log for a block (.*?) older than current head log's block (.*?)/)];
                    case 2:
                        _a.sent();
                        // unfortunate reality
                        chai_1.expect(newLogAnnouncements).to.deep.equal([new helpers_1.MockLog(0x7777)]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("dedupes logs with same blockhash and index from multiple filters", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogs, newBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2 /*return*/, Promise.resolve([
                                        new helpers_1.MockLog(0x7777, 0x0),
                                        new helpers_1.MockLog(0x7777, 0x1),
                                    ])];
                            });
                        }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, onLogAdded, [{}, {}])];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogAnnouncements).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("announces successful log before failing log history update (documented behavior, undesirable)", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var first, getLogs, failingOnLogAdded, newBlock, oldLogHistory, newLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        first = true;
                        getLogs = helpers_1.getLogsFactory(2);
                        failingOnLogAdded = function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, helpers_1.delay(0)];
                                    case 1:
                                        _a.sent();
                                        if (first)
                                            first = false;
                                        else
                                            throw new Error("banana");
                                        return [2 /*return*/];
                                }
                            });
                        }); };
                        newBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        newLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithAddedBlock(getLogs, oldLogHistory, newBlock, failingOnLogAdded, [{}]);
                        return [4 /*yield*/, chai_1.expect(newLogHistoryPromise).to.eventually.rejectedWith(Error, "banana")];
                    case 1:
                        _a.sent();
                        chai_1.expect(first).to.be.false;
                        return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
describe("reconcileLogHistoryWithRemovedBlock", function () { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var removedLogAnnouncements, onLogRemoved;
    return __generator(this, function (_a) {
        onLogRemoved = function (log) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, helpers_1.delay(0)];
                case 1:
                    _a.sent();
                    removedLogAnnouncements.push(log);
                    return [2 /*return*/];
            }
        }); }); };
        beforeEach(function () {
            removedLogAnnouncements = [];
        });
        it("returns empty log history when starting with null log", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List());
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved)];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory.toJS()).to.be.empty;
                        chai_1.expect(removedLogAnnouncements).to.be.empty;
                        return [2 /*return*/];
                }
            });
        }); });
        it("handles block removal with no associated logs", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List([new helpers_1.MockLog(0x7776)]));
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved)];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([new helpers_1.MockLog(0x7776)]);
                        chai_1.expect(removedLogAnnouncements).to.be.empty;
                        return [2 /*return*/];
                }
            });
        }); });
        it("removes logs at head for given block", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7776),
                            new helpers_1.MockLog(0x7777),
                        ]));
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved)];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7776),
                        ]);
                        chai_1.expect(removedLogAnnouncements).to.deep.equal([new helpers_1.MockLog(0x7777)]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("removes multiple logs in reverse order for same block", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistory;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7776),
                            new helpers_1.MockLog(0x7777, 0x1),
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x2),
                        ]));
                        return [4 /*yield*/, log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved)];
                    case 1:
                        newLogHistory = _a.sent();
                        chai_1.expect(newLogHistory.toJS()).to.deep.equal([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7776),
                        ]);
                        chai_1.expect(removedLogAnnouncements).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x2),
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("throws if removed block is not at head", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7776);
                        oldLogHistory = Promise.resolve(immutable_1.List([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7776),
                            new helpers_1.MockLog(0x7777),
                        ]));
                        newLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved);
                        return [4 /*yield*/, chai_1.expect(newLogHistoryPromise).to.eventually.rejectedWith(Error, "found logs for removed block not at head of log history")];
                    case 1:
                        _a.sent();
                        chai_1.expect(removedLogAnnouncements).to.be.empty;
                        return [2 /*return*/];
                }
            });
        }); });
        it("removes head logs for block before throwing upon finding nonhead logs for block", function () { return __awaiter(_this, void 0, void 0, function () {
            var removedBlock, oldLogHistory, newLogHistoryPromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        removedBlock = new helpers_1.MockBlock(0x7777);
                        oldLogHistory = Promise.resolve(immutable_1.List([
                            new helpers_1.MockLog(0x7775),
                            new helpers_1.MockLog(0x7777),
                            new helpers_1.MockLog(0x7776),
                            new helpers_1.MockLog(0x7777, 0x0),
                            new helpers_1.MockLog(0x7777, 0x1),
                        ]));
                        newLogHistoryPromise = log_reconciler_1.reconcileLogHistoryWithRemovedBlock(oldLogHistory, removedBlock, onLogRemoved);
                        return [4 /*yield*/, chai_1.expect(newLogHistoryPromise).to.eventually.rejectedWith(Error, "found logs for removed block not at head of log history")];
                    case 1:
                        _a.sent();
                        chai_1.expect(removedLogAnnouncements).to.deep.equal([
                            new helpers_1.MockLog(0x7777, 0x1),
                            new helpers_1.MockLog(0x7777, 0x0),
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
describe("BlockAndLogStreamer", function () { return __awaiter(_this, void 0, void 0, function () {
    var _this = this;
    var blockAndLogStreamer, announcements, onBlockAdded, onBlockRemoved, onLogAdded, onLogRemoved, onError, reinitialize;
    return __generator(this, function (_a) {
        onBlockAdded = function (block) { return announcements.push({ addition: true, item: block }); };
        onBlockRemoved = function (block) { return announcements.push({ addition: false, item: block }); };
        onLogAdded = function (log) { return announcements.push({ addition: true, item: log }); };
        onLogRemoved = function (log) { return announcements.push({ addition: false, item: log }); };
        onError = function (error) { return announcements.push({ addition: true, item: error }); };
        reinitialize = function (getBlockByHash, getLogs) {
            blockAndLogStreamer = new index_1.BlockAndLogStreamer(getBlockByHash, getLogs, onError, { blockRetention: 5 });
            blockAndLogStreamer.addLogFilter({});
            blockAndLogStreamer.subscribeToOnBlockAdded(onBlockAdded);
            blockAndLogStreamer.subscribeToOnBlockRemoved(onBlockRemoved);
            blockAndLogStreamer.subscribeToOnLogAdded(onLogAdded);
            blockAndLogStreamer.subscribeToOnLogRemoved(onLogRemoved);
            announcements = [];
        };
        beforeEach(function () {
            reinitialize(helpers_1.getBlockByHashFactory(), helpers_1.getLogsFactory(1));
        });
        it("announces new blocks and logs", function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777) },
                            { addition: true, item: new helpers_1.MockLog(0x7777, 0) },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("announces removed blocks and logs", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var logs, getLogs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logs = [new helpers_1.MockLog(0x7777, 0, 'AAAA'), new helpers_1.MockLog(0x7778, 0, 'AAAA'), new helpers_1.MockLog(0x7778, 0, 'BBBB')];
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, [logs.shift()]];
                        }); }); };
                        reinitialize(helpers_1.getBlockByHashFactory(), getLogs);
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777, "AAAA"))];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7778, "AAAA"))];
                    case 2:
                        _a.sent();
                        announcements = [];
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7778, "BBBB", "AAAA"))];
                    case 3:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: false, item: new helpers_1.MockLog(0x7778, 0, 'AAAA') },
                            { addition: false, item: new helpers_1.MockBlock(0x7778, "AAAA", "AAAA") },
                            { addition: true, item: new helpers_1.MockBlock(0x7778, "BBBB", "AAAA") },
                            { addition: true, item: new helpers_1.MockLog(0x7778, 0, 'BBBB') },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("latest block is latest fully reconciled block", function () { return __awaiter(_this, void 0, void 0, function () {
            var promise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        promise = blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779));
                        chai_1.expect(blockAndLogStreamer.getLatestReconciledBlock()).to.deep.equal(new helpers_1.MockBlock(0x7777));
                        return [4 /*yield*/, promise];
                    case 2:
                        _a.sent();
                        chai_1.expect(blockAndLogStreamer.getLatestReconciledBlock()).to.deep.equal(new helpers_1.MockBlock(0x7779));
                        return [2 /*return*/];
                }
            });
        }); });
        it("adding multiple blocks in quick succession results in expected callbacks", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var logs, getLogs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logs = [
                            new helpers_1.MockLog(0x7777, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7778, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7779, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7779, 0, 'BBBB'),
                        ];
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, [logs.shift()]];
                        }); }); };
                        reinitialize(helpers_1.getBlockByHashFactory(), getLogs);
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777, "AAAA", "AAAA"));
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "AAAA", "AAAA"));
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "BBBB", "AAAA"))];
                    case 1:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777, "AAAA", "AAAA") },
                            { addition: true, item: new helpers_1.MockLog(0x7777, 0) },
                            { addition: true, item: new helpers_1.MockBlock(0x7778, "AAAA", "AAAA") },
                            { addition: true, item: new helpers_1.MockLog(0x7778, 0) },
                            { addition: true, item: new helpers_1.MockBlock(0x7779, "AAAA", "AAAA") },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0) },
                            { addition: false, item: new helpers_1.MockLog(0x7779, 0) },
                            { addition: false, item: new helpers_1.MockBlock(0x7779, "AAAA", "AAAA") },
                            { addition: true, item: new helpers_1.MockBlock(0x7779, "BBBB", "AAAA") },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0, "BBBB") },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("swallows errors from callbacks", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var logs, getLogs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logs = [
                            new helpers_1.MockLog(0x7777, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7778, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7779, 0, 'AAAA'),
                            new helpers_1.MockLog(0x7779, 0, 'BBBB'),
                        ];
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, [logs.shift()]];
                        }); }); };
                        reinitialize(helpers_1.getBlockByHashFactory(), getLogs);
                        blockAndLogStreamer.subscribeToOnBlockAdded(function (block) { throw new Error("apple"); });
                        blockAndLogStreamer.subscribeToOnBlockRemoved(function (block) { throw new Error("banana"); });
                        blockAndLogStreamer.subscribeToOnLogAdded(function (log) { throw new Error("cherry"); });
                        blockAndLogStreamer.subscribeToOnLogRemoved(function (log) { throw new Error("durian"); });
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777, "AAAA", "AAAA"));
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "AAAA", "AAAA"));
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "BBBB", "AAAA"))];
                    case 1:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777, "AAAA", "AAAA") },
                            { addition: true, item: new Error("apple") },
                            { addition: true, item: new helpers_1.MockLog(0x7777, 0) },
                            { addition: true, item: new Error("cherry") },
                            { addition: true, item: new helpers_1.MockBlock(0x7778, "AAAA", "AAAA") },
                            { addition: true, item: new Error("apple") },
                            { addition: true, item: new helpers_1.MockLog(0x7778, 0) },
                            { addition: true, item: new Error("cherry") },
                            { addition: true, item: new helpers_1.MockBlock(0x7779, "AAAA", "AAAA") },
                            { addition: true, item: new Error("apple") },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0) },
                            { addition: true, item: new Error("cherry") },
                            { addition: false, item: new helpers_1.MockLog(0x7779, 0) },
                            { addition: true, item: new Error("durian") },
                            { addition: false, item: new helpers_1.MockBlock(0x7779, "AAAA", "AAAA") },
                            { addition: true, item: new Error("banana") },
                            { addition: true, item: new helpers_1.MockBlock(0x7779, "BBBB", "AAAA") },
                            { addition: true, item: new Error("apple") },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0, "BBBB") },
                            { addition: true, item: new Error("cherry") },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("unsubscribes correctly", function () { return __awaiter(_this, void 0, void 0, function () {
            var addBlockToken, removeBlockToken, addLogToken, removeLogToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        addBlockToken = blockAndLogStreamer.subscribeToOnBlockAdded(function (block) { return chai_1.expect(true).to.be.false; });
                        blockAndLogStreamer.unsubscribeFromOnBlockAdded(addBlockToken);
                        removeBlockToken = blockAndLogStreamer.subscribeToOnBlockRemoved(function (block) { return chai_1.expect(true).to.be.false; });
                        blockAndLogStreamer.unsubscribeFromOnBlockRemoved(removeBlockToken);
                        addLogToken = blockAndLogStreamer.subscribeToOnLogAdded(function (block) { return chai_1.expect(true).to.be.false; });
                        blockAndLogStreamer.unsubscribeFromOnLogAdded(addLogToken);
                        removeLogToken = blockAndLogStreamer.subscribeToOnLogRemoved(function (block) { return chai_1.expect(true).to.be.false; });
                        blockAndLogStreamer.unsubscribeFromOnLogRemoved(removeLogToken);
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777, "AAAA", "AAAA"));
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "AAAA", "AAAA"));
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, "AAAA", "BBBB"))];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        it("throws if unsubscribing with invalid token", function () { return __awaiter(_this, void 0, void 0, function () {
            var addBlockToken, removeBlockToken;
            return __generator(this, function (_a) {
                addBlockToken = blockAndLogStreamer.subscribeToOnBlockAdded(function (_) { });
                removeBlockToken = blockAndLogStreamer.subscribeToOnBlockRemoved(function (_) { });
                chai_1.expect(function () { return blockAndLogStreamer.unsubscribeFromOnBlockAdded(removeBlockToken); }).to.throw(Error);
                chai_1.expect(function () { return blockAndLogStreamer.unsubscribeFromOnBlockRemoved(addBlockToken); }).to.throw(Error);
                chai_1.expect(function () { return blockAndLogStreamer.unsubscribeFromOnLogAdded(addBlockToken); }).to.throw(Error);
                chai_1.expect(function () { return blockAndLogStreamer.unsubscribeFromOnLogRemoved(addBlockToken); }).to.throw(Error);
                return [2 /*return*/];
            });
        }); });
        it("calls getLogs multiple times for multiple filters", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogsCallCount, getLogs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogsCallCount = 0;
                        getLogs = function (filter) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                ++getLogsCallCount;
                                return [2 /*return*/, []];
                            });
                        }); };
                        blockAndLogStreamer = new index_1.BlockAndLogStreamer(helpers_1.getBlockByHashFactory(), getLogs, onError);
                        blockAndLogStreamer.addLogFilter({ address: "0xdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef", topics: [] });
                        blockAndLogStreamer.addLogFilter({ address: "0xdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef", topics: ["0xbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbaadf00d"] });
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        chai_1.expect(getLogsCallCount).to.equal(2);
                        return [2 /*return*/];
                }
            });
        }); });
        it("doesn't call getLogs if no filters are attached", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var getLogsCallCount, getLogs, filterAToken, filterBToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getLogsCallCount = 0;
                        getLogs = function (filter) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                ++getLogsCallCount;
                                return [2 /*return*/, []];
                            });
                        }); };
                        blockAndLogStreamer = new index_1.BlockAndLogStreamer(helpers_1.getBlockByHashFactory(), getLogs, onError);
                        filterAToken = blockAndLogStreamer.addLogFilter({ address: "0xdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef", topics: [] });
                        filterBToken = blockAndLogStreamer.addLogFilter({ address: "0xdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef", topics: ["0xbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbadf00dbaadf00d"] });
                        blockAndLogStreamer.removeLogFilter(filterAToken);
                        blockAndLogStreamer.removeLogFilter(filterBToken);
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        chai_1.expect(getLogsCallCount).to.equal(0);
                        return [2 /*return*/];
                }
            });
        }); });
        it("does not announce or make changes to state if we get logs for wrong block", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var defaultGetLogs, forkedGetLogs, getLogs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        defaultGetLogs = helpers_1.getLogsFactory(1);
                        forkedGetLogs = helpers_1.getLogsFactory(1, 'BBBB');
                        getLogs = function (filterOptions) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, (filterOptions.fromBlock === '0x7778' || filterOptions.fromBlock === '0x7779') ? forkedGetLogs(filterOptions) : defaultGetLogs(filterOptions)];
                        }); }); };
                        reinitialize(helpers_1.getBlockByHashFactory([new helpers_1.MockBlock(0x7778, 'BBBB', 'AAAA')]), getLogs);
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7778)).catch(function () { })];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, 'BBBB'))];
                    case 3:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777) },
                            { addition: true, item: new helpers_1.MockLog(0x7777, 0) },
                            { addition: true, item: new helpers_1.MockBlock(0x7778, 'BBBB', 'AAAA') },
                            { addition: true, item: new helpers_1.MockLog(0x7778, 0, 'BBBB') },
                            { addition: true, item: new helpers_1.MockBlock(0x7779, 'BBBB') },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0, 'BBBB') },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("does not announce or make changes to state if we can't fetch a parent block", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var defaultGetBlockByHash, getBlockByHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        defaultGetBlockByHash = helpers_1.getBlockByHashFactory();
                        getBlockByHash = function (hash) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, (hash === '0xbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cBBBB7778') ? null : defaultGetBlockByHash(hash)];
                        }); }); };
                        reinitialize(getBlockByHash, helpers_1.getLogsFactory(1));
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7778))];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, 'BBBB', 'BBBB')).catch(function () { })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779))];
                    case 4:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777) },
                            { addition: true, item: new helpers_1.MockLog(0x7777, 0) },
                            { addition: true, item: new helpers_1.MockBlock(0x7778) },
                            { addition: true, item: new helpers_1.MockLog(0x7778, 0) },
                            { addition: true, item: new helpers_1.MockBlock(0x7779) },
                            { addition: true, item: new helpers_1.MockLog(0x7779, 0) },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        it("non-awaited reconciliation failure will result in failure of following reconciliation", function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var defaultGetBlockByHash, getBlockByHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        defaultGetBlockByHash = helpers_1.getBlockByHashFactory();
                        getBlockByHash = function (hash) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, (hash === '0xbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cBBBB7778') ? null : defaultGetBlockByHash(hash)];
                        }); }); };
                        reinitialize(getBlockByHash, helpers_1.getLogsFactory(0));
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7777))];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7778))];
                    case 2:
                        _a.sent();
                        blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779, 'BBBB', 'BBBB')).catch(function () { });
                        return [4 /*yield*/, blockAndLogStreamer.reconcileNewBlock(new helpers_1.MockBlock(0x7779)).catch(function () { })];
                    case 3:
                        _a.sent();
                        chai_1.expect(announcements).to.deep.equal([
                            { addition: true, item: new helpers_1.MockBlock(0x7777) },
                            { addition: true, item: new helpers_1.MockBlock(0x7778) },
                        ]);
                        return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
//# sourceMappingURL=index.js.map