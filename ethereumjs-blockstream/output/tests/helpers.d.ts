import { Block, Transaction, Log, FilterOptions } from "../source/index";
export declare function delay(milliseconds: number): Promise<void>;
export declare function getBlockByHashFactory(blocks?: Block[]): (hash: string) => Promise<Block | null>;
export declare function getLogsFactory(logsPerFilter: number, fork?: string): (filterOptions: FilterOptions) => Promise<Log[]>;
export declare class MockBlock implements Block {
    readonly hash: string;
    readonly parentHash: string;
    readonly number: string;
    readonly nonce: string;
    readonly sha3Uncles: string;
    readonly logsBloom: string;
    readonly transactionRoot: string;
    readonly stateRoot: string;
    readonly receiptsRoot: string;
    readonly miner: string;
    readonly difficulty: string;
    readonly totalDifficulty: string;
    readonly size: string;
    readonly gasLimit: string;
    readonly gasUsed: string;
    readonly timestamp: string;
    readonly transactions: string[] | Transaction[];
    readonly uncles: string[];
    constructor(number: number, fork?: string, parentFork?: string);
}
export declare class MockLog implements Log {
    readonly logIndex: string;
    readonly blockNumber: string;
    readonly blockHash: string;
    readonly transactionHash: string;
    readonly transactionIndex: string;
    readonly address: string;
    readonly data: string;
    readonly topics: string[];
    constructor(blockNumber: number, logIndex?: number, fork?: string);
}
