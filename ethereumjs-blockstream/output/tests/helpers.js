"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
function delay(milliseconds) { return new Promise(function (resolve, reject) { return setTimeout(resolve, milliseconds); }); }
exports.delay = delay;
function getBlockByHashFactory(blocks) {
    var _this = this;
    if (blocks === void 0) { blocks = []; }
    var blockMapping = blocks.reduce(function (mapping, block) { return mapping.set(block.hash, block); }, new Map());
    return function (hash) { return __awaiter(_this, void 0, void 0, function () {
        var mappedBlock, blockNumber, fork;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, delay(0)];
                case 1:
                    _a.sent();
                    mappedBlock = blockMapping.get(hash);
                    if (mappedBlock !== undefined) {
                        return [2 /*return*/, mappedBlock];
                    }
                    else {
                        blockNumber = parseInt(hash.substr(-4), 16);
                        fork = hash.substr(-8, 4);
                        return [2 /*return*/, new MockBlock(blockNumber, fork, fork)];
                    }
                    return [2 /*return*/];
            }
        });
    }); };
}
exports.getBlockByHashFactory = getBlockByHashFactory;
function getLogsFactory(logsPerFilter, fork) {
    var _this = this;
    if (fork === void 0) { fork = "AAAA"; }
    return function (filterOptions) { return __awaiter(_this, void 0, void 0, function () {
        var logs, logIndex, i, blockNumber;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, delay(0)];
                case 1:
                    _a.sent();
                    if (!filterOptions)
                        throw new Error("filter options are required");
                    logs = [];
                    logIndex = 0;
                    for (i = 0; i < logsPerFilter; ++i) {
                        blockNumber = parseInt(filterOptions.toBlock, 16);
                        logs.push(new MockLog(blockNumber, logIndex++, fork));
                    }
                    return [2 /*return*/, logs];
            }
        });
    }); };
}
exports.getLogsFactory = getLogsFactory;
var MockBlock = /** @class */ (function () {
    function MockBlock(number, fork, parentFork) {
        if (fork === void 0) { fork = "AAAA"; }
        this.nonce = "";
        this.sha3Uncles = "";
        this.logsBloom = "string";
        this.transactionRoot = "string";
        this.stateRoot = "string";
        this.receiptsRoot = "string";
        this.miner = "string";
        this.difficulty = "string";
        this.totalDifficulty = "string";
        this.size = "string";
        this.gasLimit = "string";
        this.gasUsed = "string";
        this.timestamp = "string";
        this.transactions = [];
        this.uncles = [];
        if (!parentFork)
            parentFork = fork;
        var numberAsHex = number.toString(16);
        var parentNumberAsHex = (number - 1).toString(16);
        this.hash = "0xbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0c" + fork + ("0000" + numberAsHex).substring(numberAsHex.length);
        this.parentHash = "0xbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0c" + parentFork + ("0000" + parentNumberAsHex).substring(parentNumberAsHex.length);
        this.number = "0x" + numberAsHex;
    }
    return MockBlock;
}());
exports.MockBlock = MockBlock;
var MockLog = /** @class */ (function () {
    function MockLog(blockNumber, logIndex, fork) {
        if (logIndex === void 0) { logIndex = 0x0; }
        if (fork === void 0) { fork = "AAAA"; }
        this.transactionHash = "0xbaadf00dbaadf00dbaadf00dbaadf00dbaadf00dbaadf00dbaadf00dbaadf00d";
        this.transactionIndex = "0x0";
        this.address = "0xdeadbeefdeadbeefdeadbeefdeadbeefdeadbeef";
        this.data = "0x0000000000000000000000000000000000000000000000000000000000000000";
        this.topics = [];
        var blockNumberAsHex = blockNumber.toString(16);
        this.blockNumber = "0x" + blockNumberAsHex;
        this.blockHash = "0xbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0cbl0c" + fork + ("0000" + blockNumberAsHex).substring(blockNumberAsHex.length);
        this.logIndex = "0x" + logIndex.toString(16);
        this.transactionIndex = this.logIndex;
    }
    return MockLog;
}());
exports.MockLog = MockLog;
//# sourceMappingURL=helpers.js.map